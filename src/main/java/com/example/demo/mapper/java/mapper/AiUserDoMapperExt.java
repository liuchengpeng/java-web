package com.example.demo.mapper.java.mapper;

import com.example.demo.model.entityDto.AiUserDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface AiUserDoMapperExt extends AiUserDoMapper {

    AiUserDto loadUserByUsername(String username);

    /**
     * 查询用户信息表，可分页
     *
     * @param aiUserDto
     * @return
     */
    List<AiUserDto> findUserListByDto(AiUserDto aiUserDto);

    List<AiUserDto> getAiUserLists();

    AiUserDto findUserByMap(Map<String, Object> params);

    int batchDeleteAiUser(@Param("aiUserIdArray") String[] aiUserIdArray);
}