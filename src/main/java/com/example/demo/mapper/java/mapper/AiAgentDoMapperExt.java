package com.example.demo.mapper.java.mapper;


import com.example.demo.model.entityDto.AiAgentDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AiAgentDoMapperExt {
    /**
     * 查询权限列表
     * @param aiAgentDto
     * @return
     */
    List<AiAgentDto> queryList(@Param(value = "param") AiAgentDto aiAgentDto);
}