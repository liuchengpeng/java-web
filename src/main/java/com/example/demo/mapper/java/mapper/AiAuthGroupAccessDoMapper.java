package com.example.demo.mapper.java.mapper;

import com.example.demo.model.entityDo.AiAuthGroupAccessDo;

public interface AiAuthGroupAccessDoMapper {
    int insert(AiAuthGroupAccessDo record);

    int insertSelective(AiAuthGroupAccessDo record);
}