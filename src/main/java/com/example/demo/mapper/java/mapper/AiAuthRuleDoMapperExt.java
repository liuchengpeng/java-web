package com.example.demo.mapper.java.mapper;

import com.example.demo.model.entityDto.AiAuthRuleDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AiAuthRuleDoMapperExt {
    /**
     * 查询权限列表
     * @param aiAuthRuleDto
     * @return
     */
    List<AiAuthRuleDto> queryList(@Param(value = "param") AiAuthRuleDto aiAuthRuleDto);

    List<AiAuthRuleDto> findAiAuthRuleDtoChildrenNode(@Param("id") Integer id);

}
