package com.example.demo.mapper.java.mapper;

import com.example.demo.model.entityDo.AiAuthGroupDo;

public interface AiAuthGroupDoMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(AiAuthGroupDo record);

    int insertSelective(AiAuthGroupDo record);

    AiAuthGroupDo selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(AiAuthGroupDo record);

    int updateByPrimaryKey(AiAuthGroupDo record);
}