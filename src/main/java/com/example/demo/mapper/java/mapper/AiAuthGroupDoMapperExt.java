package com.example.demo.mapper.java.mapper;

import com.example.demo.model.entityDto.AiAuthGroupDto;

import java.util.List;
import java.util.Map;

public interface AiAuthGroupDoMapperExt extends AiAuthGroupDoMapper {

    List<AiAuthGroupDto> getAllAiAuthGroup();

    AiAuthGroupDto getAiAuthGroupByMap(Map<String, Object> params);
}
