package com.example.demo.mapper.java.mapper;

import com.example.demo.model.entityDo.AiAuthRuleDo;

public interface AiAuthRuleDoMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(AiAuthRuleDo record);

    int insertSelective(AiAuthRuleDo record);

    AiAuthRuleDo selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(AiAuthRuleDo record);

    int updateByPrimaryKey(AiAuthRuleDo record);
}
