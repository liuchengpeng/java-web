package com.example.demo;

import com.example.demo.securitys.filter.WrapperFilter;
import lombok.extern.log4j.Log4j;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;

import javax.servlet.Filter;

@MapperScan("com.example.demo.mapper.java.mapper")
@Log4j
@SpringBootApplication
public class AiwebJavaApplication {

    public static void main(String[] args) {
        SpringApplication.run(AiwebJavaApplication.class, args);
        log.info("============= SpringBoot Start Success =============");
    }

    // DataSource配置
    @Bean
    @ConfigurationProperties(prefix = "spring.datasource")
    public DataSource dataSource() {
        return new org.apache.tomcat.jdbc.pool.DataSource();
    }

    // 提供SqlSession
    /*
     * @Bean public SqlSessionFactory sqlSessionFactoryBean() throws Exception {
     * SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
     * sqlSessionFactoryBean.setDataSource(dataSource());
     * PathMatchingResourcePatternResolver resolver = new
     * PathMatchingResourcePatternResolver();
     * sqlSessionFactoryBean.setMapperLocations(resolver.getResources(
     * "classpath:/mybatis/*.xml")); return sqlSessionFactoryBean.getObject(); }
     */

    // 事务管理
    @Bean
    public PlatformTransactionManager transactionManager() {
        return new DataSourceTransactionManager(dataSource());
    }

    @Bean
    public Filter wrapperFilter() {
        return new WrapperFilter();
    }

}
