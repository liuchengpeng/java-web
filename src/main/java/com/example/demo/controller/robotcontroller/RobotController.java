package com.example.demo.controller.robotcontroller;

import com.example.demo.model.robotDO.RobotCommonReqDo;
import com.example.demo.model.robotDO.RobotCommonRespDo;
import com.example.demo.service.service.RobotService;
import com.example.demo.util.common.exception.AIValidateException;
import com.example.demo.util.common.vo.ResultModel;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/robot")
@Slf4j
public class RobotController {
    @Autowired
    private RobotService robotService;

    /**
     * 创建机器人
     * @param robotCommonReqDo
     * @param request
     * @param response
     * @return
     */
    @PostMapping("create")
    public ResultModel<RobotCommonRespDo> createRobot(RobotCommonReqDo robotCommonReqDo, HttpServletRequest request, HttpServletResponse response){
        ResultModel<RobotCommonRespDo> resultModel = new ResultModel<>();
        try {
            RobotCommonRespDo respDo = robotService.create(robotCommonReqDo,request,response);
            resultModel = returnResult(respDo);
        }catch (AIValidateException e){
            log.error("创建机器人异常",e);
            resultModel.setErrorCode(e.getCode());
            resultModel.setSuccessed(false);
        }
        return resultModel;
    }

    /**
     * 更新机器人基本信息
     * @param robotCommonReqDo
     * @param request
     * @param response
     * @return
     */
    @PostMapping("/upInfo")
    public ResultModel<RobotCommonRespDo> upInfoRobot(RobotCommonReqDo robotCommonReqDo, HttpServletRequest request, HttpServletResponse response){
        ResultModel<RobotCommonRespDo> resultModel = new ResultModel<>();
        try {
            RobotCommonRespDo respDo = robotService.upInfo(robotCommonReqDo,request,response);
            resultModel = returnResult(respDo);
        }catch (AIValidateException e){
            log.error("更新机器人基本信息",e);
            resultModel.setErrorCode(e.getCode());
            resultModel.setSuccessed(false);
        }
        return resultModel;
    }
    /**
     * 查询机器人列表
     * @param robotCommonReqDo
     * @param request
     * @param response
     * @return
     */
    @PostMapping("/queryList")
    public ResultModel<RobotCommonRespDo> queryList(RobotCommonReqDo robotCommonReqDo, HttpServletRequest request, HttpServletResponse response){
        ResultModel<RobotCommonRespDo> resultModel = new ResultModel<>();
        try {
            RobotCommonRespDo respDo = robotService.queryList(robotCommonReqDo,request,response);
            resultModel = returnResult(respDo);
        }catch (AIValidateException e){
            log.error("查询机器人列表",e);
            resultModel.setErrorCode(e.getCode());
            resultModel.setSuccessed(false);
        }
        return resultModel;
    }
    /**
     * 更新某机器人的secretkey
     * @param robotCommonReqDo
     * @param request
     * @param response
     * @return
     */
    @PostMapping("/upSecretkey")
    public ResultModel<RobotCommonRespDo> upSecretkey(RobotCommonReqDo robotCommonReqDo, HttpServletRequest request, HttpServletResponse response){
        ResultModel<RobotCommonRespDo> resultModel = new ResultModel<>();
        try {
            RobotCommonRespDo respDo = robotService.upSecretkey(robotCommonReqDo,request,response);
            resultModel = returnResult(respDo);
        }catch (AIValidateException e){
            log.error("更新某机器人的secretkey",e);
            resultModel.setErrorCode(e.getCode());
            resultModel.setSuccessed(false);
        }
        return resultModel;
    }
    /**
     * 更新某机器人的状态
     * @param robotCommonReqDo
     * @param request
     * @param response
     * @return
     */
    @PostMapping("/upStatus")
    public ResultModel<RobotCommonRespDo> upStatus(RobotCommonReqDo robotCommonReqDo, HttpServletRequest request, HttpServletResponse response){
        ResultModel<RobotCommonRespDo> resultModel = new ResultModel<>();
        try {
            RobotCommonRespDo respDo = robotService.upStatus(robotCommonReqDo,request,response);
            resultModel = returnResult(respDo);
        }catch (AIValidateException e){
            log.error("更新某机器人的状态",e);
            resultModel.setErrorCode(e.getCode());
            resultModel.setSuccessed(false);
        }
        return resultModel;
    }
    /**
     * 更新某机器人的名字
     * @param robotCommonReqDo
     * @param request
     * @param response
     * @return
     */
    @PostMapping("/upName")
    public ResultModel<RobotCommonRespDo> upName(RobotCommonReqDo robotCommonReqDo, HttpServletRequest request, HttpServletResponse response){
        ResultModel<RobotCommonRespDo> resultModel = new ResultModel<>();
        try {
            RobotCommonRespDo respDo = robotService.upName(robotCommonReqDo,request,response);
            resultModel = returnResult(respDo);
        }catch (AIValidateException e){
            log.error("更新某机器人的名字",e);
            resultModel.setErrorCode(e.getCode());
            resultModel.setSuccessed(false);
        }
        return resultModel;
    }
    /**
     * 更新某机器人的杂项信息
     * @param robotCommonReqDo
     * @param request
     * @param response
     * @return
     */
    @PostMapping("/upOther")
    public ResultModel<RobotCommonRespDo> upOther(RobotCommonReqDo robotCommonReqDo, HttpServletRequest request, HttpServletResponse response){
        ResultModel<RobotCommonRespDo> resultModel = new ResultModel<>();
        try {
            RobotCommonRespDo respDo = robotService.upOther(robotCommonReqDo,request,response);
            resultModel = returnResult(respDo);
        }catch (AIValidateException e){
            log.error("更新某机器人的杂项信息",e);
            resultModel.setErrorCode(e.getCode());
            resultModel.setSuccessed(false);
        }
        return resultModel;
    }
    /**
     * 获取某机器人各项信息
     * @param robotCommonReqDo
     * @param request
     * @param response
     * @return
     */
    @PostMapping("/selectOne")
    public ResultModel<RobotCommonRespDo> selectOne(RobotCommonReqDo robotCommonReqDo, HttpServletRequest request, HttpServletResponse response){
        ResultModel<RobotCommonRespDo> resultModel = new ResultModel<>();
        try {
            RobotCommonRespDo respDo = robotService.selectOne(robotCommonReqDo,request,response);
            resultModel = returnResult(respDo);
        }catch (AIValidateException e){
            log.error("获取某机器人各项信息",e);
            resultModel.setErrorCode(e.getCode());
            resultModel.setSuccessed(false);
        }
        return resultModel;
    }
    /**
     * 更新机器人流程图入口节点
     * @param robotCommonReqDo
     * @param request
     * @param response
     * @return
     */
    @PostMapping("/upEntryTreeId")
    public ResultModel<RobotCommonRespDo> upEntryTreeId(RobotCommonReqDo robotCommonReqDo, HttpServletRequest request, HttpServletResponse response){
        ResultModel<RobotCommonRespDo> resultModel = new ResultModel<>();
        try {
            RobotCommonRespDo respDo = robotService.upEntryTreeId(robotCommonReqDo,request,response);
            resultModel = returnResult(respDo);
        }catch (AIValidateException e){
            log.error("更新机器人流程图入口节点",e);
            resultModel.setErrorCode(e.getCode());
            resultModel.setSuccessed(false);
        }
        return resultModel;
    }
    /**
     * 将某机器人所有信息覆盖至另一个
     * @param robotCommonReqDo
     * @param request
     * @param response
     * @return
     */
    @PostMapping("/copy")
    public ResultModel<RobotCommonRespDo> copy(RobotCommonReqDo robotCommonReqDo, HttpServletRequest request, HttpServletResponse response){
        ResultModel<RobotCommonRespDo> resultModel = new ResultModel<>();
        try {
            RobotCommonRespDo respDo = robotService.copy(robotCommonReqDo,request,response);
            resultModel = returnResult(respDo);
        }catch (AIValidateException e){
            log.error("将某机器人所有信息覆盖至另一个",e);
            resultModel.setErrorCode(e.getCode());
            resultModel.setSuccessed(false);
        }
        return resultModel;
    }

    /**
     * 返回赋值
     * @param respDo
     */
    private ResultModel<RobotCommonRespDo> returnResult(RobotCommonRespDo respDo) {
        ResultModel<RobotCommonRespDo> resultModel = new ResultModel<>();
        //如果返回为0成功 否则失败
        if (StringUtils.equals(respDo.getErrno(),"0")){
            resultModel.isSuccessed();
            resultModel.setReturnValue(respDo);
        }else{
            resultModel.setSuccessed(false);
            resultModel.setErrorCode(respDo.getErrno());
            resultModel.setErrorDesc(respDo.getErrstr());
        }
        return resultModel;
    }
}
