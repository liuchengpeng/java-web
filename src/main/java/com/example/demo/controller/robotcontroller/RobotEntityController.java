package com.example.demo.controller.robotcontroller;

import com.example.demo.model.robotDto.RobotEntityReqDto;
import com.example.demo.model.robotDto.RobotEntityRespDto;
import com.example.demo.service.service.RobotEntityService;
import com.example.demo.util.common.exception.AIValidateException;
import com.example.demo.util.common.vo.ResultModel;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.*;


/**
 * @class_name: RobotEntityController
 * @package: com.example.demo.controller.robotcontroller
 * @describe: 机器人实体管理
 * @author: le.gao
 * @creat_date: 2019/7/1
 * @creat_time: 14:15
 **/
@RestController
@RequestMapping("/robotEntity")
@Slf4j
public class RobotEntityController {

    private final RobotEntityService robotEntityService;

    @PostMapping("createRobotEntity")
    public ResultModel<RobotEntityRespDto> createRobotEntity(RobotEntityReqDto robotEntityReqDto) {
        if (StringUtils.isBlank(robotEntityReqDto.getName())) {
            throw new AIValidateException("实体名称不能为空");
        }

        if (robotEntityReqDto.getRobotid() == null) {
            throw new AIValidateException("机器人编号不能为空");
        }

        ResultModel<RobotEntityRespDto> robotEntityRespDtoResultModel = new ResultModel<>();
        try {
            RobotEntityRespDto robotEntityRespDto = robotEntityService.createRobotEntity(robotEntityReqDto);
            if (!String.valueOf(0).equals(robotEntityRespDto.getErrno())) {
                robotEntityRespDtoResultModel.setSuccessed(false);
                robotEntityRespDtoResultModel.setErrorCode(robotEntityRespDto.getErrno());
                robotEntityRespDtoResultModel.setErrorDesc(robotEntityRespDto.getErrstr());
            }
        } catch (Exception a) {
            log.error("RobotEntityController createRobotEntity:", a);
            robotEntityRespDtoResultModel.setSuccessed(false);
            robotEntityRespDtoResultModel.setErrorCode("1");
            robotEntityRespDtoResultModel.setErrorDesc("创建失败");
        }
        return robotEntityRespDtoResultModel;
    }

    @GetMapping("getRobotEntityList")
    public ResultModel<RobotEntityRespDto.NewRobotEntityData> getRobotEntityList(RobotEntityReqDto robotEntityReqDto) {
        if (robotEntityReqDto.getRobotid() == null) {
            throw new AIValidateException("机器人编号不能为空");
        }

        ResultModel<RobotEntityRespDto.NewRobotEntityData> newRobotEntityDataResultModel = new ResultModel<>();
        try {
            RobotEntityRespDto robotEntityRespDto = robotEntityService.getRobotEntityList(robotEntityReqDto);
            newRobotEntityDataResultModel.setReturnValue(robotEntityRespDto.getData());
        } catch (Exception e) {
            log.error("RobotEntityController getRobotEntityList:", e);
            newRobotEntityDataResultModel.setSuccessed(false);
            newRobotEntityDataResultModel.setErrorCode("1");
            newRobotEntityDataResultModel.setErrorDesc("查询失败");
        }
        return newRobotEntityDataResultModel;
    }

    @PostMapping("/updateEntityName")
    public ResultModel<RobotEntityRespDto.NewRobotEntityData> updateEntityName(RobotEntityReqDto robotEntityReqDto) {
        checkParams(robotEntityReqDto);
        ResultModel<RobotEntityRespDto.NewRobotEntityData> robotEntityRespDtoResultModel = new ResultModel<>();

        try {
            RobotEntityRespDto robotEntityRespDto = robotEntityService.updateEntityName(robotEntityReqDto);
            if (!String.valueOf(0).equals(robotEntityRespDto.getErrno())) {
                robotEntityRespDtoResultModel.setSuccessed(false);
                robotEntityRespDtoResultModel.setErrorCode(robotEntityRespDto.getErrno());
                robotEntityRespDtoResultModel.setErrorDesc(robotEntityRespDto.getErrstr());
                return robotEntityRespDtoResultModel;
            }
            robotEntityRespDtoResultModel.setReturnValue(robotEntityRespDto.getData());
        } catch (Exception e) {
            log.error("RobotEntityController updateEntityName:", e);
            robotEntityRespDtoResultModel.setSuccessed(false);
            robotEntityRespDtoResultModel.setErrorCode("1");
            robotEntityRespDtoResultModel.setErrorDesc("修改失败");
        }
        return robotEntityRespDtoResultModel;
    }

    @PostMapping("updateEntityContent")
    public ResultModel<RobotEntityRespDto.NewRobotEntityData> updateEntityContent(@RequestBody RobotEntityReqDto robotEntityReqDto) {
        checkParams(robotEntityReqDto);

        ResultModel<RobotEntityRespDto.NewRobotEntityData> robotEntityRespDtoResultModel = new ResultModel<>();
        try {
            RobotEntityRespDto robotEntityRespDto = robotEntityService.updateEntityContent(robotEntityReqDto);
            if (!String.valueOf(0).equals(robotEntityRespDto.getErrno())) {
                robotEntityRespDtoResultModel.setSuccessed(false);
                robotEntityRespDtoResultModel.setErrorCode(robotEntityRespDto.getErrno());
                robotEntityRespDtoResultModel.setErrorDesc(robotEntityRespDto.getErrstr());
                return robotEntityRespDtoResultModel;
            }
            robotEntityRespDtoResultModel.setReturnValue(robotEntityRespDto.getData());
        } catch (Exception e) {
            log.error("RobotEntityController updateEntityContent:", e);
            robotEntityRespDtoResultModel.setSuccessed(false);
            robotEntityRespDtoResultModel.setErrorCode("1");
            robotEntityRespDtoResultModel.setErrorDesc("修改失败");
        }
        return robotEntityRespDtoResultModel;
    }

    @PostMapping("deleteEntity")
    public ResultModel<RobotEntityRespDto> deleteEntity(RobotEntityReqDto robotEntityReqDto) {
        checkParams(robotEntityReqDto);
        ResultModel<RobotEntityRespDto> robotEntityRespDtoResultModel = new ResultModel<>();
        try {
            RobotEntityRespDto robotEntityRespDto = robotEntityService.deleteEntity(robotEntityReqDto);
            if (!String.valueOf(0).equals(robotEntityRespDto.getErrno())) {
                robotEntityRespDtoResultModel.setSuccessed(false);
                robotEntityRespDtoResultModel.setErrorCode(robotEntityRespDto.getErrno());
                robotEntityRespDtoResultModel.setErrorDesc(robotEntityRespDto.getErrstr());
            }
        } catch (Exception e) {
            log.error("RobotEntityController deleteEntity:", e);
            robotEntityRespDtoResultModel.setSuccessed(false);
            robotEntityRespDtoResultModel.setErrorCode("1");
            robotEntityRespDtoResultModel.setErrorDesc("删除失败");
        }
        return robotEntityRespDtoResultModel;
    }


    @PostMapping("getEntityOneInfo")
    public ResultModel<RobotEntityRespDto.NewRobotEntityData> getEntityOneInfo(RobotEntityReqDto robotEntityReqDto) {
        checkParams(robotEntityReqDto);
        ResultModel<RobotEntityRespDto.NewRobotEntityData> robotEntityRespDtoResultModel = new ResultModel<>();

        try {
            RobotEntityRespDto robotEntityRespDto = robotEntityService.getEntityOneInfo(robotEntityReqDto);
            if (!String.valueOf(0).equals(robotEntityRespDto.getErrno())) {
                robotEntityRespDtoResultModel.setSuccessed(false);
                robotEntityRespDtoResultModel.setErrorCode(robotEntityRespDto.getErrno());
                robotEntityRespDtoResultModel.setErrorDesc(robotEntityRespDto.getErrstr());
                return robotEntityRespDtoResultModel;
            }
            robotEntityRespDtoResultModel.setReturnValue(robotEntityRespDto.getData());
        } catch (Exception e) {
            log.error("RobotEntityController getEntityOneInfo:", e);
            robotEntityRespDtoResultModel.setSuccessed(false);
            robotEntityRespDtoResultModel.setErrorCode("1");
            robotEntityRespDtoResultModel.setErrorDesc("查询失败");
        }

        return robotEntityRespDtoResultModel;
    }

    private void checkParams(RobotEntityReqDto robotEntityReqDto) {
        if (robotEntityReqDto.getRobotid() == null) {
            throw new AIValidateException("机器人编号不能为空");
        }

        if (robotEntityReqDto.getEntityid() == null) {
            throw new AIValidateException("实体编号不能为空");
        }
    }

    public RobotEntityController(RobotEntityService robotEntityService) {
        this.robotEntityService = robotEntityService;
    }
}
