package com.example.demo.controller.robotcontroller;

import com.example.demo.model.robotDO.RobotTreeJumpReqDo;
import com.example.demo.model.robotDto.RobotTreeReqDto;
import com.example.demo.model.robotDto.RobotTreeRespDto;
import com.example.demo.service.service.RobotTreeService;
import com.example.demo.util.common.exception.AIValidateException;
import com.example.demo.util.common.vo.ResultModel;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.*;

/**
 * @class_name: RobotTreeController
 * @package: com.example.demo.controller.robotcontroller
 * @describe: robot 任务节点管理
 * @author: le.gao
 * @creat_date: 2019/7/2
 * @creat_time: 19:10
 **/
@RestController
@RequestMapping("/robotTree")
@Slf4j
public class RobotTreeController {

    private RobotTreeService robotTreeService;

    @PostMapping("createTaskTree")
    public ResultModel<RobotTreeRespDto> createTaskTree(@RequestBody RobotTreeReqDto robotTreeReqDto) {
        if (StringUtils.isBlank(robotTreeReqDto.getName())) {
            throw new AIValidateException("节点名称不能为空");
        }

        if (robotTreeReqDto.getRobotid() == null) {
            throw new AIValidateException("机器人编号不能为空");
        }
        ResultModel<RobotTreeRespDto> robotTreeRespDtoResultModel = new ResultModel<>();
        try {
            RobotTreeRespDto robotTreeRespDto = robotTreeService.createTaskTree(robotTreeReqDto);
            if (!String.valueOf(0).equals(robotTreeRespDto.getErrno())) {
                robotTreeRespDtoResultModel.setSuccessed(false);
                robotTreeRespDtoResultModel.setErrorCode(robotTreeRespDto.getErrno());
                robotTreeRespDtoResultModel.setErrorDesc(robotTreeRespDto.getErrstr());
                return robotTreeRespDtoResultModel;
            }
        } catch (Exception e) {
            log.error("RobotTreeController createTaskTree:", e);
            robotTreeRespDtoResultModel.setSuccessed(false);
            robotTreeRespDtoResultModel.setErrorCode("1");
            robotTreeRespDtoResultModel.setErrorDesc("创建失败");
        }
        return robotTreeRespDtoResultModel;
    }

    @PostMapping("getTreeLsList")
    public ResultModel<RobotTreeRespDto.TreeLsInfoDo> getTreeLsList(RobotTreeReqDto robotTreeReqDto) {
        if (robotTreeReqDto.getRobotid() == null) {
            throw new AIValidateException("机器人编号不能为空");
        }

        ResultModel<RobotTreeRespDto.TreeLsInfoDo> robotTreeRespDtoResultModel = new ResultModel<>();

        try {
            RobotTreeRespDto robotTreeRespDto = robotTreeService.getTreeLsList(robotTreeReqDto);
            return buildResultData(robotTreeRespDtoResultModel, robotTreeRespDto);
        } catch (Exception e) {
            log.error("RobotTreeController getTreeLsList:", e);
            robotTreeRespDtoResultModel.setSuccessed(false);
            robotTreeRespDtoResultModel.setErrorCode("1");
            robotTreeRespDtoResultModel.setErrorDesc("查询失败");
        }
        return robotTreeRespDtoResultModel;
    }

    @PostMapping("getTreeOneInfo")
    public ResultModel<RobotTreeRespDto.TreeLsInfoDo> getTreeOneInfo(RobotTreeReqDto robotTreeReqDto) {
        checkParams(robotTreeReqDto);
        ResultModel<RobotTreeRespDto.TreeLsInfoDo> robotTreeRespDtoResultModel = new ResultModel<>();
        try {
            RobotTreeRespDto robotTreeRespDto = robotTreeService.getTreeOneInfo(robotTreeReqDto);
            return buildResultData(robotTreeRespDtoResultModel, robotTreeRespDto);
        } catch (Exception e) {
            log.error("RobotTreeController getTreeOneInfo:", e);
            robotTreeRespDtoResultModel.setSuccessed(false);
            robotTreeRespDtoResultModel.setErrorCode("1");
            robotTreeRespDtoResultModel.setErrorDesc("查询失败");
        }
        return robotTreeRespDtoResultModel;
    }

    @PostMapping("updateTreeInfo")
    public ResultModel<RobotTreeRespDto.TreeLsInfoDo> updateTreeInfo(@RequestBody RobotTreeReqDto robotTreeReqDto) {
        checkParams(robotTreeReqDto);
        ResultModel<RobotTreeRespDto.TreeLsInfoDo> robotTreeRespDtoResultModel = new ResultModel<>();
        try {
            RobotTreeRespDto robotTreeRespDto = robotTreeService.updateTreeInfo(robotTreeReqDto);
            return buildResultData(robotTreeRespDtoResultModel, robotTreeRespDto);
        } catch (Exception e) {
            log.error("RobotTreeController updateTreeInfo:", e);
            robotTreeRespDtoResultModel.setSuccessed(false);
            robotTreeRespDtoResultModel.setErrorCode("1");
            robotTreeRespDtoResultModel.setErrorDesc("修改失败");
        }
        return robotTreeRespDtoResultModel;
    }

    @GetMapping("deleteTreeInfo")
    public ResultModel<RobotTreeRespDto> deleteTreeInfo(RobotTreeReqDto robotTreeReqDto) {
        checkParams(robotTreeReqDto);
        ResultModel<RobotTreeRespDto> robotTreeRespDtoResultModel = new ResultModel<>();
        try {
            RobotTreeRespDto robotTreeRespDto = robotTreeService.deleteTreeInfo(robotTreeReqDto);
            if (!String.valueOf(0).equals(robotTreeRespDto.getErrno())) {
                robotTreeRespDtoResultModel.setSuccessed(false);
                robotTreeRespDtoResultModel.setErrorCode(robotTreeRespDto.getErrno());
                robotTreeRespDtoResultModel.setErrorDesc(robotTreeRespDto.getErrstr());
                return robotTreeRespDtoResultModel;
            }
        } catch (Exception e) {
            log.error("RobotTreeController deleteTreeInfo:", e);
            robotTreeRespDtoResultModel.setSuccessed(false);
            robotTreeRespDtoResultModel.setErrorCode("1");
            robotTreeRespDtoResultModel.setErrorDesc("删除失败");
        }
        return robotTreeRespDtoResultModel;
    }

    @PostMapping("updateTreeJump")
    public ResultModel<RobotTreeRespDto> updateTreeJump(@RequestBody RobotTreeJumpReqDo robotTreeJumpReqDo) {
        if (robotTreeJumpReqDo.getRobotid() == null) {
            throw new AIValidateException("机器人编号不能为空");
        }

        if (robotTreeJumpReqDo.getTreeid() == null) {
            throw new AIValidateException("节点编号不能为空");
        }

        ResultModel<RobotTreeRespDto> robotTreeRespDtoResultModel = new ResultModel<>();

        try {
            RobotTreeRespDto robotTreeRespDto = robotTreeService.updateTreeJump(robotTreeJumpReqDo);
            if (!String.valueOf(0).equals(robotTreeRespDto.getErrno())) {
                robotTreeRespDtoResultModel.setSuccessed(false);
                robotTreeRespDtoResultModel.setErrorCode(robotTreeRespDto.getErrno());
                robotTreeRespDtoResultModel.setErrorDesc(robotTreeRespDto.getErrstr());
                return robotTreeRespDtoResultModel;
            }
        } catch (Exception e) {
            log.error("RobotTreeController updateTreeJump:", e);
            robotTreeRespDtoResultModel.setSuccessed(false);
            robotTreeRespDtoResultModel.setErrorCode("1");
            robotTreeRespDtoResultModel.setErrorDesc("修改失败");
        }
        return robotTreeRespDtoResultModel;
    }

    @PostMapping("deleteTreeJump")
    public ResultModel<RobotTreeRespDto> deleteTreeJump(RobotTreeReqDto robotTreeReqDto) {
        checkParams(robotTreeReqDto);
        if (robotTreeReqDto.getJumpid() == null) {
            throw new AIValidateException("跳转体编号不能为空");
        }
        ResultModel<RobotTreeRespDto> robotTreeRespDtoResultModel = new ResultModel<>();

        try {
            RobotTreeRespDto robotTreeRespDto = robotTreeService.deleteTreeJump(robotTreeReqDto);
            if (!String.valueOf(0).equals(robotTreeRespDto.getErrno())) {
                robotTreeRespDtoResultModel.setSuccessed(false);
                robotTreeRespDtoResultModel.setErrorCode(robotTreeRespDto.getErrno());
                robotTreeRespDtoResultModel.setErrorDesc(robotTreeRespDto.getErrstr());
                return robotTreeRespDtoResultModel;
            }
        } catch (Exception e) {
            log.error("RobotTreeController updateTreeJump:", e);
            robotTreeRespDtoResultModel.setSuccessed(false);
            robotTreeRespDtoResultModel.setErrorCode("1");
            robotTreeRespDtoResultModel.setErrorDesc("删除失败");
        }
        return robotTreeRespDtoResultModel;
    }

    @PostMapping("sortTreeJump")
    public ResultModel<RobotTreeRespDto> sortTreeJump(@RequestBody RobotTreeReqDto robotTreeReqDto) {
        checkParams(robotTreeReqDto);
        ResultModel<RobotTreeRespDto> robotTreeRespDtoResultModel = new ResultModel<>();
        try {
            RobotTreeRespDto robotTreeRespDto = robotTreeService.sortTreeJump(robotTreeReqDto);
            if (!String.valueOf(0).equals(robotTreeRespDto.getErrno())) {
                robotTreeRespDtoResultModel.setSuccessed(false);
                robotTreeRespDtoResultModel.setErrorCode(robotTreeRespDto.getErrno());
                robotTreeRespDtoResultModel.setErrorDesc(robotTreeRespDto.getErrstr());
                return robotTreeRespDtoResultModel;
            }
        } catch (Exception e) {
            log.error("RobotTreeController sortTreeJump:", e);
            robotTreeRespDtoResultModel.setSuccessed(false);
            robotTreeRespDtoResultModel.setErrorCode("1");
            robotTreeRespDtoResultModel.setErrorDesc("排序失败");
        }
        return robotTreeRespDtoResultModel;
    }

    private void checkParams(RobotTreeReqDto robotTreeReqDto) {
        if (robotTreeReqDto.getRobotid() == null) {
            throw new AIValidateException("机器人编号不能为空");
        }

        if (robotTreeReqDto.getTreeid() == null) {
            throw new AIValidateException("节点编号不能为空");
        }
    }

    private ResultModel<RobotTreeRespDto.TreeLsInfoDo> buildResultData(ResultModel<RobotTreeRespDto.TreeLsInfoDo> robotTreeRespDtoResultModel, RobotTreeRespDto robotTreeRespDto) {
        if (!String.valueOf(0).equals(robotTreeRespDto.getErrno())) {
            robotTreeRespDtoResultModel.setSuccessed(false);
            robotTreeRespDtoResultModel.setErrorCode(robotTreeRespDto.getErrno());
            robotTreeRespDtoResultModel.setErrorDesc(robotTreeRespDto.getErrstr());
            return robotTreeRespDtoResultModel;
        }
        robotTreeRespDtoResultModel.setReturnValue(robotTreeRespDto.getData());
        return robotTreeRespDtoResultModel;
    }

    public RobotTreeController(RobotTreeService robotTreeService) {
        this.robotTreeService = robotTreeService;
    }
}
