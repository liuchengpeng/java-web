package com.example.demo.controller;

import com.example.demo.model.entityDo.AiAgentDo;
import com.example.demo.model.entityDto.AiAgentDto;
import com.example.demo.model.robotDto.RobotEntityReqDto;
import com.example.demo.model.robotDto.RobotEntityRespDto;
import com.example.demo.service.service.AiAgentService;
import com.example.demo.util.common.AIManagerPage;
import com.example.demo.util.common.exception.AIValidateException;
import com.example.demo.util.common.vo.ResultModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 场景管理
 */
@RestController
@Slf4j
@RequestMapping("/aiAgent")
public class AiAgentController {

    private final AiAgentService aiAgentService;


    public AiAgentController(AiAgentService aiAgentService) {
        this.aiAgentService = aiAgentService;
    }

    /**
     * 添加场景
     *
     * @param aiAgent
     * @param request
     * @param response
     * @return
     */
    @PostMapping("/add")
    public ResultModel<AiAgentDo> addAiAgent(AiAgentDo aiAgent, HttpServletRequest request, HttpServletResponse response) {
        ResultModel<AiAgentDo> resultModel = new ResultModel<>();
        try {
            resultModel = aiAgentService.addAiAgent(aiAgent, request, response);
        } catch (AIValidateException e) {
            log.error("添加场景异常", e);
            resultModel.setErrorCode(e.getCode());
            resultModel.setSuccessed(false);
        }
        return resultModel;
    }

    /**
     * 编辑场景
     *
     * @param aiAgent
     * @param request
     * @param response
     * @return
     */
    @PostMapping("/update")
    public ResultModel<AiAgentDo> updateAiAgent(AiAgentDo aiAgent, HttpServletRequest request, HttpServletResponse response) {
        ResultModel<AiAgentDo> resultModel = new ResultModel<>();
        try {
            resultModel = aiAgentService.updateAiAgent(aiAgent, request, response);
        } catch (AIValidateException e) {
            log.error("编辑场景异常", e);
            resultModel.setErrorCode(e.getCode());
            resultModel.setSuccessed(false);
        }
        return resultModel;
    }

    /**
     * 删除场景
     *
     * @param aiAgent
     * @param request
     * @param response
     * @return
     */
    @PostMapping("/delete")
    public ResultModel<AiAgentDo> deleteAiAgent(AiAgentDo aiAgent, HttpServletRequest request, HttpServletResponse response) {
        ResultModel<AiAgentDo> resultModel = new ResultModel<>();
        try {
            resultModel = aiAgentService.deleteAiAgent(aiAgent, request, response);
        } catch (AIValidateException e) {
            log.error("删除场景异常", e);
            resultModel.setErrorCode(e.getCode());
            resultModel.setSuccessed(false);
        }
        return resultModel;
    }

    /**
     * 查询场景列表
     *
     * @param aiAgent
     * @param request
     * @param response
     * @return
     */
    @PostMapping("/queryList")
    public ResultModel<AIManagerPage> queryListAiAgent(AiAgentDto aiAgent, HttpServletRequest request, HttpServletResponse response) {
        ResultModel<AIManagerPage> resultModel = new ResultModel<>();
        try {
            resultModel = aiAgentService.queryListAiAgent(aiAgent, request, response);
        } catch (AIValidateException e) {
            log.error("查询场景列表异常", e);
            resultModel.setErrorCode(e.getCode());
            resultModel.setSuccessed(false);
        }
        return resultModel;
    }

    @GetMapping("queryListRobotEntity")
    public ResultModel<RobotEntityRespDto.NewRobotEntityData> queryListRobotEntity(RobotEntityReqDto robotEntityReqDto) {
        if (robotEntityReqDto.getRobotid() == null) {
            throw new AIValidateException("机器人编号不能为空");
        }

        ResultModel<RobotEntityRespDto.NewRobotEntityData> robotEntityRespDtoResultModel = new ResultModel<>();
        try {
            RobotEntityRespDto robotEntityRespDto = aiAgentService.getRobotEntityList(robotEntityReqDto);
            if (!String.valueOf(0).equals(robotEntityRespDto.getErrno())) {
                robotEntityRespDtoResultModel.setErrorDesc(robotEntityRespDto.getErrstr());
                robotEntityRespDtoResultModel.setErrorCode(robotEntityRespDto.getErrno());
                robotEntityRespDtoResultModel.setSuccessed(false);
                return robotEntityRespDtoResultModel;
            }
            robotEntityRespDtoResultModel.setReturnValue(robotEntityRespDto.getData());
        } catch (AIValidateException e) {
            log.error("查看机器人实体列表接口异常：", e);
            robotEntityRespDtoResultModel.setErrorCode(e.getCode());
            robotEntityRespDtoResultModel.setSuccessed(false);
        }
        return robotEntityRespDtoResultModel;
    }

    @PostMapping("updateEntityContent")
    public ResultModel<RobotEntityRespDto.NewRobotEntityData> updateEntityContent(@RequestBody RobotEntityReqDto robotEntityReqDto) {
        if (robotEntityReqDto.getRobotid() == null) {
            throw new AIValidateException("机器人编号不能为空");
        }

        if (robotEntityReqDto.getEntityid() == null) {
            throw new AIValidateException("实体编号不能为空");
        }

        ResultModel<RobotEntityRespDto.NewRobotEntityData> robotEntityRespDtoResultModel = new ResultModel<>();
        try {
            RobotEntityRespDto robotEntityRespDto = aiAgentService.updateEntityContent(robotEntityReqDto);
            processResult(robotEntityRespDtoResultModel, robotEntityRespDto);
        } catch (Exception e) {
            log.error("实体值修改:", e);
            robotEntityRespDtoResultModel.setSuccessed(false);
            robotEntityRespDtoResultModel.setErrorCode("1");
            robotEntityRespDtoResultModel.setErrorDesc("修改失败");
        }
        return robotEntityRespDtoResultModel;
    }

    private void processResult(ResultModel<RobotEntityRespDto.NewRobotEntityData> robotEntityRespDtoResultModel, RobotEntityRespDto robotEntityRespDto) {
        if (!String.valueOf(0).equals(robotEntityRespDto.getErrno())) {
            robotEntityRespDtoResultModel.setSuccessed(false);
            robotEntityRespDtoResultModel.setErrorCode(robotEntityRespDto.getErrno());
            robotEntityRespDtoResultModel.setErrorDesc(robotEntityRespDto.getErrstr());
        } else {
            robotEntityRespDtoResultModel.setReturnValue(robotEntityRespDto.getData());
        }
    }
}
