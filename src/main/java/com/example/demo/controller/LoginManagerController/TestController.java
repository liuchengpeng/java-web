package com.example.demo.controller.LoginManagerController;

import lombok.extern.log4j.Log4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Log4j
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class TestController {
    @RequestMapping("/")
    public String home() {
        return "我是主路径，任何人都可以访问";
    }

    @RequestMapping("/hello")
    public String hello() {
        return "我是hello";
    }


    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping("/roleAuth")
    public String role() {
        return "我是role";
    }

    public static void main(String[] args) {

    }

}
