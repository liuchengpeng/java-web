package com.example.demo.controller.LoginManagerController;

import com.example.demo.model.entityDto.AiUserDto;
import com.example.demo.securitys.securityProperites.MySecurityProperties;
import com.example.demo.service.service.AIUserService;
import com.example.demo.util.common.custom.AICustomUrl;
import com.example.demo.util.common.vo.SimpleResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@RestController
@Slf4j
public class LoginController {

  @Autowired
  private HttpServletRequest request;
  @Autowired
  private HttpServletResponse response;
  @Autowired
  private AIUserService aIUserService;
  @Autowired
  private MySecurityProperties mySecurityProperties;
  // 原请求信息的缓存及恢复
  private RequestCache requestCache = new HttpSessionRequestCache();
  // 用于重定向
  private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

  /**
   * 当需要身份认证时跳转到这里
   *
   * @param request
   * @param response
   * @return
   * @throws IOException
   */
  @RequestMapping(AICustomUrl.DEFAULT_UNAUTHENTICATION_URL)
  @ResponseStatus(code = HttpStatus.UNAUTHORIZED) //返回状态码
  public SimpleResponse requireAuthentication(HttpServletRequest request, HttpServletResponse response) throws IOException {
    // 判断引发跳转的是html,还是数据请求
    log.info("mySecurityProperties.getLoginPage() = " + mySecurityProperties.getLoginPage());
    SavedRequest savedRequest = requestCache.getRequest(request, response);
    if (savedRequest != null) {
      String targetUrl = savedRequest.getRedirectUrl();
      log.info("引发跳转的请求是，targetUrl = " + targetUrl);
      if (StringUtils.endsWithIgnoreCase(targetUrl, ".html")) {
        // 跳转到用户配置的登录页面
        redirectStrategy.sendRedirect(request, response, mySecurityProperties.getLoginPage());
//        redirectStrategy.sendRedirect(request, response, url);
      }
    }
    return new SimpleResponse("访问的服务需要身份验证，请引导用户到登录页");
  }


  /**
   * 组装Dto参数
   *
   * @return
   */
  public AiUserDto AssembleDto() {
    AiUserDto aiUserDto = new AiUserDto();
    String userName = request.getParameter("userName");
    String passWord = request.getParameter("passWord");
    aiUserDto.setUsername(userName);
    aiUserDto.setPassword(passWord);
    return aiUserDto;
  }
}
