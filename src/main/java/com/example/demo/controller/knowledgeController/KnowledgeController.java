package com.example.demo.controller.knowledgeController;

import com.example.demo.model.entityDto.knowDto.KnowledgeDto;
import com.example.demo.model.entityDto.knowDto.KnowledgeDtoExt;
import com.example.demo.service.service.KnowledgeService;
import com.example.demo.util.common.exception.AIValidateException;
import com.example.demo.util.common.json.JSONUtils;
import com.example.demo.util.common.string.StringUtil;
import com.example.demo.util.common.vo.ResultModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/knowledge")
public class KnowledgeController {
    @Autowired
    HttpServletRequest request;
    @Autowired
    HttpServletResponse response;
    @Autowired
    private KnowledgeService knowledgeService;

    /**
     * 新增一个知识库
     * @return
     */
    @PostMapping("/createOrUpdateKnowledge")
    public Object createOrUpdateKnowledge() {
        ResultModel<KnowledgeDto> resultModel = new ResultModel<KnowledgeDto>();
        try {
            KnowledgeDto knowledgeDto = AssembleDto();
            KnowledgeDto resultKnowledgeDto = knowledgeService.createOrUpdateKnowledge(knowledgeDto);
            resultModel.setReturnValue(resultKnowledgeDto);
        } catch (AIValidateException e) {
            resultModel.setErrorCode(e.getCode());
            resultModel.setSuccessed(false);
        }
        return resultModel;
    }

    /**
     * 删除知识库
     * @return
     */
    @PostMapping("/deleteKnowledge")
    public Object deleteKnowledge() {
        ResultModel<KnowledgeDto> resultModel = new ResultModel<KnowledgeDto>();
        try {
            KnowledgeDto knowledgeDto = AssembleDto();
            KnowledgeDto resultKnowledgeDto = knowledgeService.deleteKnowledge(knowledgeDto);
            resultModel.setReturnValue(resultKnowledgeDto);
        } catch (AIValidateException e) {
            resultModel.setErrorCode(e.getCode());
            resultModel.setSuccessed(false);
        }
        return resultModel;
    }

    /**
     * 查询知识库列表
     *
     * @return
     */
    @PostMapping("/queryList")
    public Object queryList() {
        ResultModel<KnowledgeDto> resultModel = new ResultModel<KnowledgeDto>();
        try {
            KnowledgeDto knowledgeDto = AssembleDto();
            KnowledgeDto resultKnowledgeDto = knowledgeService.queryList(knowledgeDto);
            resultModel.setReturnValue(resultKnowledgeDto);
        } catch (AIValidateException e) {
            resultModel.setErrorCode(e.getCode());
            resultModel.setSuccessed(false);
        }
        return resultModel;
    }

    /**
     * 添加或更新一个知识词条
     *
     * @return
     */
    @PostMapping("/addOrUpdateKnowEntry")
    public Object addOrUpdateKnowEntry() {
        ResultModel<KnowledgeDto> resultModel = new ResultModel<KnowledgeDto>();
        try {
            KnowledgeDto knowledgeDto = AssembleDto();
            KnowledgeDto resultknowledgeDto = knowledgeService.addOrUpdateKnowEntry(knowledgeDto);
            resultModel.setReturnValue(resultknowledgeDto);
        } catch (AIValidateException e) {
            resultModel.setErrorCode(e.getCode());
            resultModel.setSuccessed(false);
        }
        return resultModel;
    }


    /**
     * 获取一个知识词条
     *
     * @return
     */
    @PostMapping("/getOneKnow")
    public Object getOneKnow() {
        ResultModel<KnowledgeDto> resultModel = new ResultModel<KnowledgeDto>();
        try {
            KnowledgeDto knowledgeDto = AssembleDto();
            KnowledgeDto resultKnowledgeDto = knowledgeService.getOneKnow(knowledgeDto);
            resultModel.setReturnValue(resultKnowledgeDto);
        } catch (AIValidateException e) {
            resultModel.setErrorCode(e.getCode());
            resultModel.setSuccessed(false);
        }
        return resultModel;
    }

    /**
     * 知识库词条列表，分页
     *
     * @return
     */
    @PostMapping("/listKnowPage")
    public Object listKnowPage() {
        ResultModel<KnowledgeDto> resultModel = new ResultModel<KnowledgeDto>();
        try {
            KnowledgeDto knowledgeDto = AssembleDto();
            KnowledgeDto resultKnowledgeDto = knowledgeService.listKnowPage(knowledgeDto);
            resultModel.setReturnValue(resultKnowledgeDto);
        } catch (AIValidateException e) {
            resultModel.setErrorCode(e.getCode());
            resultModel.setSuccessed(false);
        }
        return resultModel;
    }


    /**
     * 删除一个知识词条
     *
     * @return
     */
    @PostMapping("/delknow")
    public Object delknow() {
        ResultModel<KnowledgeDto> resultModel = new ResultModel<KnowledgeDto>();
        try {
            KnowledgeDto knowledgeDto = AssembleDto();
            KnowledgeDto resultKnowledgeDto = knowledgeService.delknow(knowledgeDto);
            resultModel.setReturnValue(resultKnowledgeDto);
        } catch (AIValidateException e) {
            resultModel.setErrorCode(e.getCode());
            resultModel.setSuccessed(false);
        }
        return resultModel;
    }


    /**
     * 建立/修改 robot 与知识库的绑定关系
     *
     * @return
     */
    @PostMapping("/knowRobotBandrela")
    public Object knowRobotBandrela() {
        ResultModel<KnowledgeDto> resultModel = new ResultModel<KnowledgeDto>();
        try {
            KnowledgeDto knowledgeDto = AssembleDto();
            KnowledgeDto resultKnowledgeDto = knowledgeService.knowRobotBandrela(knowledgeDto);
            resultModel.setReturnValue(resultKnowledgeDto);
        } catch (AIValidateException e) {
            resultModel.setErrorCode(e.getCode());
            resultModel.setSuccessed(false);
        }
        return resultModel;
    }

    /**
     * 解除robot与知识库的绑定关系
     *
     * @return
     */
    @PostMapping("/knowRobotUnBandrela")
    public Object knowRobotUnBandrela() {
        ResultModel<KnowledgeDto> resultModel = new ResultModel<KnowledgeDto>();
        try {
            KnowledgeDto knowledgeDto = AssembleDto();
            KnowledgeDto resultKnowledgeDto = knowledgeService.knowRobotUnBandrela(knowledgeDto);
            resultModel.setReturnValue(resultKnowledgeDto);
        } catch (AIValidateException e) {
            resultModel.setErrorCode(e.getCode());
            resultModel.setSuccessed(false);
        }
        return resultModel;
    }

    /**
     * 列出已robot绑定的知识库
     *
     * @return
     */
    @PostMapping("/listKnowRobotBandrela")
    public Object listKnowRobotBandrela() {
        ResultModel<KnowledgeDto> resultModel = new ResultModel<KnowledgeDto>();
        try {
            KnowledgeDto knowledgeDto = AssembleDto();
            KnowledgeDto resultKnowledgeDto = knowledgeService.listKnowRobotBandrela(knowledgeDto);
            resultModel.setReturnValue(resultKnowledgeDto);
        } catch (AIValidateException e) {
            resultModel.setErrorCode(e.getCode());
            resultModel.setSuccessed(false);
        }
        return resultModel;
    }

    /**
     * 组装Dto参数
     *
     * @return
     */
    public KnowledgeDto AssembleDto() {
        KnowledgeDto knowledgeDto = new KnowledgeDto();

        String name = request.getParameter("name");
        String tenantid = request.getParameter("tenantid");
        String knowcateid = request.getParameter("knowcateid");
        String knowid = request.getParameter("knowid");
        String robotid = request.getParameter("robotid");
        String isopen = request.getParameter("isopen");
        String threshold = request.getParameter("threshold");
        String question = request.getParameter("question");
        String regex = request.getParameter("regex");
        String page = request.getParameter("page");
        String pagesize = request.getParameter("pagesize");
        knowledgeDto.setName(name);
        knowledgeDto.setTenantid(tenantid);
        knowledgeDto.setKnowcateid(knowcateid);
        knowledgeDto.setKnowid(knowid);
        knowledgeDto.setRobotid(robotid);
        knowledgeDto.setIsopen(isopen);
        knowledgeDto.setThreshold(threshold);
        knowledgeDto.setPage(page);
        knowledgeDto.setPagesize(pagesize);
        if (StringUtil.isNotBlank(question)) {
            knowledgeDto.setQuestion(Arrays.asList(question.split(",")));
        }
        if (StringUtil.isNotBlank(regex)) {
            knowledgeDto.setRegex(Arrays.asList(regex.split(",")));
        }
        //默认当answer=1时，为添加或更新知识词条接口（与前端约定）
        if (StringUtil.equals("1", request.getParameter("answer"))) {
            //是一个数组对象KnowledgeDto，需要转换为对应List<KnowledgeDto> 对象
            String actionStrArr = request.getParameter("actionStrArr");
            if (StringUtil.isNotBlank(actionStrArr)) {
                log.info("actionStrArr = " + actionStrArr);
                List actions = JSONUtils.jsonToList(actionStrArr, KnowledgeDtoExt.class);
                knowledgeDto.setAnswer(actions);
            }
        }

        return knowledgeDto;
    }


}
