package com.example.demo.controller.userManagement;

import com.alibaba.fastjson.JSONObject;
import com.example.demo.model.entityDto.AiAuthGroupDto;
import com.example.demo.service.service.AiAuthGroupService;
import com.example.demo.util.common.enumClass.DataOperTypeEnum;
import com.example.demo.util.common.exception.AIValidateException;
import com.example.demo.util.common.vo.ResultModel;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @author le.gao
 */
@RestController
@RequestMapping("/aiAuthGroup")
@Slf4j
public class AiAuthGroupController {

    private final AiAuthGroupService aiAuthGroupService;

    @GetMapping("getAllAiAuthGroup")
    public ResultModel<AiAuthGroupDto> getAllAiAuthGroup() {
        ResultModel<AiAuthGroupDto> aiAuthGroupDtoResultModel = new ResultModel<>();
        try {
            List<AiAuthGroupDto> authGroupDtoList = aiAuthGroupService.getAllAiAuthGroup();
            AiAuthGroupDto aiAuthGroupDto = new AiAuthGroupDto();
            aiAuthGroupDto.setAiAuthGroupDtoList(authGroupDtoList);
            aiAuthGroupDtoResultModel.setReturnValue(aiAuthGroupDto);
        } catch (Exception e) {
            log.error("AiAuthGroupServiceImpl getAllAiAuthGroup error:", e);
            aiAuthGroupDtoResultModel.setSuccessed(false);
            aiAuthGroupDtoResultModel.setErrorCode("1");
            aiAuthGroupDtoResultModel.setErrorDesc("查询列表失败");
        }
        return aiAuthGroupDtoResultModel;
    }

    @GetMapping("updateAiAuthGroupStatus")
    public ResultModel<AiAuthGroupDto> updateAiAuthGroupStatus(@RequestParam @NonNull Integer id, @RequestParam @NonNull Integer status) {
        ResultModel<AiAuthGroupDto> aiAuthGroupDtoResultModel = new ResultModel<>();
        try {
            aiAuthGroupService.updateAiAuthGroupStatus(id, status);
        } catch (Exception e) {
            log.error("AiAuthGroupServiceImpl updateAiAuthGroupStatus error:", e);
            aiAuthGroupDtoResultModel.setSuccessed(false);
            aiAuthGroupDtoResultModel.setErrorCode("1");
            aiAuthGroupDtoResultModel.setErrorDesc("状态修改失败");
        }
        return aiAuthGroupDtoResultModel;
    }

    @GetMapping("addAiAuthGroup")
    public ResultModel<AiAuthGroupDto> addAiAuthGroup(@RequestParam String title) {
        ResultModel<AiAuthGroupDto> aiAuthGroupDtoResultModel = new ResultModel<>();
        boolean checkAiAuthGroupTitleIsExist = checkAiAuthGroupTitleIsExist(null, title, DataOperTypeEnum.SAVE.getOperType());
        if (checkAiAuthGroupTitleIsExist) {
            aiAuthGroupDtoResultModel.setSuccessed(false);
            aiAuthGroupDtoResultModel.setErrorCode("1");
            aiAuthGroupDtoResultModel.setErrorDesc("角色名已存在");
            return aiAuthGroupDtoResultModel;
        }

        try {
            aiAuthGroupService.addAiAuthGroup(title);
        } catch (Exception e) {
            log.error("AiAuthGroupServiceImpl addAiAuthGroup error:", e);
            aiAuthGroupDtoResultModel.setSuccessed(false);
            aiAuthGroupDtoResultModel.setErrorCode("1");
            aiAuthGroupDtoResultModel.setErrorDesc("添加失败");
        }

        return aiAuthGroupDtoResultModel;
    }

    private boolean checkAiAuthGroupTitleIsExist(Integer id, String title, String type) throws AIValidateException {
        AiAuthGroupDto aiAuthGroupDto = aiAuthGroupService.checkAiAuthGroupTitleIsExist(title);
        if (DataOperTypeEnum.UPDATE.getOperType().equals(type) && aiAuthGroupDto != null && aiAuthGroupDto.getId().equals(id) && title.equals(aiAuthGroupDto.getTitle())) {
            return false;
        }

        return aiAuthGroupDto != null;
    }

    @PostMapping("updateAiAuthGroup")
    public ResultModel<AiAuthGroupDto> updateAiAuthGroup(@RequestParam @NonNull Integer id, @RequestParam @NonNull String title, @RequestParam @NonNull Integer status) {
        ResultModel<AiAuthGroupDto> aiAuthGroupDtoResultModel = new ResultModel<>();
        boolean checkAiAuthGroupTitleIsExist = checkAiAuthGroupTitleIsExist(id, title, DataOperTypeEnum.UPDATE.getOperType());
        if (checkAiAuthGroupTitleIsExist) {
            aiAuthGroupDtoResultModel.setSuccessed(false);
            aiAuthGroupDtoResultModel.setErrorCode("1");
            aiAuthGroupDtoResultModel.setErrorDesc("角色名已存在");
            return aiAuthGroupDtoResultModel;
        }

        try {
            aiAuthGroupService.updateAiAuthGroup(id, title, status);
        } catch (Exception e) {
            log.error("AiAuthGroupController updateAiAuthGroup:", e);
            aiAuthGroupDtoResultModel.setSuccessed(false);
            aiAuthGroupDtoResultModel.setErrorCode("1");
            aiAuthGroupDtoResultModel.setErrorDesc("编辑失败");
        }
        return aiAuthGroupDtoResultModel;
    }

    @GetMapping("deleteAiAuthGroup")
    public ResultModel<AiAuthGroupDto> deleteAiAuthGroup(@RequestParam @NonNull Integer id) {
        ResultModel<AiAuthGroupDto> aiAuthGroupDtoResultModel = new ResultModel<>();
        try {
            aiAuthGroupService.deleteAiAuthGroup(id);
        } catch (Exception e) {
            log.error("AiAuthGroupController deleteAiAuthGroup error:", e);
            aiAuthGroupDtoResultModel.setSuccessed(false);
            aiAuthGroupDtoResultModel.setErrorCode("1");
            aiAuthGroupDtoResultModel.setErrorDesc("删除失败");
        }
        return aiAuthGroupDtoResultModel;
    }

    @GetMapping("findUserAuthorizationInfo")
    public ResultModel<JSONObject> findUserAuthorizationInfo(@RequestParam @NonNull Integer id) {
        ResultModel<JSONObject> jsonObjectResultModel = new ResultModel<>();
        try {
            JSONObject returnJson = aiAuthGroupService.findUserAuthorizationInfo(id);
            jsonObjectResultModel.setReturnValue(returnJson);
        } catch (Exception e) {
            log.error("AiAuthGroupController findUserAuthorizationInfo error:", e);
            jsonObjectResultModel.setSuccessed(false);
            jsonObjectResultModel.setErrorCode("1");
            jsonObjectResultModel.setErrorDesc("获取授权列表失败");
        }
        return jsonObjectResultModel;
    }

    @GetMapping("commitUserAuthorizationInfo")
    public ResultModel<AiAuthGroupDto> commitUserAuthorizationInfo(@RequestParam @NonNull Integer id, String ruleIds) {
        ResultModel<AiAuthGroupDto> aiAuthGroupDtoResultModel = new ResultModel<>();
        if (StringUtils.isBlank(ruleIds)) {
            return aiAuthGroupDtoResultModel;
        }

        try {
            aiAuthGroupService.commitUserAuthorizationInfo(id, ruleIds);
        } catch (Exception e) {
            log.error("AiAuthGroupController commitUserAuthorizationInfo error:", e);
            aiAuthGroupDtoResultModel.setSuccessed(false);
            aiAuthGroupDtoResultModel.setErrorCode("1");
            aiAuthGroupDtoResultModel.setErrorDesc("授权失败");
        }
        return aiAuthGroupDtoResultModel;
    }

    public AiAuthGroupController(AiAuthGroupService aiAuthGroupService) {
        this.aiAuthGroupService = aiAuthGroupService;
    }
}
