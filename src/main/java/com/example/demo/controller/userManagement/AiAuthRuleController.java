package com.example.demo.controller.userManagement;

import com.example.demo.model.entityDo.AiAuthRuleDo;
import com.example.demo.model.entityDto.AiAuthRuleDto;
import com.example.demo.service.service.AiAuthRuleService;
import com.example.demo.util.common.vo.ResultModel;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@Log4j
@RequestMapping("/aiAuthRule")
public class AiAuthRuleController {

    @Autowired
    private AiAuthRuleService aiAuthRuleService;

    @ResponseBody
    @RequestMapping("/queryList")
    public ResultModel<AiAuthRuleDto> queryList(AiAuthRuleDto aiAuthRuleDto, HttpServletRequest request, HttpServletResponse response){
        ResultModel<AiAuthRuleDto> ruleDtoResultModel = new ResultModel<>();
        ruleDtoResultModel = aiAuthRuleService.queryList(aiAuthRuleDto,request,response);
        return ruleDtoResultModel;
    }

    @ResponseBody
    @RequestMapping("/update")
    public ResultModel<AiAuthRuleDto> update(AiAuthRuleDo aiAuthRuleDo, HttpServletRequest request, HttpServletResponse response){
        ResultModel<AiAuthRuleDto> ruleDtoResultModel = new ResultModel<>();
        ruleDtoResultModel = aiAuthRuleService.update(aiAuthRuleDo,request,response);
        return ruleDtoResultModel;
    }

    @ResponseBody
    @RequestMapping("/insert")
    public ResultModel<AiAuthRuleDto> insert(AiAuthRuleDo aiAuthRuleDo, HttpServletRequest request, HttpServletResponse response){
        ResultModel<AiAuthRuleDto> ruleDtoResultModel = new ResultModel<>();
        ruleDtoResultModel = aiAuthRuleService.insert(aiAuthRuleDo,request,response);
        return ruleDtoResultModel;
    }

    @ResponseBody
    @RequestMapping("/detele")
    public ResultModel<AiAuthRuleDto> detele(Integer id, HttpServletRequest request, HttpServletResponse response){
        ResultModel<AiAuthRuleDto> ruleDtoResultModel = new ResultModel<>();
        ruleDtoResultModel = aiAuthRuleService.detele(id,request,response);
        return ruleDtoResultModel;
    }
}
