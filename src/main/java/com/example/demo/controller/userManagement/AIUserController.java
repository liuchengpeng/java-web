package com.example.demo.controller.userManagement;

import com.alibaba.fastjson.JSONObject;
import com.example.demo.model.entityDto.AiUserDto;
import com.example.demo.service.service.AIUserService;
import com.example.demo.util.common.AIManagerPage;
import com.example.demo.util.common.enumClass.DataOperTypeEnum;
import com.example.demo.util.common.exception.AIValidateException;
import com.example.demo.util.common.vo.ResultModel;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.log4j.Log4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author cheng
 */

@RestController
@Log4j
@RequestMapping("/aiUser")
public class AIUserController {
    @Autowired
    private HttpServletRequest request;
    @Autowired
    private AIUserService aIUserService;

    @RequestMapping("/test")
    public String test() {
        log.info("124");
        log.info("125");
        log.info("126");

        return "duan dian wu xiao 666666666666666666666";
    }


    /**
     * @param pageNum  当前页码
     * @param pageSize 每页总数
     * @return
     * @throws AIValidateException
     */
    @PostMapping("/getUserLists")
    public ResultModel<AIManagerPage> getUserLists(@RequestParam(value = "pageNum", defaultValue = "1") Integer pageNum,
                                                   @RequestParam(value = "pageSize", defaultValue = "10") Integer pageSize) throws AIValidateException {
        ResultModel<AIManagerPage> resultModel = new ResultModel<AIManagerPage>();
        try {
            PageHelper.startPage(pageNum, pageSize);
            AiUserDto resultAiUserDto = aIUserService.getAiUserLists();
            PageInfo<AiUserDto> aiUserPageInfo = new PageInfo<>(resultAiUserDto.getAiUserDtos());
            List<AiUserDto> pageList = aiUserPageInfo.getList();
            resultAiUserDto.setAiUserDtos(pageList);
            AIManagerPage<AiUserDto> aiUserDtoAIManagerPage = new AIManagerPage<AiUserDto>(pageSize, pageNum, (int) aiUserPageInfo.getTotal(), resultAiUserDto);
            resultModel.setReturnValue(aiUserDtoAIManagerPage);
        } catch (AIValidateException e) {
            log.error("AIUserController.getUserLists.{}", e);
            resultModel.setErrorCode(e.getCode());
            resultModel.setSuccessed(false);
        }
        return resultModel;
    }

    @PostMapping("addAiUser")
    public ResultModel<AiUserDto> addAiUser(@RequestBody JSONObject reqJson) {
        AiUserDto aiUserDto = JSONObject.toJavaObject(reqJson, AiUserDto.class);
        checkParams(aiUserDto);
        ResultModel<AiUserDto> aiUserDtoResultModel = new ResultModel<>();
        //校验用户名是否重复
        boolean userNameIsExist = checkPasswordIsExist(aiUserDto.getId(), aiUserDto.getUsername(), DataOperTypeEnum.SAVE.getOperType());
        if (userNameIsExist) {
            aiUserDtoResultModel.setSuccessed(false);
            aiUserDtoResultModel.setErrorCode("1");
            aiUserDtoResultModel.setErrorDesc("用户名已存在");
            return aiUserDtoResultModel;
        }

        try {
            aIUserService.addAiUser(aiUserDto);
        } catch (Exception e) {
            log.error("AIUserServiceImpl addAiUser error:", e);
            aiUserDtoResultModel.setSuccessed(false);
            aiUserDtoResultModel.setErrorCode("1");
            aiUserDtoResultModel.setErrorDesc("添加用户失败");
        }
        return aiUserDtoResultModel;
    }

    @GetMapping("showAiUser")
    public ResultModel<AiUserDto> showAiUser(@RequestParam Integer id) {
        ResultModel<AiUserDto> aiUserDtoResultModel = aIUserService.showAiUser(id);
        aiUserDtoResultModel.getReturnValue().setPassword(null);
        return aiUserDtoResultModel;
    }

    @PostMapping("updateAiUser")
    public ResultModel<AiUserDto> updateAiUser(@RequestBody JSONObject reqJson) {
        AiUserDto aiUserDto = JSONObject.toJavaObject(reqJson, AiUserDto.class);
        checkParams(aiUserDto);
        ResultModel<AiUserDto> aiUserDtoResultModel = new ResultModel<>();
        //校验用户名是否重复
        boolean userNameIsExist = checkPasswordIsExist(aiUserDto.getId(), aiUserDto.getUsername(), DataOperTypeEnum.UPDATE.getOperType());
        if (userNameIsExist) {
            aiUserDtoResultModel.setSuccessed(false);
            aiUserDtoResultModel.setErrorCode("1");
            aiUserDtoResultModel.setErrorDesc("用户名已存在");
            return aiUserDtoResultModel;
        }

        try {
            aIUserService.updateAiUser(aiUserDto);
        } catch (Exception e) {
            log.error("AIUserServiceImpl updateAiUser error:", e);
            aiUserDtoResultModel.setSuccessed(false);
            aiUserDtoResultModel.setErrorCode("1");
            aiUserDtoResultModel.setErrorDesc("修改失败");
        }

        return aiUserDtoResultModel;
    }

    @GetMapping("deleteAiUser")
    public ResultModel<AiUserDto> deleteAiUser(@RequestParam Integer id) {
        ResultModel<AiUserDto> aiUserDtoResultModel = new ResultModel<>();
        try {
            aIUserService.deleteAiUser(id);
        } catch (Exception e) {
            aiUserDtoResultModel.setSuccessed(false);
            aiUserDtoResultModel.setErrorCode("1");
            aiUserDtoResultModel.setErrorDesc("删除失败");
        }
        return aiUserDtoResultModel;
    }

    @GetMapping("batchDeleteAiUser")
    public ResultModel<AiUserDto> batchDeleteAiUser(@RequestParam String aiUserIds) {
        ResultModel<AiUserDto> aiUserDtoResultModel = new ResultModel<>();
        try {
            aIUserService.batchDeleteAiUser(aiUserIds);
        } catch (Exception e) {
            log.error("AIUserServiceImpl batchDeleteAiUser error:", e);
            aiUserDtoResultModel.setSuccessed(false);
            aiUserDtoResultModel.setErrorCode("1");
            aiUserDtoResultModel.setErrorDesc("删除失败");
        }
        return aiUserDtoResultModel;
    }

    /**
     * 校验用户名是否存在
     *
     * @param username 用户名
     * @return flag
     */
    private boolean checkPasswordIsExist(Integer id, String username, String type) {
        Map<String, Object> params = new HashMap<>(1);
        params.put("username", username);
        AiUserDto aiUserDto = aIUserService.findUserByMap(params);
        if (DataOperTypeEnum.UPDATE.getOperType().equals(type) && aiUserDto != null && aiUserDto.getId().equals(id) && aiUserDto.getUsername().equals(username)) {
            return false;
        }

        boolean flag = false;
        if (aiUserDto != null) {
            flag = true;
        }
        return flag;
    }

    private void checkParams(AiUserDto aiUserDto) {
        if (aiUserDto.getGroupId() == null) {
            throw new AIValidateException("用户角色编号不能为空");
        }

        if (StringUtils.isBlank(aiUserDto.getUsername())) {
            throw new AIValidateException("用户名称不能为空");
        }

        if (StringUtils.isBlank(aiUserDto.getPassword())) {
            throw new AIValidateException("用户密码不能为空");
        }

        if (StringUtils.isBlank(aiUserDto.getEmail())) {
            throw new AIValidateException("用户邮箱不能为空");
        }
    }
}
