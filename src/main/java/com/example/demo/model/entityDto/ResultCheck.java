package com.example.demo.model.entityDto;

import lombok.Data;

import java.io.Serializable;

@Data
public class ResultCheck implements Serializable {
    private String username;
    private String token;
    private String code;
}
