package com.example.demo.model.entityDto;

import com.example.demo.model.entityDo.AiAgentDo;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
public class AiAgentDto extends AiAgentDo{
    private List<AiAgentDto> aiAgentDtoList;
}