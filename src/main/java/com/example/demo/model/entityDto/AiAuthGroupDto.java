package com.example.demo.model.entityDto;

import com.example.demo.model.entityDo.AiAuthGroupDo;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class AiAuthGroupDto extends AiAuthGroupDo implements Serializable {
    private List<AiAuthGroupDto> aiAuthGroupDtoList;
}
