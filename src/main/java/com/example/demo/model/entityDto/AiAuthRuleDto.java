package com.example.demo.model.entityDto;

import com.example.demo.model.entityDo.AiAuthRuleDo;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class AiAuthRuleDto extends AiAuthRuleDo implements Serializable {
    private List<AiAuthRuleDto> aiAuthRuleDos;
}
