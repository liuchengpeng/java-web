package com.example.demo.model.entityDto.knowDto;

import com.example.demo.model.robotDO.RobotCommonReqDo;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class KnowledgeDto extends RobotCommonReqDo implements Serializable {

    /**
     * 知识库ID
     */
    private String knowcateid;
    /**
     * 知识词条ID
     */
    private String knowid;
    /**
     *问题的json列表
     */
    private List<String> question;
    /**
     *正则表达式list
     */
    private List<String> regex;
    /**
     * 答案，格式同格式同condi的do节点
     */
    private List<KnowledgeDtoExt> answer;
    /**
     * 可选，0=不启用，1=启用
     */
    private String isopen;
    /**
     * 可选，匹配阀值，默认50，取值范围1~99
     */
    private String threshold;

    private Integer errno;
    private KnowledgeDtoExt data;


    @Data
    public static class Newcate {
        private Integer knowcateid;
        private Integer knowid;
        private Integer tenantid;
        private Integer threshold;
        private Integer isopen;
        private String name;
        private List<String> question;
        private List<String> regex;
        private List<KnowledgeDtoExt> answer;
        private Integer total;
        private List<Newcate> data;
    }

    @Data
    public static class Action {
        private Integer ver;
        private Integer vad;
        private String next;
        private List<Text> text;
    }

    @Data
    public static class Text {
        private String t;
        private String f;
    }

    @Data
    public static class Newknow {
        private String knowid;
        private String knowcateid;
        private List<String> question;
        private List<String> regex;
        private List<KnowledgeDtoExt> answer;
    }

    @Data
    public static class Newrela {
        private Integer robotid;
        private Integer knowcateid;
        private Integer isopen;
        private Integer threshold;
    }


}

