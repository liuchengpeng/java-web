package com.example.demo.model.entityDto;

import java.io.Serializable;
import java.util.List;

import com.example.demo.model.entityDo.AiUserDo;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
public class AiUserDto extends AiUserDo implements Serializable {

    private static final long serialVersionUID = 7311110458957987179L;

    private String createTimeStr;// 创建时间格式化
    private String lastLoginTimeStr;// 最后登录时间格式化
    private List<AiUserDto> aiUserDtos;// dto集合
    /**
     * 角色ID
     */
    private Integer groupId;

}
