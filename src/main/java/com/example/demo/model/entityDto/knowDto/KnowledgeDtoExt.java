package com.example.demo.model.entityDto.knowDto;

import lombok.Data;

import java.io.Serializable;

@Data
public class KnowledgeDtoExt extends KnowledgeDto implements Serializable {

    private Integer deletedcate;
    private Integer deletedid;
    private Newcate ls;
    private Newcate newcate;
    private Newrela newrela;
    private Integer errno;
    private Integer atype;
    private Action action;
    private Newknow newknow;

}
