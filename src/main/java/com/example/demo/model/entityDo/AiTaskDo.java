package com.example.demo.model.entityDo;

import java.util.Date;

public class AiTaskDo {
    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column ai_task.t_id
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    private Integer tId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column ai_task.s_id
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    private Integer sId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column ai_task.tc_id
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    private Integer tcId;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column ai_task.t_number
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    private String tNumber;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column ai_task.t_addition
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    private String tAddition;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column ai_task.t_status
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    private Integer tStatus;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column ai_task.t_sessionid
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    private String tSessionid;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column ai_task.t_sip_start
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    private Date tSipStart;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column ai_task.t_sip_inv
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    private Date tSipInv;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column ai_task.t_sip_code183
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    private Integer tSipCode183;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column ai_task.t_sip_code200
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    private Date tSipCode200;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column ai_task.t_sip_code408
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    private Integer tSipCode408;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column ai_task.t_sip_bye
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    private Date tSipBye;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column ai_task.t_sip_end
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    private Date tSipEnd;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column ai_task.t_sip_byetype
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    private String tSipByetype;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column ai_task.t_sip_byereason
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    private Integer tSipByereason;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column ai_task.t_audio
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    private String tAudio;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column ai_task.t_result
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    private String tResult;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column ai_task.t_cdate
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    private Date tCdate;

    /**
     *
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column ai_task.t_mdate
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    private Date tMdate;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ai_task.t_id
     *
     * @return the value of ai_task.t_id
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    public Integer gettId() {
        return tId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ai_task.t_id
     *
     * @param tId the value for ai_task.t_id
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    public void settId(Integer tId) {
        this.tId = tId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ai_task.s_id
     *
     * @return the value of ai_task.s_id
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    public Integer getsId() {
        return sId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ai_task.s_id
     *
     * @param sId the value for ai_task.s_id
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    public void setsId(Integer sId) {
        this.sId = sId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ai_task.tc_id
     *
     * @return the value of ai_task.tc_id
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    public Integer getTcId() {
        return tcId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ai_task.tc_id
     *
     * @param tcId the value for ai_task.tc_id
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    public void setTcId(Integer tcId) {
        this.tcId = tcId;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ai_task.t_number
     *
     * @return the value of ai_task.t_number
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    public String gettNumber() {
        return tNumber;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ai_task.t_number
     *
     * @param tNumber the value for ai_task.t_number
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    public void settNumber(String tNumber) {
        this.tNumber = tNumber == null ? null : tNumber.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ai_task.t_addition
     *
     * @return the value of ai_task.t_addition
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    public String gettAddition() {
        return tAddition;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ai_task.t_addition
     *
     * @param tAddition the value for ai_task.t_addition
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    public void settAddition(String tAddition) {
        this.tAddition = tAddition == null ? null : tAddition.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ai_task.t_status
     *
     * @return the value of ai_task.t_status
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    public Integer gettStatus() {
        return tStatus;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ai_task.t_status
     *
     * @param tStatus the value for ai_task.t_status
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    public void settStatus(Integer tStatus) {
        this.tStatus = tStatus;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ai_task.t_sessionid
     *
     * @return the value of ai_task.t_sessionid
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    public String gettSessionid() {
        return tSessionid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ai_task.t_sessionid
     *
     * @param tSessionid the value for ai_task.t_sessionid
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    public void settSessionid(String tSessionid) {
        this.tSessionid = tSessionid == null ? null : tSessionid.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ai_task.t_sip_start
     *
     * @return the value of ai_task.t_sip_start
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    public Date gettSipStart() {
        return tSipStart;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ai_task.t_sip_start
     *
     * @param tSipStart the value for ai_task.t_sip_start
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    public void settSipStart(Date tSipStart) {
        this.tSipStart = tSipStart;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ai_task.t_sip_inv
     *
     * @return the value of ai_task.t_sip_inv
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    public Date gettSipInv() {
        return tSipInv;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ai_task.t_sip_inv
     *
     * @param tSipInv the value for ai_task.t_sip_inv
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    public void settSipInv(Date tSipInv) {
        this.tSipInv = tSipInv;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ai_task.t_sip_code183
     *
     * @return the value of ai_task.t_sip_code183
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    public Integer gettSipCode183() {
        return tSipCode183;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ai_task.t_sip_code183
     *
     * @param tSipCode183 the value for ai_task.t_sip_code183
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    public void settSipCode183(Integer tSipCode183) {
        this.tSipCode183 = tSipCode183;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ai_task.t_sip_code200
     *
     * @return the value of ai_task.t_sip_code200
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    public Date gettSipCode200() {
        return tSipCode200;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ai_task.t_sip_code200
     *
     * @param tSipCode200 the value for ai_task.t_sip_code200
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    public void settSipCode200(Date tSipCode200) {
        this.tSipCode200 = tSipCode200;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ai_task.t_sip_code408
     *
     * @return the value of ai_task.t_sip_code408
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    public Integer gettSipCode408() {
        return tSipCode408;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ai_task.t_sip_code408
     *
     * @param tSipCode408 the value for ai_task.t_sip_code408
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    public void settSipCode408(Integer tSipCode408) {
        this.tSipCode408 = tSipCode408;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ai_task.t_sip_bye
     *
     * @return the value of ai_task.t_sip_bye
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    public Date gettSipBye() {
        return tSipBye;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ai_task.t_sip_bye
     *
     * @param tSipBye the value for ai_task.t_sip_bye
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    public void settSipBye(Date tSipBye) {
        this.tSipBye = tSipBye;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ai_task.t_sip_end
     *
     * @return the value of ai_task.t_sip_end
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    public Date gettSipEnd() {
        return tSipEnd;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ai_task.t_sip_end
     *
     * @param tSipEnd the value for ai_task.t_sip_end
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    public void settSipEnd(Date tSipEnd) {
        this.tSipEnd = tSipEnd;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ai_task.t_sip_byetype
     *
     * @return the value of ai_task.t_sip_byetype
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    public String gettSipByetype() {
        return tSipByetype;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ai_task.t_sip_byetype
     *
     * @param tSipByetype the value for ai_task.t_sip_byetype
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    public void settSipByetype(String tSipByetype) {
        this.tSipByetype = tSipByetype == null ? null : tSipByetype.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ai_task.t_sip_byereason
     *
     * @return the value of ai_task.t_sip_byereason
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    public Integer gettSipByereason() {
        return tSipByereason;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ai_task.t_sip_byereason
     *
     * @param tSipByereason the value for ai_task.t_sip_byereason
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    public void settSipByereason(Integer tSipByereason) {
        this.tSipByereason = tSipByereason;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ai_task.t_audio
     *
     * @return the value of ai_task.t_audio
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    public String gettAudio() {
        return tAudio;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ai_task.t_audio
     *
     * @param tAudio the value for ai_task.t_audio
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    public void settAudio(String tAudio) {
        this.tAudio = tAudio == null ? null : tAudio.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ai_task.t_result
     *
     * @return the value of ai_task.t_result
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    public String gettResult() {
        return tResult;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ai_task.t_result
     *
     * @param tResult the value for ai_task.t_result
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    public void settResult(String tResult) {
        this.tResult = tResult == null ? null : tResult.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ai_task.t_cdate
     *
     * @return the value of ai_task.t_cdate
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    public Date gettCdate() {
        return tCdate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ai_task.t_cdate
     *
     * @param tCdate the value for ai_task.t_cdate
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    public void settCdate(Date tCdate) {
        this.tCdate = tCdate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column ai_task.t_mdate
     *
     * @return the value of ai_task.t_mdate
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    public Date gettMdate() {
        return tMdate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column ai_task.t_mdate
     *
     * @param tMdate the value for ai_task.t_mdate
     *
     * @mbg.generated Wed Jul 03 17:17:53 CST 2019
     */
    public void settMdate(Date tMdate) {
        this.tMdate = tMdate;
    }
}