package com.example.demo.model.entityDo;

public class AiAuthGroupAccessDo {
    private Integer uid;

    private Integer groupId;

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }
}