package com.example.demo.model.robotDto;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.annotation.JSONField;
import com.example.demo.model.robotDO.RobotCommonReqDo;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class RobotTreeReqDto extends RobotCommonReqDo {
    /**
     * 节点摘要
     */
    private String summary;

    /**
     * 进入该节点的动作(选填)
     */
    private JSONArray action;

    /**
     * 是否未最终节点 0=否，1=是 ，默认否(选填)
     */
    private Integer isend;

    private String addition;

    /**
     * name,summary,action,jump,isend,addition
     */
    private String fileds;

    /**
     * 要获取的任务id
     */
    private Integer treeid;

    /**
     * 可选，要修改的跳转体ID，0=添加新跳转
     */
    private String jumpid;

    private List<Integer> sort;

    @Data
    public static class ActionData implements Serializable {
        private CondiData condi;

        @JSONField(name = "do")
        private List<DoData> doData;
    }

    @Data
    public static class CondiData {
        private Integer ver;

        private String condi;

        private Integer condi_case;

        private NormalData normal;

        private List<RulesData> rules;
    }

    @Data
    public static class RulesData {
        /**
         * 0=系统状态 1=实体
         */
        private Integer key_type;

        /**
         * 目前仅支持anything_else
         */
        private String key = "anything_else";

        private String op;

        private Integer value_type;

        private String value;
    }

    @Data
    public static class DoData {
        private Integer atype;

        private DoActionData action;
    }

    @Data
    public static class NormalData {
        private String op;

        private List<RulesData> rules;
    }

    @Data
    public static class DoActionData {
        private Integer ver;

        /**
         * 本句是否支持打断，0=不支持 1=支持
         */
        private Integer vad;

        /**
         * 下一步触发词
         */
        private String next;

        private List<TextData> text;

        /**
         * 要跳转进入的任务ID(treeid)
         */
        private String toid;

        /**
         * 10=跳转到动作，20=跳转到jump（级联条）
         */
        private Integer jtype;

        /**
         * 变量赋值(atype=30)
         */
        private List<ParamData> param;

        /**
         * 自定义跳转路径，不填写则post到robot默认
         */
        private String url;

        /**
         * 动作名称
         */
        private String act;

        /**
         * 静音等待时间(welcome必填)
         */
        private Integer waitms;

        /**
         * 没有识别出文本的情况(welcome必填)  repeat=没识别出文字重复上句，next=没识别出文字说下一句
         */
        private String whennull;

        /**
         * cmd可选值
         * intent:next		触发下一个动作
         * intent:repeat		重复前一句话
         * intent:bye		触发挂机
         */
        private String cmd;
    }

    @Data
    public static class ParamData {
        /**
         * key需要符合变量命名规范
         */
        private String key;

        /**
         * 例如：撒点粉 如果是实体(value_type=1)，需要加@
         */
        private String value;

        /**
         * 0=字符串赋值，1=实体赋值
         */
        private Integer value_type;
    }

    @Data
    public static class TextData {
        /**
         * 文本
         */
        private String t;

        /**
         * 真人录音文件名
         */
        private String f;
    }
}
