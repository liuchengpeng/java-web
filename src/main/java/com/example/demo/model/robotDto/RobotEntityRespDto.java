package com.example.demo.model.robotDto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class RobotEntityRespDto implements Serializable {

    /**
     * 返回码
     */
    private String errno;

    private NewRobotEntityData data;

    /**
     * 返回的错误信息
     */
    private String errstr;

    @Data
    public static class NewRobotEntityData implements Serializable {
        private List<NewRobotEntity> data;

        private Integer total;

        private Integer id;

        private Integer robotid;

        private String name;


        private String summary;

        /**
         * 实体值内容
         */
        private List<List<String>> content;

        /**
         * 删除实体标识 1：删除成功，0：删除失败
         */
        private Integer delentity;

        private NewRobotEntity.OneEntityInfo info;
    }

    @Data
    public static class NewRobotEntity extends NewRobotEntityData {
        static class OneEntityInfo extends NewRobotEntity {

        }
    }
}
