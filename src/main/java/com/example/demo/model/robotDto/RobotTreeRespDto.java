package com.example.demo.model.robotDto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class RobotTreeRespDto implements Serializable {

    /**
     * 错误码
     */
    private String errno;

    private TreeLsInfoDo data;

    private String errstr;


    @Data
    public static class TreeLsInfoDo implements Serializable {
        /**
         * 入口id
         */
        private Integer entryid;

        /**
         * 所有节点内容
         */
        private List<TreeLsInfoDo> ls;

        private Integer id;

        private String name;

        private String summary;

        private List<RobotTreeReqDto.ActionData> action;

        private Integer deletedid;

        private Integer isend;

        private String addition;
    }
}
