package com.example.demo.model.robotDto;

import com.example.demo.model.robotDO.RobotCommonReqDo;
import lombok.Data;

import java.util.List;

/**
 * @class_name: RobotEntityReqDto
 * @package: com.example.demo.model.robotDto
 * @describe: 机器人实体扩展
 * @author: le.gao
 * @creat_date: 2019/7/1
 * @creat_time: 14:29
 **/
@Data
public class RobotEntityReqDto extends RobotCommonReqDo {

    /**
     * 实体描述信息
     */
    private String summary;

    /**
     * 实体ID
     */
    private Integer entityid;


    /**
     * 实体值列表
     */
    private List<List<String>> content;
}
