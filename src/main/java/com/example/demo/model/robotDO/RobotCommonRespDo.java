package com.example.demo.model.robotDO;

import com.alibaba.fastjson.JSONArray;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 机器人的通用响应类 有额外扩展的属性 可以继承该类
 */
@Data
public class RobotCommonRespDo implements Serializable {
    /**
     * 返回码
     */
    private String errno;
    /**
     * 返回值
     */
    private DataResp data;

    /**
     * 返回的错误信息
     */
    private String errstr;

    @Data
    public static class DataResp {
        private String secretkey;
        private String total;
        private NewRobot NewRobot;
        private List<NewRobot> data;
        private String status;
        private String name;
        private String tenantid;
        private String timeout_sec;
        private String hookopen;
        private String hookurl;
        private WelcomeResp welcome;
        private NohitResp nohit;
        private MultinohitResp multinohit;
        private String multinohit_count;
    }

    @Data
    public static class NewRobot {
        private String id;
        private String domainid;
        private String name;
        private String appid;
        private String status;
        private String secretkey;
        private String tenantid;

    }

    @Data
    public static class WelcomeResp{
        private String ver;
        private String waitms;
        private String whennull;
        private String filename;
        private String vad;
        private String next;
        private JSONArray text;
    }

    @Data
    public static class NohitResp{
        private String ver;
        private String filename;
        private String vad;
        private String next;
        private JSONArray text;
    }

    @Data
    public static class MultinohitResp{
        private String ver;
        private String filename;
        private String vad;
        private String next;
        private JSONArray text;
    }
}
