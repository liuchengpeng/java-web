package com.example.demo.model.robotDO;

import com.alibaba.fastjson.JSONArray;
import lombok.Data;

import java.io.Serializable;

/**
 * 机器人的通用请求类 有额外扩展的属性 可以继承该类
 */
@Data
public class RobotCommonReqDo implements Serializable{
    //-----------创建请求参数----------
    /**
     * 机器人名字，1~50个字符
     */
    private String name;
    /**
     * 机器人的租户ID，必须为整数
     */
    private String tenantid;
    //------更新请求参数-------------
    /**
     * 要更新的robot
     */
    private String robotid;
    /**
     * 可选，填写1为需要更新secretkey
     */
    private String sec;
    /**
     * 可选，设置当前robot的新状态
     */
    private String status;
    /**
     * 可选，webhook是否开启 0=开 1=关
     */
    private String hookopen;
    /**
     * 可选，webhook的url地址
     */
    private String hookurl;
    /**
     * 可选，该robot的session的超时秒数，默认1800，取值范围(300~1800)
     */
    private String timeout_sec;
    /**
     * 可选，robot的welcome回复，json格式，参考“回复体定义”部分
     */
    private WelcomeReq welcome;
    /**
     * 可选，robot未命中回复，json格式，参考“回复体定义”部分
     */
    private NohitReq nohit;
    /**
     * 可选，robot多次未命中回复，json格式，参考“回复体定义”部分
     */
    private MultinohitReq multinohit;
    /**
     * 可选，几次回复未未命中触发multinohit，0=不使用该功能
     */
    private String multinohitcount;
    /**
     * 要使用的流程图入口节点ID
     */
    private String entrytreeid;
    /**
     * 来源robotID
     */
    private String fromrobotid;
    /**
     * 可选，当前页，默认为1
     */
    private String page;
    /**
     * 可选，每页个数，默认为20
     */
    private String pagesize;
    /**
     * 可选一下字段 name,tenantid,appid,secretkey,status,timeout_sec,
     * hookopen,hookurl,welcome,nohit,
     * multinohit,multinohit_count
     */
    private String fields;

    @Data
    public static class WelcomeReq{
        private String ver;
        private String waitms;
        private String whennull;
        private String filename;
        private String vad;
        private String next;
        private JSONArray text;
    }

    @Data
    public static class NohitReq{
        private String ver;
        private String filename;
        private String vad;
        private String next;
        private JSONArray text;
    }

    @Data
    public static class MultinohitReq{
        private String ver;
        private String filename;
        private String vad;
        private String next;
        private JSONArray text;
    }
}
