package com.example.demo.model.robotDO;

import com.example.demo.model.robotDto.RobotTreeReqDto;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @class_name: RobotTreeJumpReqDo
 * @package: com.example.demo.model.robotDO
 * @describe: 机器人节点调整 do
 * @author: le.gao
 * @creat_date: 2019/7/3
 * @creat_time: 14:46
 **/
@Data
public class RobotTreeJumpReqDo implements Serializable {

    private Integer robotid;

    /**
     * 要获取的任务id
     */
    private Integer treeid;

    /**
     * 可选，要修改的跳转体ID，0=添加新跳转
     */
    private String jumpid;

    private RobotTreeReqDto.ActionData action;

    private List<Integer> sort;
}
