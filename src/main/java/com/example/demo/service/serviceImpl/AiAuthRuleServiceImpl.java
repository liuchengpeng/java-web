package com.example.demo.service.serviceImpl;

import com.example.demo.mapper.java.mapper.AiAuthRuleDoMapper;
import com.example.demo.mapper.java.mapper.AiAuthRuleDoMapperExt;
import com.example.demo.model.entityDo.AiAuthRuleDo;
import com.example.demo.model.entityDto.AiAuthRuleDto;
import com.example.demo.service.service.AiAuthRuleService;
import com.example.demo.util.common.vo.ResultModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Service
public class AiAuthRuleServiceImpl implements AiAuthRuleService {

    @Autowired
    private AiAuthRuleDoMapper aiAuthRuleDoMapper;
    @Autowired
    private AiAuthRuleDoMapperExt aiAuthRuleDoMapperExt;

    @Override
    public ResultModel<AiAuthRuleDto> queryList(AiAuthRuleDto aiAuthRuleDto, HttpServletRequest request, HttpServletResponse response) {
        ResultModel<AiAuthRuleDto> ruleDtoResultModel = new ResultModel<>();
        List<AiAuthRuleDto> list = getAiAuthRuleDtoTree(0);
        ruleDtoResultModel.setSuccessed(true);
        aiAuthRuleDto.setAiAuthRuleDos(list);
        ruleDtoResultModel.setReturnValue(aiAuthRuleDto);
        return ruleDtoResultModel;
    }

    @Override
    public ResultModel<AiAuthRuleDto> update(AiAuthRuleDo aiAuthRuleDo, HttpServletRequest request, HttpServletResponse response) {
        ResultModel<AiAuthRuleDto> ruleDtoResultModel = new ResultModel<>();
        int r = aiAuthRuleDoMapper.updateByPrimaryKeySelective(aiAuthRuleDo);
        if (r == 0){
            ruleDtoResultModel.setSuccessed(false);
            ruleDtoResultModel.setErrorCode("1");
            ruleDtoResultModel.setErrorDesc("更新失败");
        }else{
            ruleDtoResultModel.isSuccessed();
        }
        return ruleDtoResultModel;
    }

    @Override
    public ResultModel<AiAuthRuleDto> detele(Integer id, HttpServletRequest request, HttpServletResponse response) {
        ResultModel<AiAuthRuleDto> ruleDtoResultModel = new ResultModel<>();
        int r = aiAuthRuleDoMapper.deleteByPrimaryKey(id);
        if (r == 0){
            ruleDtoResultModel.setSuccessed(false);
            ruleDtoResultModel.setErrorCode("1");
            ruleDtoResultModel.setErrorDesc("删除失败");
        }else{
            ruleDtoResultModel.isSuccessed();
        }
        return ruleDtoResultModel;
    }

    @Override
    public ResultModel<AiAuthRuleDto> insert(AiAuthRuleDo aiAuthRuleDo, HttpServletRequest request, HttpServletResponse response) {
        validate(aiAuthRuleDo);
        ResultModel<AiAuthRuleDto> ruleDtoResultModel = new ResultModel<>();
        int r = aiAuthRuleDoMapper.insertSelective(aiAuthRuleDo);
        if (r == 0){
            ruleDtoResultModel.setSuccessed(false);
            ruleDtoResultModel.setErrorCode("1");
            ruleDtoResultModel.setErrorDesc("新增失败");
        }else{
            ruleDtoResultModel.isSuccessed();
        }
        return ruleDtoResultModel;
    }

    @Override
    public List<AiAuthRuleDto> getAiAuthRuleDtoTree(Integer id) {
        return aiAuthRuleDoMapperExt.findAiAuthRuleDtoChildrenNode(id);
    }

    /**
     * 参数校验
     * @param aiAuthRuleDo
     */
    private void validate(AiAuthRuleDo aiAuthRuleDo) {
    }
}
