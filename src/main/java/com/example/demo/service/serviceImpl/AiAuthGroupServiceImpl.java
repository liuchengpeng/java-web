package com.example.demo.service.serviceImpl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.example.demo.mapper.java.mapper.AiAuthGroupDoMapperExt;
import com.example.demo.model.entityDo.AiAuthGroupDo;
import com.example.demo.model.entityDto.AiAuthGroupDto;
import com.example.demo.model.entityDto.AiAuthRuleDto;
import com.example.demo.service.service.AiAuthGroupService;
import com.example.demo.service.service.AiAuthRuleService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class AiAuthGroupServiceImpl implements AiAuthGroupService {

    private final AiAuthGroupDoMapperExt aiAuthGroupDoMapperExt;

    private final AiAuthRuleService aiAuthRuleService;

    public AiAuthGroupServiceImpl(AiAuthGroupDoMapperExt aiAuthGroupDoMapperExt, AiAuthRuleService aiAuthRuleService) {
        this.aiAuthGroupDoMapperExt = aiAuthGroupDoMapperExt;
        this.aiAuthRuleService = aiAuthRuleService;
    }

    @Override
    public List<AiAuthGroupDto> getAllAiAuthGroup() {
        return aiAuthGroupDoMapperExt.getAllAiAuthGroup();
    }

    @Override
    public int updateAiAuthGroupStatus(Integer id, Integer status) {
        AiAuthGroupDto aiAuthGroupDto = new AiAuthGroupDto();
        aiAuthGroupDto.setId(id);
        if (status == 0) {
            status = 1;
        } else {
            status = 0;
        }
        aiAuthGroupDto.setStatus(status);
        return aiAuthGroupDoMapperExt.updateByPrimaryKeySelective(aiAuthGroupDto);
    }

    @Override
    public int addAiAuthGroup(String title) {
        AiAuthGroupDto aiAuthGroupDto = new AiAuthGroupDto();
        aiAuthGroupDto.setStatus(0);
        aiAuthGroupDto.setTitle(title);
        return aiAuthGroupDoMapperExt.insertSelective(aiAuthGroupDto);
    }

    @Override
    public AiAuthGroupDto checkAiAuthGroupTitleIsExist(String title) {
        Map<String, Object> params = new HashMap<>(16);
        params.put("title", title);
        return aiAuthGroupDoMapperExt.getAiAuthGroupByMap(params);
    }

    @Override
    public int updateAiAuthGroup(Integer id, String title, Integer status) {
        AiAuthGroupDto aiAuthGroupDto = new AiAuthGroupDto();
        aiAuthGroupDto.setId(id);
        aiAuthGroupDto.setTitle(title);
        aiAuthGroupDto.setStatus(status);
        return aiAuthGroupDoMapperExt.updateByPrimaryKeySelective(aiAuthGroupDto);
    }

    @Override
    public int deleteAiAuthGroup(Integer id) {
        return aiAuthGroupDoMapperExt.deleteByPrimaryKey(id);
    }

    @Override
    public JSONObject findUserAuthorizationInfo(Integer id) {
        JSONObject returnJson = new JSONObject();
        AiAuthGroupDo aiAuthGroupDo = aiAuthGroupDoMapperExt.selectByPrimaryKey(id);
        if (aiAuthGroupDo != null) {
            List<AiAuthRuleDto> aiAuthRuleDtoList = aiAuthRuleService.getAiAuthRuleDtoTree(0);
            JSONArray aiAuthRuleDtoArr = (JSONArray) JSONObject.toJSON(aiAuthRuleDtoList);
            if (StringUtils.isNotBlank(aiAuthGroupDo.getRules())) {
                Map<String, Integer> aiAuthRuleMap = new HashMap<>(16);
                if (aiAuthGroupDo.getRules().contains(",")) {
                    String[] aiAuthGroupDoArr = aiAuthGroupDo.getRules().split(",");
                    for (String rule : aiAuthGroupDoArr) {
                        aiAuthRuleMap.put(rule, aiAuthGroupDo.getId());
                        findCheckedRule(aiAuthRuleDtoArr, aiAuthRuleMap);
                    }
                } else {
                    aiAuthRuleMap.put(aiAuthGroupDo.getRules(), aiAuthGroupDo.getId());
                    findCheckedRule(aiAuthRuleDtoArr, aiAuthRuleMap);
                }
            }
            returnJson.put("aiAuthRules", aiAuthRuleDtoArr);
        }
        return returnJson;
    }

    @Override
    public int commitUserAuthorizationInfo(Integer id, String ruleIds) {
        AiAuthGroupDto aiAuthGroupDto = new AiAuthGroupDto();
        aiAuthGroupDto.setId(id);
        aiAuthGroupDto.setRules(ruleIds);
        return aiAuthGroupDoMapperExt.updateByPrimaryKeySelective(aiAuthGroupDto);
    }

    private void findCheckedRule(JSONArray aiAuthRuleDtoArr, Map<String, Integer> aiAuthRuleMap) {
        for (Object o : aiAuthRuleDtoArr) {
            JSONObject aiAuthRuleDtoJson = (JSONObject) o;
            if (aiAuthRuleMap.containsKey(aiAuthRuleDtoJson.getString("id"))) {
                aiAuthRuleDtoJson.put("checked", true);
            }

            if (aiAuthRuleDtoJson.containsKey("aiAuthRuleDos") && aiAuthRuleDtoJson.getJSONArray("aiAuthRuleDos").size() > 0) {
                findCheckedRule(aiAuthRuleDtoJson.getJSONArray("aiAuthRuleDos"), aiAuthRuleMap);
            }
        }
    }
}
