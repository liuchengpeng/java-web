package com.example.demo.service.serviceImpl;

import com.alibaba.fastjson.JSONObject;
import com.example.demo.constant.AiCoreConstant;
import com.example.demo.model.entityDto.knowDto.KnowledgeDto;
import com.example.demo.service.service.KnowledgeService;
import com.example.demo.util.common.CommHttpClientUtils;
import com.example.demo.util.common.exception.AIValidateException;
import com.example.demo.util.common.json.JSONUtils;
import com.example.demo.util.common.string.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class KnowledgeServiceImpl implements KnowledgeService {


    @Override
    public KnowledgeDto createOrUpdateKnowledge(KnowledgeDto knowledgeDto) throws AIValidateException {
        checkParameter(knowledgeDto);
        String name = knowledgeDto.getName();
        if (StringUtil.isBlank(name) || StringUtil.length(name) > 20) {
            throw new AIValidateException("问答库名称不能为空，且最多20个字");
        }
        String tenantid = knowledgeDto.getTenantid();
        if (StringUtil.isBlank(tenantid)) {
            throw new AIValidateException("所属租户ID不能为空");
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("tenantid", tenantid);
        jsonObject.put("name", name);
        String result = null;
        // 根据知识库id判断是否是更新或是新增，若知识库id存在则调用update接口，否则调用create接口
        // 调用新增接口
        if (StringUtil.isBlank(knowledgeDto.getKnowcateid())) {
            result = CommHttpClientUtils.doPostToAiCore(AiCoreConstant.KNOW_ADDCATE, jsonObject);
            // 调用更新接口
        } else {
            jsonObject.put("knowcateid", knowledgeDto.getKnowcateid());
            result = CommHttpClientUtils.doPostToAiCore(AiCoreConstant.KNOW_UPCATE, jsonObject);
        }
        // json字符串转bean
        Object obj = JSONUtils.jsonToBean(result, KnowledgeDto.class);
        return (KnowledgeDto) obj;

    }

    public static void main(String[] args) {
        //        KnowledgeDto knowledgeDto = new KnowledgeDto();
        String string =
                "{\"errno\": 0, \"data\": {\"ls\": {\"total\": 51, \"data\": [{\"knowcateid\": 48, \"tenantid\": 0, \"name\": \"98\"}, {\"knowcateid\": 47, \"tenantid\": 0, \"name\": \"96\"}, {\"knowcateid\": 46, \"tenantid\": 0, \"name\": \"97\"}]}}}";

        JSONObject jsonObject1 = JSONObject.parseObject(string);
        KnowledgeDto knowledgeDtossssss = JSONObject.toJavaObject(jsonObject1, KnowledgeDto.class);
        String getKnowcateid = knowledgeDtossssss.getKnowcateid();
        System.out.println(string);
    }

    @Override
    public KnowledgeDto deleteKnowledge(KnowledgeDto knowledgeDto) throws AIValidateException {
        checkParameter(knowledgeDto);
        String knowcateid = knowledgeDto.getKnowcateid();
        if (StringUtil.isBlank(knowcateid)) {
            throw new AIValidateException("知识库ID不能为空");
        }
        // 根据知识库id调用删除接口
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("knowcateid", knowcateid);
        String result = CommHttpClientUtils.doPostToAiCore(AiCoreConstant.KNOW_DELCATE, jsonObject);
        Object obj = JSONUtils.jsonToBean(result, KnowledgeDto.class);
        return (KnowledgeDto) obj;
    }


    @Override
    public KnowledgeDto queryList(KnowledgeDto knowledgeDto) throws AIValidateException {
        checkParameter(knowledgeDto);
        String tenantid = knowledgeDto.getTenantid();
        if (StringUtil.isBlank(tenantid)) {
            throw new AIValidateException("所属租户ID不能为空");
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("tenantid", tenantid);
        jsonObject.put("page", knowledgeDto.getPage());
        jsonObject.put("pagesize", knowledgeDto.getPagesize());
        String result = CommHttpClientUtils.doPostToAiCore(AiCoreConstant.KNOW_LSCATE, jsonObject);
        // json字符串转bean
        Object obj = JSONUtils.jsonToBean(result, KnowledgeDto.class);
        return (KnowledgeDto) obj;
    }


    /**
     * 添加或更新一个知识词条
     *
     * @param knowledgeDto
     * @return
     * @throws AIValidateException
     */
    @Override
    public KnowledgeDto addOrUpdateKnowEntry(KnowledgeDto knowledgeDto) throws AIValidateException {
        checkParameter(knowledgeDto);
        String knowcateid = knowledgeDto.getKnowcateid();
        if (StringUtil.isBlank(knowcateid)) {
            throw new AIValidateException("知识库ID不能为空");
        }
        // 根据知识词条ID判断是否是更新或是新增，若知识词条ID存在则调用update接口，否则调用create接口
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("knowcateid", knowcateid);
        jsonObject.put("question", knowledgeDto.getQuestion());
        jsonObject.put("regex", knowledgeDto.getRegex());
        jsonObject.put("answer", knowledgeDto.getAnswer());
        String result = null;
        // 调用新增知识词条接口
        if (StringUtil.isBlank(knowledgeDto.getKnowid())) {
            result = CommHttpClientUtils.doPostToAiCore(AiCoreConstant.KNOW_ADDKNOW, jsonObject);
        } else {
            result = CommHttpClientUtils.doPostToAiCore(AiCoreConstant.KNOW_UPKNOW, jsonObject);
        }
        Object obj = JSONUtils.jsonToBean(result, KnowledgeDto.class);
        return (KnowledgeDto) obj;
    }

    @Override
    public KnowledgeDto getOneKnow(KnowledgeDto knowledgeDto) throws AIValidateException {
        checkParameter(knowledgeDto);
        checkParameter2(knowledgeDto);
        String knowcateid = knowledgeDto.getKnowcateid();
        String knowid = knowledgeDto.getKnowid();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("knowcateid", knowcateid);
        jsonObject.put("knowid", knowid);
        // 根据知识库ID和知识词条ID查询知识词条信息
        String result = CommHttpClientUtils.doPostToAiCore(AiCoreConstant.KNOW_ONEKNOW, jsonObject);
        Object obj = JSONUtils.jsonToBean(result, KnowledgeDto.class);
        return (KnowledgeDto) obj;
    }

    /**
     * 列出知识库的知识词条
     *
     * @param knowledgeDto
     * @return
     * @throws AIValidateException
     */
    @Override
    public KnowledgeDto listKnowPage(KnowledgeDto knowledgeDto) throws AIValidateException {
        checkParameter(knowledgeDto);
        String knowcateid = knowledgeDto.getKnowcateid();
        if (StringUtil.isBlank(knowcateid)) {
            throw new AIValidateException("知识库ID不能为空");
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("knowcateid", knowcateid);
        jsonObject.put("knowid", knowledgeDto.getKnowid());
        jsonObject.put("page", knowledgeDto.getPage());
        jsonObject.put("pagesize", knowledgeDto.getPagesize());

        //TODO 根据知识库ID查询知识词条信息
        String result = CommHttpClientUtils.doPostToAiCore(AiCoreConstant.KNOW_LSKNOW, jsonObject);
        Object obj = JSONUtils.jsonToBean(result, KnowledgeDto.class);
        return (KnowledgeDto) obj;
    }

    @Override
    public KnowledgeDto delknow(KnowledgeDto knowledgeDto) throws AIValidateException {
        checkParameter(knowledgeDto);
        checkParameter2(knowledgeDto);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("knowcateid", knowledgeDto.getKnowcateid());
        jsonObject.put("knowid", knowledgeDto.getKnowid());
        //TODO 根据知识库ID和知识词条ID删除知识词条
        String result = CommHttpClientUtils.doPostToAiCore(AiCoreConstant.KNOW_DELKNOW, jsonObject);
        Object obj = JSONUtils.jsonToBean(result, KnowledgeDto.class);
        return (KnowledgeDto) obj;
    }

    @Override
    public KnowledgeDto knowRobotBandrela(KnowledgeDto knowledgeDto) throws AIValidateException {
        checkParameter(knowledgeDto);
        String knowcateid = knowledgeDto.getKnowcateid();
        if (StringUtil.isBlank(knowcateid)) {
            throw new AIValidateException("知识库ID不能为空");
        }
        String robotid = knowledgeDto.getRobotid();
        if (StringUtil.isBlank(robotid)) {
            throw new AIValidateException("机器人ID不能为空");
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("knowcateid", knowcateid);
        jsonObject.put("robotid", knowledgeDto.getRobotid());
        jsonObject.put("isopen", knowledgeDto.getIsopen());
        jsonObject.put("threshold", knowledgeDto.getThreshold());
        String result = CommHttpClientUtils.doPostToAiCore(AiCoreConstant.KNOW_BANDRELA, jsonObject);
        Object obj = JSONUtils.jsonToBean(result, KnowledgeDto.class);
        return (KnowledgeDto) obj;
    }

    @Override
    public KnowledgeDto knowRobotUnBandrela(KnowledgeDto knowledgeDto) throws AIValidateException {
        String knowcateid = knowledgeDto.getKnowcateid();
        if (StringUtil.isBlank(knowcateid)) {
            throw new AIValidateException("知识库ID不能为空");
        }
        String robotid = knowledgeDto.getRobotid();
        if (StringUtil.isBlank(robotid)) {
            throw new AIValidateException("机器人ID不能为空");
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("knowcateid", knowcateid);
        jsonObject.put("robotid", robotid);
        String result = CommHttpClientUtils.doPostToAiCore(AiCoreConstant.KNOW_UNBANDRELA, jsonObject);
        Object obj = JSONUtils.jsonToBean(result, KnowledgeDto.class);
        return (KnowledgeDto) obj;
    }

    @Override
    public KnowledgeDto listKnowRobotBandrela(KnowledgeDto knowledgeDto) throws AIValidateException {
        checkParameter(knowledgeDto);
        String robotid = knowledgeDto.getRobotid();
        if (StringUtil.isBlank(robotid)) {
            throw new AIValidateException("机器人ID不能为空");
        }
        // TODO 根据 robotid 调用“列出已robot绑定的知识库”的接口
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("robotid", knowledgeDto.getRobotid());
        String result = CommHttpClientUtils.doPostToAiCore(AiCoreConstant.KNOW_LSBANDED, jsonObject);
        Object obj = JSONUtils.jsonToBean(result, KnowledgeDto.class);
        return (KnowledgeDto) obj;
    }

    /**
     * 通用参数校验
     *
     * @param knowledgeDto
     * @throws AIValidateException
     */
    private void checkParameter(KnowledgeDto knowledgeDto) throws AIValidateException {
        if (knowledgeDto == null) {
            throw new AIValidateException("参数不能为空");
        }
    }

    /**
     * 知识库ID/知识词条ID校验
     *
     * @param knowledgeDto
     * @throws AIValidateException
     */
    private void checkParameter2(KnowledgeDto knowledgeDto) throws AIValidateException {
        if (StringUtil.isBlank(knowledgeDto.getKnowcateid())) {
            throw new AIValidateException("知识库ID不能为空");
        }
        if (StringUtil.isBlank(knowledgeDto.getKnowid())) {
            throw new AIValidateException("知识词条ID不能为空");
        }
    }


}
