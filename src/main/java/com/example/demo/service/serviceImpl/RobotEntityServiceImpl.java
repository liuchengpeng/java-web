package com.example.demo.service.serviceImpl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.example.demo.constant.AiCoreConstant;
import com.example.demo.model.robotDto.RobotEntityReqDto;
import com.example.demo.model.robotDto.RobotEntityRespDto;
import com.example.demo.service.service.RobotEntityService;
import com.example.demo.util.common.CommHttpClientUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @class_name: RobotEntityServiceImpl
 * @package: com.example.demo.service.serviceImpl
 * @describe: 机器人实体服务
 * @author: le.gao
 * @creat_date: 2019/7/1
 * @creat_time: 14:40
 **/
@Service
@Slf4j
public class RobotEntityServiceImpl implements RobotEntityService {

    @Override
    public RobotEntityRespDto createRobotEntity(RobotEntityReqDto robotEntityReqDto) {
        JSONObject reqJson = (JSONObject) JSONObject.toJSON(robotEntityReqDto);
        String result = CommHttpClientUtils.doPostToAiCore(AiCoreConstant.ENTITY_CREATE, reqJson);
        JSONObject resultJson = (JSONObject) JSONObject.parse(result);
        return JSONObject.toJavaObject(resultJson, RobotEntityRespDto.class);
    }

    @Override
    public RobotEntityRespDto getRobotEntityList(RobotEntityReqDto robotEntityReqDto) {
        JSONObject reqJson = (JSONObject) JSONObject.toJSON(robotEntityReqDto);
        String result = CommHttpClientUtils.doPostToAiCore(AiCoreConstant.ENTITY_LS, reqJson);
        JSONObject resultJson = (JSONObject) JSONObject.parse(result);
        if (resultJson.containsKey("data")) {
            JSONObject data = resultJson.getJSONObject("data");
            if (data.containsKey("total")) {
                Integer total = data.getInteger("total");
                if (total == 0) {
                    data.put("data", new JSONArray());
                }
            }
        }
        return JSONObject.toJavaObject(resultJson, RobotEntityRespDto.class);
    }

    @Override
    public RobotEntityRespDto updateEntityName(RobotEntityReqDto robotEntityReqDto) {
        JSONObject reqJson = (JSONObject) JSONObject.toJSON(robotEntityReqDto);
        String result = CommHttpClientUtils.doPostToAiCore(AiCoreConstant.ENTITY_UP_NAME, reqJson);
        JSONObject resultJson = (JSONObject) JSONObject.parse(result);
        return JSONObject.toJavaObject(resultJson, RobotEntityRespDto.class);
    }

    @Override
    public RobotEntityRespDto updateEntityContent(RobotEntityReqDto robotEntityReqDto) {
        JSONObject reqJson = (JSONObject) JSONObject.toJSON(robotEntityReqDto);
        String result = CommHttpClientUtils.doPostToAiCore(AiCoreConstant.ENTITY_UP_CONTENT, reqJson);
        JSONObject resultJson = (JSONObject) JSONObject.parse(result);
        return JSONObject.toJavaObject(resultJson, RobotEntityRespDto.class);
    }

    @Override
    public RobotEntityRespDto deleteEntity(RobotEntityReqDto robotEntityReqDto) {
        JSONObject reqJson = (JSONObject) JSONObject.toJSON(robotEntityReqDto);
        String result = CommHttpClientUtils.doPostToAiCore(AiCoreConstant.ENTITY_DEL, reqJson);
        JSONObject resultJson = (JSONObject) JSONObject.parse(result);
        return JSONObject.toJavaObject(resultJson, RobotEntityRespDto.class);
    }

    @Override
    public RobotEntityRespDto getEntityOneInfo(RobotEntityReqDto robotEntityReqDto) {
        JSONObject reqJson = (JSONObject) JSONObject.toJSON(robotEntityReqDto);
        String result = CommHttpClientUtils.doPostToAiCore(AiCoreConstant.ENTITY_ONE, reqJson);
        JSONObject resultJson = (JSONObject) JSONObject.parse(result);
        return JSONObject.toJavaObject(resultJson, RobotEntityRespDto.class);
    }
}
