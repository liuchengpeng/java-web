package com.example.demo.service.serviceImpl;

import com.example.demo.mapper.java.mapper.AiAuthGroupAccessDoMapper;
import com.example.demo.mapper.java.mapper.AiUserDoMapperExt;
import com.example.demo.model.entityDo.AiAuthGroupAccessDo;
import com.example.demo.model.entityDo.AiUserDo;
import com.example.demo.model.entityDto.AiUserDto;
import com.example.demo.service.service.AIUserService;
import com.example.demo.util.common.encryption.sm3.Sm3Utils;
import com.example.demo.util.common.exception.AIValidateException;
import com.example.demo.util.common.vo.ResultModel;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;


@Service
@Slf4j
public class AIUserServiceImpl implements AIUserService {

    @Resource
    private AiUserDoMapperExt aiUserDoMapperExt;

    private final AiAuthGroupAccessDoMapper aiAuthGroupAccessDoMapper;

    public AIUserServiceImpl(AiAuthGroupAccessDoMapper aiAuthGroupAccessDoMapper) {
        this.aiAuthGroupAccessDoMapper = aiAuthGroupAccessDoMapper;
    }


    /**
     * 根据用户名称查询用户信息
     *
     * @param username
     * @return
     * @throws AIValidateException
     */
    @Override
    public AiUserDto loadUserByUsername(String username) throws AIValidateException {
        AiUserDto aiUserDtos = aiUserDoMapperExt.loadUserByUsername(username);
        return aiUserDtos;
    }


    @Override
    public AiUserDto getUserLists(AiUserDto aiUserDto) throws AIValidateException {

        PageHelper.startPage(3, 3);
        List<AiUserDto> pageAiUserDtos = aiUserDoMapperExt.findUserListByDto(aiUserDto);
        if (CollectionUtils.isEmpty(pageAiUserDtos)) {
            return null;
        }
        AiUserDto resultAiUserDto = new AiUserDto();
        // 得到分页的结果对象信息
        PageInfo<AiUserDto> aiUserPageInfo = new PageInfo<>(pageAiUserDtos);
        List<AiUserDto> pageList = aiUserPageInfo.getList();
        resultAiUserDto.setAiUserDtos(pageList);
        return resultAiUserDto;
    }


    @Override
    public AiUserDto login(AiUserDto aiUserDto) throws AIValidateException {

        List<AiUserDto> aiUserDtos = aiUserDoMapperExt.findUserListByDto(aiUserDto);
        AiUserDto resultAiUserDto = new AiUserDto();
        resultAiUserDto.setAiUserDtos(aiUserDtos);
        return resultAiUserDto;
    }

    @Override
    public AiUserDto getAiUserLists() {
        List<AiUserDto> aiUserDtoList = aiUserDoMapperExt.getAiUserLists();
        AiUserDto resultAiUserDto = new AiUserDto();
        resultAiUserDto.setAiUserDtos(aiUserDtoList);
        return resultAiUserDto;
    }

    @Override
    @Transactional(rollbackFor = {Exception.class})
    public int addAiUser(AiUserDto aiUserDto) {
        aiUserDto.setType("1");
        aiUserDto.setStatus(1);
        aiUserDto.setMyid(0);
        aiUserDto.setCreateTime(new Date());
        aiUserDto.setPassword(Sm3Utils.encrypt(aiUserDto.getPassword()));
        aiUserDoMapperExt.insert(aiUserDto);
        AiAuthGroupAccessDo aiAuthGroupAccessDo = new AiAuthGroupAccessDo();
        aiAuthGroupAccessDo.setUid(aiUserDto.getId());
        aiAuthGroupAccessDo.setGroupId(aiUserDto.getGroupId());
        aiAuthGroupAccessDoMapper.insert(aiAuthGroupAccessDo);
        return 1;
    }

    @Override
    public ResultModel<AiUserDto> showAiUser(Integer id) {
        ResultModel<AiUserDto> aiUserDtoResultModel = new ResultModel<>();
        AiUserDo aiUserDo = aiUserDoMapperExt.selectByPrimaryKey(id);
        if (aiUserDo == null) {
            aiUserDtoResultModel.setSuccessed(false);
            aiUserDtoResultModel.setErrorCode("1");
            aiUserDtoResultModel.setErrorDesc("查询失败");
            return aiUserDtoResultModel;
        }
        AiUserDto aiUserDto = new AiUserDto();
        BeanUtils.copyProperties(aiUserDo, aiUserDto);
        aiUserDtoResultModel.setReturnValue(aiUserDto);
        return aiUserDtoResultModel;
    }

    @Override
    public int updateAiUser(AiUserDto aiUserDto) {
        //密码不为空时，才进行加密
        if (StringUtils.isNotBlank(aiUserDto.getPassword())) {
            aiUserDto.setPassword(Sm3Utils.encrypt(aiUserDto.getPassword()));
        }
        return aiUserDoMapperExt.updateByPrimaryKeySelective(aiUserDto);
    }

    @Override
    public int deleteAiUser(Integer id) {
        return aiUserDoMapperExt.deleteByPrimaryKey(id);
    }

    @Override
    public int batchDeleteAiUser(String aiUserIds) {
        if (!aiUserIds.contains(",")) {
            return deleteAiUser(Integer.parseInt(aiUserIds));
        }
        String[] aiUserIdArray = aiUserIds.split(",");
        return aiUserDoMapperExt.batchDeleteAiUser(aiUserIdArray);
    }

    @Override
    public AiUserDto findUserByMap(Map<String, Object> params) {
        return aiUserDoMapperExt.findUserByMap(params);
    }
}
