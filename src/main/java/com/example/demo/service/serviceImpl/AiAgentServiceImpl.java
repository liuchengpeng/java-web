package com.example.demo.service.serviceImpl;

import com.example.demo.mapper.java.mapper.AiAgentDoMapper;
import com.example.demo.mapper.java.mapper.AiAgentDoMapperExt;
import com.example.demo.mapper.java.mapper.AiCompanyDoMapper;
import com.example.demo.model.entityDo.AiAgentDo;
import com.example.demo.model.entityDo.AiCompanyDo;
import com.example.demo.model.entityDto.AiAgentDto;
import com.example.demo.model.robotDO.RobotCommonReqDo;
import com.example.demo.model.robotDO.RobotCommonRespDo;
import com.example.demo.model.robotDto.RobotEntityReqDto;
import com.example.demo.model.robotDto.RobotEntityRespDto;
import com.example.demo.service.service.AiAgentService;
import com.example.demo.service.service.RobotEntityService;
import com.example.demo.service.service.RobotService;
import com.example.demo.util.common.AIManagerPage;
import com.example.demo.util.common.enumClass.AiAgentStatusEnum;
import com.example.demo.util.common.exception.AIValidateException;
import com.example.demo.util.common.vo.ResultModel;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Service
@Slf4j
public class AiAgentServiceImpl implements AiAgentService {
    @Autowired
    private AiCompanyDoMapper aiCompanyMapper;
    @Autowired
    private AiAgentDoMapper aiAgentMapper;
    @Autowired
    private RobotService robotService;
    @Autowired
    private AiAgentDoMapperExt aiAgentDoMapperExt;
    @Autowired
    private RobotEntityService robotEntityService;

    @Override
    public ResultModel<AiAgentDo> addAiAgent(AiAgentDo aiAgent, HttpServletRequest request, HttpServletResponse response) throws AIValidateException {
        ResultModel<AiAgentDo> resultModel = new ResultModel<>();
        if (aiAgent.getcId() == null){
            throw new AIValidateException("商户ID不允许为空");
        }
        AiCompanyDo aiCompany = aiCompanyMapper.selectByPrimaryKey(aiAgent.getcId());
        if (aiCompany == null){
            throw new AIValidateException("商户数据为空");
        }
        RobotCommonReqDo robotCommonReqDo  = new RobotCommonReqDo();
        robotCommonReqDo.setName(aiAgent.getaName());
        robotCommonReqDo.setTenantid(aiAgent.getcId().toString());
        RobotCommonRespDo robotCommonRespDo = robotService.create(robotCommonReqDo,request,response);
        if (robotCommonRespDo == null || !StringUtils.equals("0",robotCommonRespDo.getErrno())){
            log.error("创建机器人失败原因: "+robotCommonRespDo.getErrstr());
            throw new AIValidateException("创建机器人失败");
        }
        aiAgent.setrId(Integer.parseInt(robotCommonRespDo.getData().getNewRobot().getId()));
        aiAgent.setaAppid(robotCommonRespDo.getData().getNewRobot().getAppid());
        aiAgent.setaSecretkey(robotCommonRespDo.getData().getNewRobot().getSecretkey());
        aiAgent.setaType(0);
        aiAgent.setaTemplate(0);
        Integer status = null;
        if (StringUtils.equals(AiAgentStatusEnum.closeAI.getCode().toString(),robotCommonRespDo.getData().getNewRobot().getStatus())){
            status = AiAgentStatusEnum.close.getCode();
        }else if(StringUtils.equals(AiAgentStatusEnum.startAI.getCode().toString(),robotCommonRespDo.getData().getNewRobot().getStatus())){
            status = AiAgentStatusEnum.start.getCode();
        }else{
            status = AiAgentStatusEnum.delete.getCode();
        }
        aiAgent.setaStatus(status);
        int r = aiAgentMapper.insertSelective(aiAgent);
        return returnResult(r,resultModel,aiAgent,"新增场景记录成功","新增场景记录失败");
    }

    @Override
    public ResultModel<AiAgentDo> updateAiAgent(AiAgentDo aiAgent, HttpServletRequest request, HttpServletResponse response) throws AIValidateException {
        ResultModel<AiAgentDo> resultModel = new ResultModel<>();
        if (aiAgent.getaId() == null){
            throw new AIValidateException("场景ID不允许为空");
        }
        if (aiAgent.getcId() == null){
            throw new AIValidateException("商户ID不允许为空");
        }
        if (StringUtils.isEmpty(aiAgent.getaName())){
            throw new AIValidateException("场景名称不允许为空");
        }
        int r = aiAgentMapper.updateByPrimaryKeySelective(aiAgent);
        return returnResult(r,resultModel,aiAgent,"编辑场景记录成功","编辑场景记录失败");
    }

    @Override
    public ResultModel<AiAgentDo> deleteAiAgent(AiAgentDo aiAgent, HttpServletRequest request, HttpServletResponse response) throws AIValidateException {
        ResultModel<AiAgentDo> resultModel = new ResultModel<>();
        if (aiAgent.getaId() == null){
            throw new AIValidateException("场景ID不允许为空");
        }
        int r  = aiAgentMapper.deleteByPrimaryKey(aiAgent.getaId());
        return returnResult(r,resultModel,aiAgent,"删除场景记录成功","删除场景记录失败");
    }

    @Override
    public ResultModel<AIManagerPage> queryListAiAgent(AiAgentDto aiAgent, HttpServletRequest request, HttpServletResponse response) throws AIValidateException {
        ResultModel<AIManagerPage> resultModel = new ResultModel<>();
        PageHelper.startPage(1, 10);
        List<AiAgentDto> list = aiAgentDoMapperExt.queryList(aiAgent);
        PageInfo<AiAgentDto> aiAgentDtoPageInfo = new PageInfo<>(list);
        List<AiAgentDto> pageList = aiAgentDtoPageInfo.getList();
        resultModel.setSuccessed(true);
        aiAgent.setAiAgentDtoList(pageList);
        AIManagerPage<AiAgentDto> aiAgentDtoAIManagerPage = new AIManagerPage<AiAgentDto>(1, 10, (int) aiAgentDtoPageInfo.getTotal(), aiAgent);
        resultModel.setReturnValue(aiAgentDtoAIManagerPage);
        return resultModel;
    }

    @Override
    public RobotEntityRespDto getRobotEntityList(RobotEntityReqDto robotEntityReqDto) {
        return robotEntityService.getRobotEntityList(robotEntityReqDto);
    }

    @Override
    public RobotEntityRespDto updateEntityContent(RobotEntityReqDto robotEntityReqDto) {
        return robotEntityService.updateEntityContent(robotEntityReqDto);
    }

    private ResultModel<AiAgentDo> returnResult(int r,ResultModel<AiAgentDo> resultModel, AiAgentDo aiAgent,String successDesc,String falseDesc) throws AIValidateException {
        if (r == 0){
            log.info(falseDesc);
            resultModel.setSuccessed(false);
            resultModel.setReturnValue(aiAgent);
            resultModel.setErrorCode("1");
            resultModel.setErrorDesc(falseDesc);
        }else{
            log.info(successDesc);
            resultModel.isSuccessed();
            resultModel.setReturnValue(aiAgent);
        }
        return resultModel;
    }
}
