package com.example.demo.service.serviceImpl;

import com.alibaba.fastjson.JSONObject;
import com.example.demo.constant.AiCoreConstant;
import com.example.demo.model.robotDO.RobotTreeJumpReqDo;
import com.example.demo.model.robotDto.RobotTreeReqDto;
import com.example.demo.model.robotDto.RobotTreeRespDto;
import com.example.demo.service.service.RobotTreeService;
import com.example.demo.util.common.CommHttpClientUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class RobotTreeServiceImpl implements RobotTreeService {

    @Override
    public RobotTreeRespDto createTaskTree(RobotTreeReqDto robotTreeReqDto) {
        return callExternalInterface(robotTreeReqDto, AiCoreConstant.TREE_CREATE);
    }

    @Override
    public RobotTreeRespDto getTreeLsList(RobotTreeReqDto robotTreeReqDto) {
        return callExternalInterface(robotTreeReqDto, AiCoreConstant.TREE_LS);
    }

    @Override
    public RobotTreeRespDto getTreeOneInfo(RobotTreeReqDto robotTreeReqDto) {
        return callExternalInterface(robotTreeReqDto, AiCoreConstant.TREE_ONE);
    }

    @Override
    public RobotTreeRespDto updateTreeInfo(RobotTreeReqDto robotTreeReqDto) {
        return callExternalInterface(robotTreeReqDto, AiCoreConstant.TREE_UPDATE);
    }

    @Override
    public RobotTreeRespDto deleteTreeInfo(RobotTreeReqDto robotTreeReqDto) {
        return callExternalInterface(robotTreeReqDto, AiCoreConstant.TREE_DEL);
    }

    @Override
    public RobotTreeRespDto updateTreeJump(RobotTreeJumpReqDo robotTreeJumpReqDo) {
        JSONObject reqJson = (JSONObject) JSONObject.toJSON(robotTreeJumpReqDo);
        String result = CommHttpClientUtils.doPostToAiCore(AiCoreConstant.TREE_UP_JUMP, reqJson);
        JSONObject resultJson = (JSONObject) JSONObject.parse(result);
        return JSONObject.toJavaObject(resultJson, RobotTreeRespDto.class);
    }

    @Override
    public RobotTreeRespDto deleteTreeJump(RobotTreeReqDto robotTreeReqDto) {
        return callExternalInterface(robotTreeReqDto, AiCoreConstant.TREE_DEL_JUMP);
    }

    @Override
    public RobotTreeRespDto sortTreeJump(RobotTreeReqDto robotTreeReqDto) {
        return callExternalInterface(robotTreeReqDto, AiCoreConstant.TREE_SORT_JUMP);
    }

    private RobotTreeRespDto callExternalInterface(RobotTreeReqDto robotTreeReqDto, String interPath) {
        JSONObject reqJson = (JSONObject) JSONObject.toJSON(robotTreeReqDto);
        String result = CommHttpClientUtils.doPostToAiCore(interPath, reqJson);
        JSONObject resultJson = (JSONObject) JSONObject.parse(result);
        return JSONObject.toJavaObject(resultJson, RobotTreeRespDto.class);
    }
}
