package com.example.demo.service.serviceImpl;

import com.alibaba.fastjson.JSONObject;
import com.example.demo.constant.AiCoreConstant;
import com.example.demo.model.robotDO.RobotCommonReqDo;
import com.example.demo.model.robotDO.RobotCommonRespDo;
import com.example.demo.service.service.RobotService;
import com.example.demo.util.common.CommHttpClientUtils;
import com.example.demo.util.common.exception.AIValidateException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 机器人业务层
 */
@Service
@Slf4j
public class RobotServiceImpl implements RobotService {


    @Override
    public RobotCommonRespDo create(RobotCommonReqDo robotCommonReqDo, HttpServletRequest request, HttpServletResponse response) throws AIValidateException {
        validate(robotCommonReqDo);
        return strToJSONObject(robotCommonReqDo,AiCoreConstant.ROBOT_CREATE);
    }


    @Override
    public RobotCommonRespDo upInfo(RobotCommonReqDo robotCommonReqDo, HttpServletRequest request, HttpServletResponse response) throws AIValidateException {
        validate(robotCommonReqDo);
        return strToJSONObject(robotCommonReqDo,AiCoreConstant.ROBOT_UPINFO);
    }

    @Override
    public RobotCommonRespDo queryList(RobotCommonReqDo robotCommonReqDo, HttpServletRequest request, HttpServletResponse response) throws AIValidateException {
        validate(robotCommonReqDo);
        return strToJSONObject(robotCommonReqDo,AiCoreConstant.ROBOT_LS);
    }

    @Override
    public RobotCommonRespDo upSecretkey(RobotCommonReqDo robotCommonReqDo, HttpServletRequest request, HttpServletResponse response) throws AIValidateException {
        validate(robotCommonReqDo);
        return strToJSONObject(robotCommonReqDo,AiCoreConstant.ROBOT_UPSEC);
    }

    @Override
    public RobotCommonRespDo upStatus(RobotCommonReqDo robotCommonReqDo, HttpServletRequest request, HttpServletResponse response) throws AIValidateException {
        validate(robotCommonReqDo);
        return strToJSONObject(robotCommonReqDo,AiCoreConstant.ROBOT_UPSTATUS);
    }

    @Override
    public RobotCommonRespDo upName(RobotCommonReqDo robotCommonReqDo, HttpServletRequest request, HttpServletResponse response) throws AIValidateException {
        validate(robotCommonReqDo);
        return strToJSONObject(robotCommonReqDo,AiCoreConstant.ROBOT_UPNAME);
    }

    @Override
    public RobotCommonRespDo upOther(RobotCommonReqDo robotCommonReqDo, HttpServletRequest request, HttpServletResponse response) throws AIValidateException {
        validate(robotCommonReqDo);
        return strToJSONObject(robotCommonReqDo,AiCoreConstant.ROBOT_UPOTHER);
    }

    @Override
    public RobotCommonRespDo selectOne(RobotCommonReqDo robotCommonReqDo, HttpServletRequest request, HttpServletResponse response) throws AIValidateException {
        validate(robotCommonReqDo);
        return strToJSONObject(robotCommonReqDo,AiCoreConstant.ROBOT_ONE);
    }

    @Override
    public RobotCommonRespDo upEntryTreeId(RobotCommonReqDo robotCommonReqDo, HttpServletRequest request, HttpServletResponse response) throws AIValidateException {
        validate(robotCommonReqDo);
        return strToJSONObject(robotCommonReqDo,AiCoreConstant.ROBOT_UPENTRYTREEID);
    }

    @Override
    public RobotCommonRespDo copy(RobotCommonReqDo robotCommonReqDo, HttpServletRequest request, HttpServletResponse response) throws AIValidateException {
        validate(robotCommonReqDo);
        return strToJSONObject(robotCommonReqDo,AiCoreConstant.ROBOT_COPY);
    }

    /**
     * 参数校验
     * @param robotCommonReqDo
     */
    private void validate(RobotCommonReqDo robotCommonReqDo) {
        if (robotCommonReqDo == null){
            throw new AIValidateException("机器人对象不能为空");
        }
    }

    /**
     * 通用对robotCommonReqDo对象进行json转换
     * @param robotCommonReqDo
     * @param robotCreate
     * @return
     */
    private RobotCommonRespDo strToJSONObject(RobotCommonReqDo robotCommonReqDo, String robotCreate) {
        JSONObject jsonObject = (JSONObject) JSONObject.toJSON(robotCommonReqDo);
        String result = CommHttpClientUtils.doPostToAiCore(robotCreate,jsonObject);
        JSONObject jsonObject1 = JSONObject.parseObject(result);
        return JSONObject.toJavaObject(jsonObject1, RobotCommonRespDo.class);
    }
}
