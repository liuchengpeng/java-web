package com.example.demo.service.service;

import com.example.demo.model.robotDO.RobotTreeJumpReqDo;
import com.example.demo.model.robotDto.RobotTreeReqDto;
import com.example.demo.model.robotDto.RobotTreeRespDto;

public interface RobotTreeService {
    /**
     * 创建任务节点
     *
     * @param robotTreeReqDto 节点参数
     * @return RobotTreeRespDto
     */
    RobotTreeRespDto createTaskTree(RobotTreeReqDto robotTreeReqDto);

    /**
     * 获取机器人下的所有任务节点
     *
     * @param robotTreeReqDto 节点参数
     * @return RobotTreeRespDto
     */
    RobotTreeRespDto getTreeLsList(RobotTreeReqDto robotTreeReqDto);

    /**
     * 根据机器人ID 节点ID 查询单个节点信息
     *
     * @param robotTreeReqDto 节点参数
     * @return RobotTreeRespDto
     */
    RobotTreeRespDto getTreeOneInfo(RobotTreeReqDto robotTreeReqDto);

    /**
     * 更新指定节点信息
     *
     * @param robotTreeReqDto 节点参数
     * @return RobotTreeRespDto
     */
    RobotTreeRespDto updateTreeInfo(RobotTreeReqDto robotTreeReqDto);

    /**
     * 根据机器人ID 节点ID 删除节点信息
     *
     * @param robotTreeReqDto 节点参数
     * @return RobotTreeRespDto
     */
    RobotTreeRespDto deleteTreeInfo(RobotTreeReqDto robotTreeReqDto);

    /**
     * 根据机器人ID 节点ID 更新/添加节点的某跳转条件
     *
     * @param robotTreeJumpReqDo 节点参数
     * @return RobotTreeRespDto
     */
    RobotTreeRespDto updateTreeJump(RobotTreeJumpReqDo robotTreeJumpReqDo);

    /**
     * 根据机器人ID 节点ID 跳转体ID 删除 跳转配置
     *
     * @param robotTreeReqDto 节点参数
     * @return RobotTreeRespDto
     */
    RobotTreeRespDto deleteTreeJump(RobotTreeReqDto robotTreeReqDto);

    /**
     * 根据机器人ID 节点ID 对节点跳转配置排序
     *
     * @param robotTreeReqDto 节点参数
     * @return RobotTreeRespDto
     */
    RobotTreeRespDto sortTreeJump(RobotTreeReqDto robotTreeReqDto);
}
