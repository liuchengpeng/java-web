package com.example.demo.service.service;


import com.example.demo.model.entityDto.knowDto.KnowledgeDto;
import com.example.demo.util.common.exception.AIValidateException;

public interface KnowledgeService {


    /**
     * 创建或更新知识库信息
     *
     * @param knowledgeDto
     * @return
     */
    KnowledgeDto createOrUpdateKnowledge(KnowledgeDto knowledgeDto) throws AIValidateException;

    /**
     * 删除知识库
     *
     * @param knowledgeDto
     * @return
     */
    KnowledgeDto deleteKnowledge(KnowledgeDto knowledgeDto) throws AIValidateException;
    /**
     * 查询列表
     *
     * @param knowledgeDto
     * @return
     */
    KnowledgeDto queryList(KnowledgeDto knowledgeDto) throws AIValidateException;

    /**
     * 添加或更新一个知识词条
     *
     * @param knowledgeDto
     * @return
     */
    KnowledgeDto addOrUpdateKnowEntry(KnowledgeDto knowledgeDto) throws AIValidateException;

    /**
     * 获取一个知识词条
     *
     * @param knowledgeDto
     * @return
     */
    KnowledgeDto getOneKnow(KnowledgeDto knowledgeDto) throws AIValidateException;

    /**
     * 知识库词条列表，分页
     *
     * @param knowledgeDto
     * @return
     */
    KnowledgeDto listKnowPage(KnowledgeDto knowledgeDto) throws AIValidateException;

    /**
     * 删除一个知识词条
     *
     * @param knowledgeDto
     * @return
     */
    KnowledgeDto delknow(KnowledgeDto knowledgeDto) throws AIValidateException;

    /**
     * 建立/修改 robot 与知识库的绑定关系
     *
     * @param knowledgeDto
     * @return
     */
    KnowledgeDto knowRobotBandrela(KnowledgeDto knowledgeDto) throws AIValidateException;

    /**
     * 解除robot与知识库的绑定关系
     *
     * @param knowledgeDto
     * @return
     */
    KnowledgeDto knowRobotUnBandrela(KnowledgeDto knowledgeDto) throws AIValidateException;

    /**
     * 列出已robot绑定的知识库
     *
     * @param knowledgeDto
     * @return
     */
    KnowledgeDto listKnowRobotBandrela(KnowledgeDto knowledgeDto) throws AIValidateException;

}
