package com.example.demo.service.service;

import com.alibaba.fastjson.JSONObject;
import com.example.demo.model.entityDto.AiAuthGroupDto;

import java.util.List;

public interface AiAuthGroupService {

    /**
     * 获取所有角色列表
     *
     * @return
     */
    List<AiAuthGroupDto> getAllAiAuthGroup();

    /**
     * 根据角色 ID 修改状态
     *
     * @param id     角色ID
     * @param status 角色状态
     * @return int
     */
    int updateAiAuthGroupStatus(Integer id, Integer status);


    /**
     * 添加角色
     *
     * @param title 角色名称
     * @return int
     */
    int addAiAuthGroup(String title);


    /**
     * 校验角色名字是否存在
     *
     * @param title 角色名称
     * @return AiAuthGroupDto
     */
    AiAuthGroupDto checkAiAuthGroupTitleIsExist(String title);

    /**
     * 根据 ID 修改角色信息
     *
     * @param id     角色 ID
     * @param title  角色名称
     * @param status 角色状态
     * @return int
     */
    int updateAiAuthGroup(Integer id, String title, Integer status);


    /**
     * 根据角色 ID 删除角色
     *
     * @param id 角色ID
     * @return int
     */
    int deleteAiAuthGroup(Integer id);


    /**
     * 根据角色 ID 获取角色授权列表
     *
     * @param id 角色ID
     * @return JSONObject
     */
    JSONObject findUserAuthorizationInfo(Integer id);


    /**
     * 修改角色授权信息
     *
     * @param id      角色ID
     * @param ruleIds 授权规则ID
     * @return int
     */
    int commitUserAuthorizationInfo(Integer id, String ruleIds);
}
