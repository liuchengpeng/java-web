package com.example.demo.service.service;


import com.example.demo.model.entityDo.AiAgentDo;
import com.example.demo.model.entityDto.AiAgentDto;
import com.example.demo.model.robotDto.RobotEntityReqDto;
import com.example.demo.model.robotDto.RobotEntityRespDto;
import com.example.demo.util.common.AIManagerPage;
import com.example.demo.util.common.vo.ResultModel;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface AiAgentService {
    /**
     * 添加场景
     * @param aiAgent
     * @param request
     * @param response
     * @return
     */
    ResultModel<AiAgentDo> addAiAgent(AiAgentDo aiAgent, HttpServletRequest request, HttpServletResponse response);
    /**
     * 编辑场景
     * @param aiAgent
     * @param request
     * @param response
     * @return
     */
    ResultModel<AiAgentDo> updateAiAgent(AiAgentDo aiAgent, HttpServletRequest request, HttpServletResponse response);

    /**
     * 删除场景
     * @param aiAgent
     * @param request
     * @param response
     * @return
     */
    ResultModel<AiAgentDo> deleteAiAgent(AiAgentDo aiAgent, HttpServletRequest request, HttpServletResponse response);
    /**
     * 查询场景列表
     * @param aiAgent
     * @param request
     * @param response
     * @return
     */
    ResultModel<AIManagerPage> queryListAiAgent(AiAgentDto aiAgent, HttpServletRequest request, HttpServletResponse response);

    /**
     * 获取机器人实体
     * @param robotEntityReqDto 机器人ID参数
     * @return RobotEntityRespDto
     */
    RobotEntityRespDto getRobotEntityList(RobotEntityReqDto robotEntityReqDto);

    /**
     * 修改实体值
     * @param robotEntityReqDto 实体ID、实体值
     * @return  RobotEntityRespDto
     */
    RobotEntityRespDto updateEntityContent(RobotEntityReqDto robotEntityReqDto);
}
