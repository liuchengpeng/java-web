package com.example.demo.service.service;



import com.example.demo.model.robotDO.RobotCommonReqDo;
import com.example.demo.model.robotDO.RobotCommonRespDo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface RobotService {
    /**
     * 创建机器人
     * @param robotCommonReqDo
     * @param request
     * @param response
     * @return
     */
    RobotCommonRespDo create(RobotCommonReqDo robotCommonReqDo, HttpServletRequest request, HttpServletResponse response);
    /**
     * 更新机器人基本信息
     * @param robotCommonReqDo
     * @param request
     * @param response
     * @return
     */
    RobotCommonRespDo upInfo(RobotCommonReqDo robotCommonReqDo, HttpServletRequest request, HttpServletResponse response);

    /**
     * 查询机器人列表
     * @param robotCommonReqDo
     * @param request
     * @param response
     * @return
     */
    RobotCommonRespDo queryList(RobotCommonReqDo robotCommonReqDo, HttpServletRequest request, HttpServletResponse response);

    /**
     * 更新某机器人的secretkey
     * @param robotCommonReqDo
     * @param request
     * @param response
     * @return
     */
    RobotCommonRespDo upSecretkey(RobotCommonReqDo robotCommonReqDo, HttpServletRequest request, HttpServletResponse response);

    /**
     * 更新某机器人的状态
     * @param robotCommonReqDo
     * @param request
     * @param response
     * @return
     */
    RobotCommonRespDo upStatus(RobotCommonReqDo robotCommonReqDo, HttpServletRequest request, HttpServletResponse response);

    /**
     * 更新某机器人的名字
     * @param robotCommonReqDo
     * @param request
     * @param response
     * @return
     */
    RobotCommonRespDo upName(RobotCommonReqDo robotCommonReqDo, HttpServletRequest request, HttpServletResponse response);

    /**
     * 更新某机器人的杂项信息
     * @param robotCommonReqDo
     * @param request
     * @param response
     * @return
     */
    RobotCommonRespDo upOther(RobotCommonReqDo robotCommonReqDo, HttpServletRequest request, HttpServletResponse response);

    /**
     * 获取某机器人各项信息
     * @param robotCommonReqDo
     * @param request
     * @param response
     * @return
     */
    RobotCommonRespDo selectOne(RobotCommonReqDo robotCommonReqDo, HttpServletRequest request, HttpServletResponse response);

    /**
     * 更新机器人流程图入口节点
     * @param robotCommonReqDo
     * @param request
     * @param response
     * @return
     */
    RobotCommonRespDo upEntryTreeId(RobotCommonReqDo robotCommonReqDo, HttpServletRequest request, HttpServletResponse response);

    /**
     * 将某机器人所有信息覆盖至另一个
     * @param robotCommonReqDo
     * @param request
     * @param response
     * @return
     */
    RobotCommonRespDo copy(RobotCommonReqDo robotCommonReqDo, HttpServletRequest request, HttpServletResponse response);
}
