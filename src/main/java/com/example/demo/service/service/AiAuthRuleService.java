package com.example.demo.service.service;

import com.example.demo.model.entityDo.AiAuthRuleDo;
import com.example.demo.model.entityDto.AiAuthRuleDto;
import com.example.demo.util.common.vo.ResultModel;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface AiAuthRuleService {
    /**
     * 查询权限列表
     * @param aiAuthRuleDto
     * @param request
     * @param response
     * @return
     */
    ResultModel<AiAuthRuleDto> queryList(AiAuthRuleDto aiAuthRuleDto, HttpServletRequest request, HttpServletResponse response);

    /**
     * 编辑权限数据
     * @param aiAuthRuleDo
     * @param request
     * @param response
     * @return
     */
    ResultModel<AiAuthRuleDto> update(AiAuthRuleDo aiAuthRuleDo, HttpServletRequest request, HttpServletResponse response);

    /**
     * 删除权限数据
     * @param id
     * @param request
     * @param response
     * @return
     */
    ResultModel<AiAuthRuleDto> detele(Integer id, HttpServletRequest request, HttpServletResponse response);

    /**
     * 新增权限数据
     * @param aiAuthRuleDo
     * @param request
     * @param response
     * @return
     */
    ResultModel<AiAuthRuleDto> insert(AiAuthRuleDo aiAuthRuleDo, HttpServletRequest request, HttpServletResponse response);

    List<AiAuthRuleDto> getAiAuthRuleDtoTree(Integer id);
}
