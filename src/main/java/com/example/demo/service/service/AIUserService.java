package com.example.demo.service.service;


import com.example.demo.model.entityDto.AiUserDto;
import com.example.demo.util.common.exception.AIValidateException;
import com.example.demo.util.common.vo.ResultModel;

import java.util.Map;

public interface AIUserService {


    /**
     * 用户登录，根据用户名查询密码
     *
     * @return
     * @throws AIValidateException
     * @parameter username
     */
    AiUserDto loadUserByUsername(String username) throws AIValidateException;


    /**
     * 获取用户列表，可分页
     */
    AiUserDto getUserLists(AiUserDto aiUserDto) throws AIValidateException;

    /**
     * 用户登录
     *
     * @param aiUserDto
     * @return
     * @throws AIValidateException
     */
    AiUserDto login(AiUserDto aiUserDto) throws AIValidateException;


    /**
     * 获取用户列表
     *
     * @return AiUserDto
     */
    AiUserDto getAiUserLists();


    /**
     * 添加用户
     *
     * @param aiUserDto 用户信息
     * @return ResultModel<AiUserDto>
     */
    int addAiUser(AiUserDto aiUserDto);

    /**
     * 根据用户 ID 回显用户信息
     *
     * @param id 用户ID
     * @return ResultModel<AiUserDto>
     */
    ResultModel<AiUserDto> showAiUser(Integer id);

    /**
     * 修改用户信息
     *
     * @param aiUserDto 用户信息
     * @return int
     */
    int updateAiUser(AiUserDto aiUserDto);

    /**
     * 根据用户 ID 删除用户
     *
     * @param id 用户ID
     * @return int
     */
    int deleteAiUser(Integer id);

    /**
     * 批量删除用户
     *
     * @param aiUserIds 多个用户ID
     * @return ResultModel<AiUserDto>
     */
    int batchDeleteAiUser(String aiUserIds);


    /**
     * 根据 Map 里面的参数进行查询
     *
     * @param params Map
     * @return AiUserDto
     */
    AiUserDto findUserByMap(Map<String, Object> params);
}
