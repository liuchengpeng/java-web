package com.example.demo.service.service;

import com.example.demo.model.robotDto.RobotEntityReqDto;
import com.example.demo.model.robotDto.RobotEntityRespDto;

public interface RobotEntityService {

    /**
     * 创建机器人实体
     *
     * @param robotEntityReqDto 实体信息
     * @return RobotEntityRespDto
     */
    RobotEntityRespDto createRobotEntity(RobotEntityReqDto robotEntityReqDto);

    /**
     * 根据机器人ID 获取实体列表
     *
     * @param robotEntityReqDto 参数
     * @return RobotEntityRespDto
     */
    RobotEntityRespDto getRobotEntityList(RobotEntityReqDto robotEntityReqDto);

    /**
     * 根据机器人 ID ，实体 ID 修改实体姓名，描述
     *
     * @param robotEntityReqDto 实体参数
     * @return RobotEntityRespDto
     */
    RobotEntityRespDto updateEntityName(RobotEntityReqDto robotEntityReqDto);

    /**
     * 根据机器人 ID ，实体 ID 修改实体值
     *
     * @param robotEntityReqDto 实体参数
     * @return RobotEntityRespDto
     */
    RobotEntityRespDto updateEntityContent(RobotEntityReqDto robotEntityReqDto);

    /**
     * 根据机器人 ID ，实体 ID 删除实体
     *
     * @param robotEntityReqDto 实体参数
     * @return RobotEntityRespDto
     */
    RobotEntityRespDto deleteEntity(RobotEntityReqDto robotEntityReqDto);

    /**
     * 根据机器人 ID ，实体 ID 查询单个实体所有信息
     *
     * @param robotEntityReqDto 实体参数
     * @return RobotEntityRespDto
     */
    RobotEntityRespDto getEntityOneInfo(RobotEntityReqDto robotEntityReqDto);
}
