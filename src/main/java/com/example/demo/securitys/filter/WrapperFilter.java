package com.example.demo.securitys.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class WrapperFilter implements Filter {
    
    @Override
	public void destroy() {
        
    }

    @Override
	public void doFilter(ServletRequest request, ServletResponse response,
						 FilterChain chain) throws IOException, ServletException {
    	 HttpServletResponse res = (HttpServletResponse)response;
    	 res.setHeader("Access-Control-Allow-Origin", "*");
    	 res.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
    	 res.setHeader("Access-Control-Max-Age", "1800");
    	 res.setHeader("Access-Control-Allow-Headers", "X-Requested-With, Content-Type, Accept, Authorization");
    	 ServletRequest requestWrapper = null;
		 requestWrapper = new BodyHttpServletRequestWrapper((HttpServletRequest) request);
		 chain.doFilter(requestWrapper, res);  
    }
    
    /**
     * 初始化函数时，需要获取排除在外的url
     */
    @Override
	public void init(FilterConfig config) throws ServletException {
    
    }
}
