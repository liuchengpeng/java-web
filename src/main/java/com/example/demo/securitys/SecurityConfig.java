package com.example.demo.securitys;

import com.example.demo.securitys.loginProcessing.MyAuthenticationFailureHandler;
import com.example.demo.securitys.loginProcessing.MyAuthenticationSuccessHandler;
import com.example.demo.securitys.loginProcessing.MyLogoutSuccessHandler;
import com.example.demo.securitys.securityProperites.MySecurityProperties;
import com.example.demo.securitys.user.MyUserDetailsService;
import com.example.demo.util.common.custom.AICustomUrl;
import com.example.demo.util.common.encryption.PasswordEncoderExt;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

import java.util.ArrayList;

@Slf4j
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private MyUserDetailsService myUserDetailsService;

    @Autowired
    private MySecurityProperties mySecurityProperties;

    @Autowired
    private MyAuthenticationSuccessHandler myAuthenticationSuccessHandler;

    @Autowired
    private MyAuthenticationFailureHandler myAuthenticationFailureHandler;

    @Autowired
    private MyLogoutSuccessHandler myLogoutSuccessHandler;

    /**
     * 验证
     *
     * @param auth
     * @throws Exception
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(myUserDetailsService).passwordEncoder(new PasswordEncoderExt());//自定义密码验证
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {
//        super.configure(http);
        ArrayList<String> antMatchers = new ArrayList<String>();

        antMatchers.add(AICustomUrl.DEFAULT_UNAUTHENTICATION_URL);
        antMatchers.add(mySecurityProperties.getLoginPage());
        antMatchers.add("/");

        String[] antMatchersArray = new String[antMatchers.size()];
        String[] antMatchersArrays = antMatchers.toArray(antMatchersArray);

        http.authorizeRequests()
                .antMatchers(antMatchersArrays).permitAll()// 设置不拦截的请求
                .anyRequest().authenticated()
                .and()
                .logout().permitAll()
                .and()
                .formLogin()
                .loginPage(AICustomUrl.DEFAULT_UNAUTHENTICATION_URL)//设置登录页面
                .loginProcessingUrl(AICustomUrl.DEFAULT_LOGIN_PROCESSING_URL_FORM)//自定义的登录接口
                .successHandler(myAuthenticationSuccessHandler)//登录成功处理
                .failureHandler(myAuthenticationFailureHandler)//登录失败处理

//                .defaultSuccessUrl("/user/home").permitAll()// 登录成功之后，默认跳转的页面
                // 基于token，所以不需要session
//                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .and()
                .logout()
                .logoutSuccessHandler(myLogoutSuccessHandler)
                .logoutUrl(AICustomUrl.DEFAULT_LOGOUT_PROCESSING_URL_FORM)
                .and()
                //基于token，所以不需要session
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                // 由于使用的是JWT，我们这里不需要csrf
                .and().csrf().disable();

    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        super.configure(web);
        web.ignoring().antMatchers("/webjars/**/*", "/**/*.css", "/**/*.js");
        web.ignoring().antMatchers("/js/**", "/css/**", "/images/**");
        web.ignoring().antMatchers("/authentication/require");
        web.ignoring().antMatchers("/authentication/form");
        // TODO 关闭spring security
//        web.ignoring().antMatchers("/**");
    }


}
