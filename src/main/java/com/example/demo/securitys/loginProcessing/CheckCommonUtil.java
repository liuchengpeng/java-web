package com.example.demo.securitys.loginProcessing;

import com.example.demo.model.entityDto.ResultCheck;
import com.example.demo.util.common.vo.ResultModel;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;

public class CheckCommonUtil {

    /**
     * 登录成功登出成功通用返回方法
     *
     * @param authentication
     * @param token
     * @return
     */
    public static Object commonResult(Authentication authentication, String token) {
        String username = null;
        Object principal = authentication.getPrincipal();
        if (principal instanceof UserDetails) {
            username = ((UserDetails) principal).getUsername();
        }
        ResultModel<ResultCheck> resultModel = new ResultModel<ResultCheck>();
        ResultCheck resultCheck = new ResultCheck();
        // http状态码（200）
        resultCheck.setCode(HttpStatus.OK.toString());
        resultCheck.setUsername(username);
        resultCheck.setToken(token);
        resultModel.setReturnValue(resultCheck);

        return resultModel;

    }
}
