package com.example.demo.securitys.loginProcessing;

import com.example.demo.securitys.jwt.JwtUtil;
import com.example.demo.securitys.securityProperites.MySecurityJwtProperties;
import com.example.demo.securitys.securityProperites.MySecurityProperties;
import com.example.demo.securitys.user.MyUserDetails;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * 自定义登录成功处理
 */
@Slf4j
@Component("myAuthenticationSuccessHandler")
public class MyAuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

    @Autowired
    private MySecurityProperties mySecurityProperties;
    @Autowired
    private MySecurityJwtProperties mySecurityJwtProperties;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private JwtUtil jwtUtil;

    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        log.info("登录成功");
        String username;
        Object principal = authentication.getPrincipal();
        if (principal instanceof MyUserDetails) {
            username = ((MyUserDetails) principal).getUsername();
        }
        MyUserDetails myUserDetails = new MyUserDetails();
        String token = jwtUtil.createJWT(mySecurityJwtProperties.getTokenExpiration(), myUserDetails);
        Object obj = CheckCommonUtil.commonResult(authentication, token);

        String resultModelstr = objectMapper.writeValueAsString(obj);
        log.info("resultModelstr = " + resultModelstr);
        response.getWriter().write(resultModelstr);
    }

}
