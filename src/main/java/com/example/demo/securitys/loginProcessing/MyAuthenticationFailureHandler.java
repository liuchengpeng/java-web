package com.example.demo.securitys.loginProcessing;

import com.example.demo.model.entityDto.ResultCheck;
import com.example.demo.util.common.vo.ResultModel;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 自定义登录失败处理
 */
@Slf4j
@Component("myAuthenticationFailureHandler")
public class MyAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {

    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        log.info("登录失败");
        // 配置登录方式为Json
        response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());// 状态码500
        response.setContentType("application/json;charset= UTF-8");
        ResultModel<ResultCheck> resultModel = new ResultModel<ResultCheck>();
        ResultCheck resultLogin = new ResultCheck();

        // http状态码（500）
        resultLogin.setCode(HttpStatus.INTERNAL_SERVER_ERROR.toString());
        resultModel.setReturnValue(resultLogin);
        String resultModelstr = objectMapper.writeValueAsString(resultModel);
        log.info("resultModelstr = " + resultModelstr);
        response.getWriter().write(resultModelstr);

    }
}
