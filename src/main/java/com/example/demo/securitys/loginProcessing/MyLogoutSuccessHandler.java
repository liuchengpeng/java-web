package com.example.demo.securitys.loginProcessing;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 自定义登出处理
 */
@Slf4j
@Component("myLogoutSuccessHandler")
public class MyLogoutSuccessHandler implements LogoutSuccessHandler {

    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        log.info("登出成功");
        response.setContentType("application/json;charset= UTF-8");
        response.setStatus(HttpStatus.OK.value());// 状态码200

        Object obj = CheckCommonUtil.commonResult(authentication, null);
        String resultModelstr = objectMapper.writeValueAsString(obj);
        log.info("resultModelstr = " + resultModelstr);
        response.getWriter().write(resultModelstr);

    }


}

