package com.example.demo.securitys.user;

import com.example.demo.model.entityDto.AiUserDto;
import com.example.demo.service.service.AIUserService;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

/**
 * 处理用户获取逻辑
 */
@RestController
@Log4j
@Component
public class MyUserDetailsService implements UserDetailsService {
    @Autowired
    private AIUserService aIUserService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        // 根据用户名查询用户信息
        AiUserDto userDto = aIUserService.loadUserByUsername(username);
        if (userDto != null) {
            ArrayList<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
            authorities.add(new SimpleGrantedAuthority("ROLE_SPITTER"));
            //逗号隔开字符串转换成需要的对象的集合
//            AuthorityUtils.commaSeparatedStringToAuthorityList("");
            //封装用户信息
            MyUserDetails myUserDetails = new MyUserDetails();
            myUserDetails.setUsername(userDto.getUsername());
            myUserDetails.setPassword(userDto.getPassword());
            myUserDetails.setId(userDto.getId());
            myUserDetails.setAuthorities(authorities);
            return myUserDetails;
//            return new User(userDto.getUsername(), userDto.getPassword(), authorities);
        }
        throw new UsernameNotFoundException("The user name " + username + " can not be found!");
    }


}

