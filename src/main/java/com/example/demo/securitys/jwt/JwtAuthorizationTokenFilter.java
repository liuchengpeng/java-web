package com.example.demo.securitys.jwt;

import com.example.demo.securitys.securityProperites.MySecurityJwtProperties;
import com.example.demo.util.common.vo.ResultModel;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.HandlerMethod;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * 校验token过滤器
 */
@Slf4j
public class JwtAuthorizationTokenFilter extends HandlerInterceptorAdapter {

    private UserDetailsService userDetailsService;
    private String tokenHeader;
    @Autowired
    private JwtUtil jwtUtil;
    @Autowired
    private MySecurityJwtProperties mySecurityJwtProperties;


    /**
     * <pre>
     *  三个方法的执行顺序如下：
     *  preHandler -> Controller -> postHandler -> model渲染-> afterCompletion
     * </pre>
     */


    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        // 从 http 请求头中取出 token
//        String token = request.getHeader("token");
        String token = request.getHeader("Authorization");
        // 执行认证
        /*if (token == null) {
            // 如果不是映射到方法直接通过
//            if (!(handler instanceof HandlerMethod)) {
//                return true;
//            }
            throw new RuntimeException("无token，请重新登录");
        }*/
        ResultModel<String> resultModel = new ResultModel<>();
        if (token == null) {
            return returnResult(resultModel,response,"1004","token is null ");
        }

        try{
            Claims claims = jwtUtil.parseJWT(token, null);
            System.out.println(claims.getId());
            System.out.println(claims.getSubject());
            Boolean verify = jwtUtil.isVerify(token);
            if (!verify) {
                throw new RuntimeException("非法访问！");
            }
        }catch (ExpiredJwtException e){
            log.error("token已超时,异常",e);
            return returnResult(resultModel,response,"1005","token is overtime");
//            throw new RuntimeException("token已失效");
        }
        return true;
    }

    private boolean returnResult(ResultModel<String> resultModel, HttpServletResponse response,String errorCode,String errorDesc) {
        PrintWriter out = null;
        try {
            response.setCharacterEncoding("utf-8");
            out = response.getWriter();
            resultModel.setErrorCode(errorCode);
            resultModel.setErrorDesc(errorDesc);
            out.print(JSONObject.fromObject(resultModel));
            out.close();
        } catch (IOException e) {
            log.error("token校验IO异常",e);
        }
        return false;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}

