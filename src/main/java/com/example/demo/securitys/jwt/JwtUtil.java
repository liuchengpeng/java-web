package com.example.demo.securitys.jwt;

import com.example.demo.securitys.securityProperites.MySecurityJwtProperties;
import com.example.demo.securitys.user.MyUserDetails;
import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Slf4j
@Component
public class JwtUtil {

    //    private static String secret = "mySecret";
    private static String issuer = "aiCall";
    @Autowired
    private MySecurityJwtProperties mySecurityJwtProperties;

    /**
     * 用户登录成功后生成Jwt
     * 使用Hs256算法  私匙使用配置文件里边的配置
     *
     * @param ttlMillis jwt过期时间
     * @param user      登录成功的user对象
     * @return
     */
    public String createJWT(long ttlMillis, MyUserDetails user) {
        //指定签名的时候使用的签名算法，也就是header那部分，jjwt已经将这部分内容封装好了。
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

        //创建payload的私有声明（根据特定的业务需要添加，如果要拿这个做验证，一般是需要和jwt的接收方提前沟通好验证方式的）
        Map<String, Object> claims = new HashMap<String, Object>();
//        claims.put("id", user.getId());
        claims.put("username", user.getUsername());
        claims.put("userid", user.getId());
        claims.put("aIcustom", mySecurityJwtProperties.getAIcustom());
        //生成签名的时候使用的秘钥secret,这个方法本地封装了的，一般可以从本地配置文件中读取，切记这个秘钥不能外露哦。它就是你服务端的私钥，在任何场景都不应该流露出去。一旦客户端得知这个secret, 那就意味着客户端是可以自我签发jwt了。
        String key = mySecurityJwtProperties.getTokenSecret();

        //生成签发人
        String subject = user.getUsername();
        /**
         *
         * <pre>
         *  iss: jwt签发者
         *  sub: jwt所面向的用户
         *  aud: 接收jwt的一方
         *  exp: jwt的过期时间，这个过期时间必须要大于签发时间
         *  nbf: 定义在什么时间之前，该jwt都是不可用的.
         *  iat: jwt的签发时间
         *  jti: jwt的唯一身份标识，主要用来作为一次性token,从而回避重放攻击。
         * </pre>
         */

        // 下面就是在为payload添加各种标准声明和私有声明了
        // 这里其实就是new一个JwtBuilder，设置jwt的body
        JwtBuilder builder =
                Jwts.builder()
                        // 如果有私有声明，一定要先设置这个自己创建的私有的声明，这个是给builder的claim赋值，一旦写在标准的声明赋值之后，就是覆盖了那些标准的声明的
                        .setClaims(claims)
                        // 设置jti(JWT ID)：是JWT的唯一标识，根据业务需要，这个可以设置为一个不重复的值，主要用来作为一次性token,从而回避重放攻击。
                        .setId(UUID.randomUUID().toString())
                        // iat: jwt的签发时间
                        .setIssuedAt(new Date())
                        .setIssuer(issuer)
                        // 代表这个JWT的主体，即它的所有人，这个是一个json格式的字符串，可以存放什么userid，roldid之类的，作为什么用户的唯一标志。
                        .setSubject(subject)
                        .setExpiration(new Date(System.currentTimeMillis() + ttlMillis))
                        // 设置签名使用的签名算法和签名使用的秘钥
                        .signWith(signatureAlgorithm, key);
//        if (ttlMillis >= 0) {
//            long expMillis = nowMillis + ttlMillis;
//            Date exp = new Date(expMillis);
//            //设置过期时间
//            builder.setExpiration(exp);
//        }
        return builder.compact();
    }


    /**
     * Token的解密
     *
     * @param token 加密后的token
     * @param user  用户的对象
     * @return
     */
    public Claims parseJWT(String token, MyUserDetails user) {
        //签名秘钥，和生成的签名的秘钥一模一样
        String key = mySecurityJwtProperties.getTokenSecret();

        //得到DefaultJwtParser
        Claims claims = Jwts.parser()
                //设置签名的秘钥
                .setSigningKey(key)
                //设置需要解析的jwt
                .parseClaimsJws(token).getBody();
        return claims;
    }

//    public static void main(String[] args) {
//        MyUserDetails user = new MyUserDetails();
//        user.setUsername("xiaoliu");
//// Claims claims = Jwts.claims().setExpiration(new Date(System.currentTimeMillis() + 3600000));
//        //
//        String token = createJWT(60 * 1000, user);
//        log.info("token = " + token);
//        // 解密token
//        Claims claims = parseJWT(token, user);
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        log.info("claims.getExpiration() = " + sdf.format(claims.getExpiration()));
//        log.info("claims.getSubject() = " + claims.getSubject());
//        log.info("claims.getId() = " + claims.getId());
//        log.info("claims.getIssuer() = " + claims.getIssuer());
//        log.info("claims.getIssuedAt() = " + sdf.format(claims.getIssuedAt()));
//
//
//    }

    /**
     * 校验token 在这里可以使用官方的校验，我这里校验的是token中携带的密码于数据库一致的话就校验通过
     *
     * @param token
     * @return
     */
    public Boolean isVerify(String token) throws SignatureException, ExpiredJwtException {
        try {
            // 签名秘钥，和生成的签名的秘钥一模一样
            String key = mySecurityJwtProperties.getTokenSecret();
            // 得到DefaultJwtParser
            Claims claims =
                    Jwts.parser()
                            // 设置签名的秘钥
                            .setSigningKey(key)
                            // 设置需要解析的jwt
                            .parseClaimsJws(token)
                            .getBody();
            // 判断指定的Key是否有问题
            if (claims.get("aIcustom").equals(mySecurityJwtProperties.getAIcustom())) {
                return true;
            }
        }
        // 在解析JWT字符串时，如果密钥不正确，将会解析失败，抛出SignatureException异常，说明该JWT字符串是伪造的
        // 在解析JWT字符串时，如果‘过期时间字段’已经早于当前时间，将会抛出ExpiredJwtException异常，说明本次请求已经失效
        catch (SignatureException e) {
            log.info("该token是伪造的");
        } catch (ExpiredJwtException e) {
            log.info("该token已过期");
        }
        return false;
    }
}
