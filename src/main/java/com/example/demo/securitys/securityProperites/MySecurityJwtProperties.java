package com.example.demo.securitys.securityProperites;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 读取配置中所有以 ai.security.jwt 开头的配置项
 */
@Data
@Component // 添加 SecurityProperties 到 spring 容器中
@ConfigurationProperties(prefix = "ai.security.jwt")
public class MySecurityJwtProperties {

    private String header = "defaultAuthorization";
    private String tokenSecret = "defaultMySecret";
    private long tokenExpiration = 60 * 15;
    private String tokenHead = "Bearer ";
    private String aIcustom = "";

}
