package com.example.demo.securitys.securityProperites;

import com.example.demo.util.common.custom.AICustomUrl;
import com.example.demo.util.common.enumClass.LoginTypeEnum;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 读取配置中所有以 ai.security.browser 开头的配置项
 */
@Data
@Component // 添加 SecurityProperties 到 spring 容器中
@ConfigurationProperties(prefix = "ai.security.browser")
public class MySecurityProperties {

    // 默认登录页，当在配置文件里边没有指定登录页时生效
    private String loginPage = AICustomUrl.DEFAULT_LOGIN_PAGE_URL;
    private LoginTypeEnum loginType = LoginTypeEnum.JSON;// 默认返回json格式

}
