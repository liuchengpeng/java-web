package com.example.demo.config;

import com.example.demo.securitys.jwt.JwtAuthorizationTokenFilter;
import com.example.demo.util.common.custom.AICustomUrl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;


/**
 * 拦截器配置
 */
@Configuration
@Component
public class InterceptorConfiguration extends WebMvcConfigurerAdapter {

    @Bean
    public HandlerInterceptorAdapter getJwtAuthorizationTokenFilter() {

        return new JwtAuthorizationTokenFilter();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 拦截规则
        registry.addInterceptor(getJwtAuthorizationTokenFilter())
                .addPathPatterns("/**")
                .excludePathPatterns(AICustomUrl.DEFAULT_UNAUTHENTICATION_URL)
                .excludePathPatterns(AICustomUrl.DEFAULT_LOGIN_PROCESSING_URL_FORM);
        super.addInterceptors(registry);
    }
}
