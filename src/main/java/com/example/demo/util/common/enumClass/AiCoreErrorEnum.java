package com.example.demo.util.common.enumClass;

/**
 * 机器错误通用码
 */
public enum AiCoreErrorEnum {

    _500("500", "后端服务器错误"), _600("600", "传参错误"),
    _700("700", "domainid/secretkey错误或不存在"), _701("701", "domain被禁用"),
    _702("702", "权限不足"), _703("703", "要操作的robot不存在")
    ;

    /**
     * 结果码
     */
    private String code;

    /**
     * 结果描述
     */
    private String desc;


    /**
     * @param code
     * @param desc
     */
    AiCoreErrorEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @return the desc
     */
    public String getDesc() {
        return desc;
    }
}
