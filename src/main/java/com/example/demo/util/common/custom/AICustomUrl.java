package com.example.demo.util.common.custom;

public class AICustomUrl {
    /**
     * 默认登录页面
     */
    public final static String DEFAULT_LOGIN_PAGE_URL = "/imooc-signIn.html";
    /**
     * 当请求需要身份认证时，默认跳转的url
     */
    public final static String DEFAULT_UNAUTHENTICATION_URL = "/authentication/require";
    /**
     * 默认的用户名密码登录请求处理url
     */
    public final static String DEFAULT_LOGIN_PROCESSING_URL_FORM = "/authentication/form";

    /**
     * 默认的用户名密码登录请求处理url
     */
    public final static String DEFAULT_LOGOUT_PROCESSING_URL_FORM = "/authentication/out";
}
