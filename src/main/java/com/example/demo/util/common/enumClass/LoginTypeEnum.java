package com.example.demo.util.common.enumClass;

/**
 * 定义返回前端类型是重定向还是Json形式，
 */
public enum LoginTypeEnum {
    REDIRECT,
    JSON
}
