/*
 * Copyright 2017 Alibaba.com All right reserved. This software is the confidential and proprietary information of
 * Alibaba.com ("Confidential Information"). You shall not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the license agreement you entered into with Alibaba.com.
 */
package com.example.demo.util.common;

import java.util.List;

/**
 *
 * @author cheng
 *
 */
public class QueryCommonModel {

	/**
	 * 项目id列表
	 */
	private List<String> proList;
	private Long projectId;
	/**
	 * 被投主体id
	 */
	private Long enterpriseSubjectId;

	public Long getProjectId() {
		return projectId;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	/**
	 * @return the proList
	 */
	public List<String> getProList() {
		return proList;
	}

	/**
	 * @param proList the proList to set
	 */
	public void setProList(List<String> proList) {
		this.proList = proList;
	}

	/**
	 * @return the enterpriseSubjectId
	 */
	public Long getEnterpriseSubjectId() {
		return enterpriseSubjectId;
	}

	/**
	 * @param enterpriseSubjectId the enterpriseSubjectId to set
	 */
	public void setEnterpriseSubjectId(Long enterpriseSubjectId) {
		this.enterpriseSubjectId = enterpriseSubjectId;
	}

}
