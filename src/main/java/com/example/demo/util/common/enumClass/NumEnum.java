/*
 * Copyright 2014 Alibaba.com All right reserved. This software is the confidential and proprietary information of
 * Alibaba.com ("Confidential Information"). You shall not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the license agreement you entered into with Alibaba.com.
 */
package com.example.demo.util.common.enumClass;

/**
 *
 * @author cheng
 *
 */
public enum NumEnum {
	/** 0 */
	ZERO(0),
	/** 1 */
	ONE(1),
	/** 5 */
	FIVE(5),
	/** 10 */
	TEN(10),
	/** 20 */
	TWENTY(20),
	/** 50 */
	FIFTY(50),
	/** 100 */
	HUNDRED(100),
	/** 10000 */
	ALL(10000),

	;

	private int value;

	public int getValue() {
		return this.value;
	}

	private NumEnum(int value) {
		this.value = value;
	}
}
