package com.example.demo.util.common.enumClass;

/**
 * 场景列表机器人状态
 */
public enum AiAgentStatusEnum {
    //本地数据库的枚举
    close(0, "关闭"), start(1, "开启"),
    delete(2, "删除"),
    //人工方的枚举
    closeAI(10, "关闭"), startAI(20, "开启"),
    deleteAI(30, "删除")
    ;

    /**
     * 结果码
     */
    private Integer code;

    /**
     * 结果描述
     */
    private String desc;


    /**
     * @param code
     * @param desc
     */
    AiAgentStatusEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    /**
     * @return the code
     */
    public Integer getCode() {
        return code;
    }

    /**
     * @return the desc
     */
    public String getDesc() {
        return desc;
    }
}
