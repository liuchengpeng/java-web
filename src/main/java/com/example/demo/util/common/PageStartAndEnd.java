package com.example.demo.util.common;

import java.io.Serializable;

import com.example.demo.util.common.enumClass.NumEnum;

/**
 * 分页sql的开始和结束游标
 *
 * @author cheng
 */
public class PageStartAndEnd implements Serializable {

	private static final long serialVersionUID = 7646538688382638700L;

	/**
	 * RowNum开始游标
	 */
	private Integer startIndex;

	/**
	 * RowNum结束游标
	 */
	private Integer endIndex;

	/**
	 * 每页数量
	 */
	private Integer pageSize = NumEnum.TEN.getValue();

	/**
	 * 当前页码
	 */
	private Integer pageIndex = NumEnum.ONE.getValue();

	/**
	 * 排序字段
	 */
	private String orderColumn;

	/**
	 * 排序方式
	 */
	private String orderType;

	/**
	 * 基本构造器
	 */
	public PageStartAndEnd() {

	}

	/**
	 * 分页参数构造器
	 *
	 * @param pageSize
	 * @param pageIndex
	 */
	public PageStartAndEnd(int pageSize, int pageIndex) {
		super();
		this.startIndex = ((pageIndex - 1) * pageSize) + 1;
		this.endIndex = pageIndex * pageSize;
		this.pageSize = pageSize;
		this.pageIndex = pageIndex;
	}

	/**
	 * 排序参数构造器
	 *
	 * @param pageSize
	 * @param pageIndex
	 */
	public PageStartAndEnd(String orderColumn, String orderType) {
		super();
		this.orderColumn = orderColumn;
		this.orderType = orderType;
	}

	/**
	 * 分页排序参数构造器
	 *
	 * @param startIndex
	 * @param endIndex
	 * @param pageSize
	 * @param pageIndex
	 */
	public PageStartAndEnd(int pageSize, int pageIndex, String orderColumn, String orderType) {
		super();
		this.startIndex = ((pageIndex - 1) * pageSize) + 1;
		this.endIndex = pageIndex * pageSize;
		this.pageSize = pageSize;
		this.pageIndex = pageIndex;
		this.orderColumn = orderColumn;
		this.orderType = orderType;
	}

	/**
	 * @return the startIndex
	 */
	public Integer getStartIndex() {
		this.startIndex = ((pageIndex - 1) * pageSize) + 1;
		return startIndex;
	}

	/**
	 * @param startIndex the startIndex to set
	 */
	public void setStartIndex(int startIndex) {
		this.startIndex = startIndex;
	}

	/**
	 * @return the endIndex
	 */
	public Integer getEndIndex() {
		this.endIndex = pageIndex * pageSize;
		return endIndex;
	}

	/**
	 * @param endIndex the endIndex to set
	 */
	public void setEndIndex(int endIndex) {
		this.endIndex = endIndex;
	}

	/**
	 * @return the pageSize
	 */
	public Integer getPageSize() {
		return pageSize;
	}

	/**
	 * @param pageSize the pageSize to set
	 */
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * @return the pageIndex
	 */
	public Integer getPageIndex() {
		return pageIndex;
	}

	/**
	 * @param pageIndex the pageIndex to set
	 */
	public void setPageIndex(int pageIndex) {
		this.pageIndex = pageIndex;
	}

	/**
	 * @return the orderColumn
	 */
	public String getOrderColumn() {
		return orderColumn;
	}

	/**
	 * @param orderColumn the orderColumn to set
	 */
	public void setOrderColumn(String orderColumn) {
		this.orderColumn = orderColumn;
	}

	/**
	 * @return the orderType
	 */
	public String getOrderType() {
		return orderType;
	}

	/**
	 * @param orderType the orderType to set
	 */
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
}
