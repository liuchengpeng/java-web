package com.example.demo.util.common.enumClass;

/**
 * @author le.gao
 */

public enum DataOperTypeEnum {
    SAVE("save", "保存"),
    UPDATE("update", "修改");

    private String operType;
    private String operTypeDesc;

    DataOperTypeEnum(String operType, String operTypeDesc) {
        this.operType = operType;
        this.operTypeDesc = operTypeDesc;
    }

    public String getOperType() {
        return operType;
    }

    public void setOperType(String operType) {
        this.operType = operType;
    }

    public String getOperTypeDesc() {
        return operTypeDesc;
    }

    public void setOperTypeDesc(String operTypeDesc) {
        this.operTypeDesc = operTypeDesc;
    }
}
