package com.example.demo.util.common.encryption;

import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * 自定义密码验证
 */
public class PasswordEncoderExt implements PasswordEncoder {

    //    private final String SALT = "texun";
    private final String SALT = "";

    /**
     * 加密方法
     *
     * @param rawPassword
     * @return
     */
    @Override
    public String encode(CharSequence rawPassword) {
        Md5PasswordEncoder encoder = new Md5PasswordEncoder();
        return encoder.encodePassword(rawPassword.toString(), SALT);
    }

    /**
     * 匹配方法
     *
     * @param rawPassword
     * @param encodedPassword
     * @return
     */
    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        // encodedPassword=e10adc3949ba59abbe56e057f20f883e rawPassword =123456
        Md5PasswordEncoder encoder = new Md5PasswordEncoder();
        return encoder.isPasswordValid(encodedPassword, rawPassword.toString(), SALT);
    }


//    public static void main(String[] args) {
//        PasswordEncoderExt passwordEncoderExt = new PasswordEncoderExt();
//        // encodedPassword=e10adc3949ba59abbe56e057f20f883e rawPassword =123456
////        matches()
//        String rawPassword = "e10adc3949ba59abbe56e057f20f883e";
//        boolean s = passwordEncoderExt.matches("123456", rawPassword);
//        System.out.println(s);
//    }
}
