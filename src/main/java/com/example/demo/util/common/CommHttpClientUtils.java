package com.example.demo.util.common;

import com.alibaba.fastjson.JSONObject;
import com.example.demo.util.common.http.HttpClientUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


@Component
@Slf4j
public class CommHttpClientUtils {

    private static String D;
    @Value("${aicore.d}")
    public void setD(String D){
        CommHttpClientUtils.D = D;
    }

    private static String S;
    @Value("${aicore.s}")
    public void setS(String S){
        CommHttpClientUtils.S = S;
    }

    private static String URL;
    @Value("${aicore.url}")
    public void setURL(String URL){
        CommHttpClientUtils.URL = URL;
    }

    /**
     * Robot请求接口
     * @param serviceName 接口名称  示例:robot.create
     * @param jsonReq 请求参数
     * @return
     */
    public static String doPostToAiCore(String serviceName, JSONObject jsonReq){
        log.info("jsonReq = " + jsonReq);
        String response = "";
        try{
            jsonReq.put("_d", D);
            jsonReq.put("_s",S);
            response = HttpClientUtils.doPostJson(URL+serviceName,jsonReq.toString());
        }catch (Exception e){
            log.error("请求接口异常",e);
        }
        return response;
    }
}
