package com.example.demo.util.common.exception;

public class AIValidateException extends RuntimeException {

    private static final long serialVersionUID = 883132628243816501L;

	private final String code;
	private String specialMsg;

    public AIValidateException(String specialMsg) {
        super(specialMsg);
        this.code = "-1";
    }

    public AIValidateException(String code, String specialMsg) {
        super(specialMsg);
        this.code = code;
    }

    public AIValidateException(Throwable cause) {
        super(cause);
        this.code = "-1";
    }

    public String getCode() {
        return code;
    }

    public String getSpecialMsg() {
        return specialMsg;
    }
}
