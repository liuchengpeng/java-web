package com.example.demo.util.common.vo;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * 统一接口返回类型
 * 
 * @author cheng
 *
 * @param <T>
 */
public class ResultModel<T extends Serializable> implements Serializable {

	private static final long serialVersionUID = 9041530013023432967L;
	/**
	 * 返回结果是否成功，true成功，false失败
	 */
	private boolean isSuccessed = true;
	/**
	 * 返回结果对象
	 */
	private T returnValue;
	/**
	 * 返回结果失败时的错误代码，isSuccessed=false时返回
	 */
	private String errorCode;
	/**
	 * 返回结果失败时的错误描述，isSuccessed=false时根据需要返回
	 */
	private String errorDesc;
	/**
	 * 返回结果失败时的详细异常信息，isSuccessed=false时根据需要返回
	 */
	private String exceptionDesc;
	/**
	 * 在报错的情况下，如果任然需要传递给前端返回值，用这个属性来表示
	 */
	private String errorReturnValue;

	/**
	 * @return the errorReturnValue
	 */
	public String getErrorReturnValue() {
		return errorReturnValue;
	}

	/**
	 * @param errorReturnValue the errorReturnValue to set
	 */
	public void setErrorReturnValue(String errorReturnValue) {
		this.errorReturnValue = errorReturnValue;
	}

	public static <E extends Serializable> ResultModel<E> newInstance() {
		return new ResultModel<E>();
	}

	public boolean isSuccessed() {
		return isSuccessed;
	}

	public void setSuccessed(boolean isSuccessed) {
		this.isSuccessed = isSuccessed;
	}

	public String getExceptionDesc() {
		return exceptionDesc;
	}

	public void setExceptionDesc(String exceptionDesc) {
		this.exceptionDesc = exceptionDesc;
	}

	public T getReturnValue() {
		return returnValue;
	}

	public void setReturnValue(T returnValue) {
		this.returnValue = returnValue;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorDesc() {
		return errorDesc;
	}

	public void setErrorDesc(String errorDesc) {
		this.errorDesc = errorDesc;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
