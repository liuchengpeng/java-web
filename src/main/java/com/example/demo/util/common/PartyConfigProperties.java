package com.example.demo.util.common;

import org.apache.commons.lang3.StringUtils;

import java.util.Map;
import java.util.Properties;

public class PartyConfigProperties {
    private static Properties properties = null;

    public static String getProperty(String key) {
        if (null == properties) {
            Map<String, String> map = System.getenv();
            String fileName = "application-" + (StringUtils.isBlank(map.get("APP_ENV")) ? "dev" : map.get("APP_ENV")) + ".properties";
            properties = PropertiesUtils.getProperties(fileName);
        }
        return properties.getProperty(key);
    }

    public static String getProperty(String key, String defaultValue) {
        if (null == properties) {
            Map<String, String> map = System.getenv();
            String fileName = "application-" + (StringUtils.isBlank(map.get("APP_ENV")) ? "dev" : map.get("APP_ENV")) + ".properties";
            properties = PropertiesUtils.getProperties(fileName);
        }
        return properties.getProperty(key, defaultValue);
    }
}
