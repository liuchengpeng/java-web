/*
 * Copyright 2015 Alibaba.com All right reserved. This software is the confidential and proprietary information of
 * Alibaba.com ("Confidential Information"). You shall not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the license agreement you entered into with Alibaba.com.
 */
package com.example.demo.util.common.json;

import com.alibaba.fastjson.JSONObject;
import com.example.demo.util.common.string.StringUtil;
import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONArray;
import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.StringWriter;
import java.util.*;

/**
 * 类JSONUtils.java的实现描述： 类实现描述
 *
 */
@Slf4j
public class JSONUtils {

	private static final Logger logger = LoggerFactory.getLogger(JSONUtils.class);

	public static ObjectMapper objectMapper = new ObjectMapper();

	public static String beanToJSON(Object obj) {
		StringWriter str = new StringWriter();
		try {
			objectMapper.writeValue(str, obj);
			logger.info(str.toString());
			return str.toString();
		} catch (Exception e) {
			logger.error("对象转化为json格式时报错:{}", e);
		}
		return null;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static Object jsonToObject(String json, Class clasz) {
		try {
			return objectMapper.readValue(json, clasz);
		} catch (Exception e) {
			logger.error("json数据转化为对象时报错:{}", e);
		}
		return null;
	}

	/**
	 * json数组转化为java集合对象
	 *
	 * @param jsonArrayStr
	 * @param clasz
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static List jsonToList(String jsonArrayStr, Class clasz) {
		List list = new ArrayList();
		try {
			if (StringUtils.isBlank(jsonArrayStr)) {

				return list;
			}
			JSONArray datas = JSONArray.fromObject(jsonArrayStr);
			for (int i = 0; i < datas.size(); i++) {
				if (datas.get(i) != null) {
					Object data = JSONUtils.jsonToObject(datas.get(i).toString(), clasz);
					if (data != null) {
						list.add(data);
					}
				}
			}
		} catch (Exception ex) {
			logger.error("json数据转化为对象时报错:{}", ex);
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	public static Map<String, Object> parseJSON2Map(String jsonStr) {
		Map<String, Object> map = new HashMap<String, Object>();
		// 最外层解析
		JSONObject json = JSONObject.parseObject(jsonStr);
		for (Object k : json.keySet()) {
			Object v = json.get(k);
			// 如果内层还是数组的话，继续解析
			if (v instanceof JSONArray) {
				List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
				Iterator<JSONObject> it = ((JSONArray) v).iterator();
				while (it.hasNext()) {
					JSONObject json2 = it.next();
					list.add(parseJSON2Map(json2.toString()));
				}
				map.put(k.toString(), list);
			} else {
				map.put(k.toString(), v);
			}
		}
		return map;
	}

	/**
	 * 将map转化为json字符串
	 * @param map
	 * @return
	 */
	public static String mapToJson(Map<? extends Object, ? extends Object> map) {
		String string = StringUtil.EMPTY_STRING;
		if ((map != null) && !map.isEmpty()) {
			Gson gson = new Gson();
			string = gson.toJson(map);
		}
		return string;
	}


	/**
	 * json字符串转bean
	 *
	 * @param result
	 * @param c
	 * @return
	 */
	public static Object jsonToBean(String result, Class c) {
		log.info("result = " + result);
		JSONObject jsonObject1 = JSONObject.parseObject(result);
		Object objBean = JSONObject.toJavaObject(jsonObject1, c);
		return objBean;
	}
}
