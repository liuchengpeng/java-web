package com.example.demo.util.common.string;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.apache.commons.lang.StringUtils;

/**
 * 可以使用这个StringUtils。该类继承了apache-common包的字符工具类。
 *
 */
public class StringUtil extends StringUtils {

	// 一个空的字符串。
	public static final String EMPTY_STRING = "";
	// 一个空格
	public static final String SPACE = " ";
	// 双引号
	public static final String TOW_QUOTE = "\"";

	// ================================将符号也作为常量，避免半角全角符号导致的狗血异常======================================
	// 逗号
	public static final String SYMBOL_COMMA = ",";

	// 等于号
	public static final String SYMBOL_EQUAL = "=";

	// 点号
	public static final String SYMBOL_DOT = ".";

	// 问号
	public static final String SYMBOL_QUESTION = "?";

	// 分号
	public static final String SYMBOL_SEMICOLON = ";";

	// 中划线
	public static final String SYMBOL_HYPHEN = "-";

	// 感叹号
	public static final String SYMBOL_EXCL = "!";

	// 冒号
	public static final String SYMBOL_COLON = ":";

	// 单引号
	public static final String SYMBOL_SINGLE_QUOTE = "'";

	// 双引号
	public static final String SYMBOL_DOUBLE_QUOTE = "\"";

	// 百分号
	public static final String SYMBOL_PERCENT = "%";

	// 省略号
	public static final String SYMBOL_ELLIPSIS = "……";

	// 左括号
	public static final String SYMBOL_LEFT_BRACE = "(";

	// 右括号
	public static final String SYMBOL_RIGHT_BRACE = ")";

	// &
	public static final String SYMBOL_OPERATION_AND = "&";

	// 下划线
	public static final String SYMBOL_UNDERLINE = "_";

	// 斜杠
	public static final String SYMBOL_SLASH = "/";

	// @符号
	public static final String SYMBOL_AT = "@";

	/**
	 * 正则表达式预编译，校验邮箱格式
	 */
	private static Pattern patternMailboxFormat = Pattern.compile(
			"^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,10}|[0-9]{1,10})(\\]?)$");
	/**
	 * 正则表达式预编译，校验手机号码
	 */
	private static Pattern patternMobileNoFormat = Pattern.compile("^((13[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$");
	/**
	 * 正则表达式预编译，校验电话号码
	 */
	private static Pattern patternPhoneNoFormat = Pattern.compile("^[0-9-+()]+$");
	/**
	 * 正则表达式预编译，校验手机号码是否为11位
	 */
	private static Pattern patternMobileNois11 = Pattern.compile("^\\d{11}$");
	/**
	 * 正则表达式预编译，校验是否正整数
	 */
	private static Pattern patternPositiveInteger = Pattern.compile("^[1-9][0-9]*$");
	/**
	 * 正则表达式预编译，校验网址
	 */
	private static Pattern patternWebsite = Pattern.compile("[a-zA-z]+://[^\\s]*");
	/**
	 * 正则表达式预编译，验证网址的字母、数字、中划线、下划线、点，
	 */
	private static Pattern patternWebsiteSymbol = Pattern.compile("^[\\w\\d_\\+\\.]*$");
	/**
	 * 去除空格回车换行水平制表符
	 */
	private static final Pattern PATTERN_SPECIAL_CHARACTERS = Pattern.compile("\\s*|\t|\r|\n");

	/**
	 * 去除所有符号，只保留中英文
	 */
	private static final Pattern PATTERN_ALL_CHARACTERS = Pattern
			.compile("[`!@#$%^&*()+=|{}':;',//[//].<>/?！@#￥%……&*（）——+|{}【】‘；：”“’。，、？、，：；。！？\n"
					+ "｛｝（）〔〕＜＞〈〉《》［］「」『』〖〗【】\n" + "＠＃％＊＆＋＝±×÷～－\u2014\u2015＿—─━￣\u2025…┈┄┅┉┆┇┊┋｜\ufe31│┃∥＼／\u2215\n"
					+ "‘’“”＂＇\u2035′\u301d″\u02ca\u02cb\n" + "＄￡￥‰§№°℃\u2109\u2105\n" + "＾ˇ¨｀°¤〃\n" + "♂♀ ￠¤※\u2573\n"
					+ "\u221f\u2252\u2266\u2267\u22bf∧∨∑∏∪∩∈∷√⊥∥∠⌒⊙∫∮≡≌≈∽∝≠≮≯≤≥∞∵∴\n"
					+ "☆★○●◎◇◆□■△▲\u25bd\u25bc\u2609\n"
					+ "〓\u25e2\u25e3\u25e4\u25e5\u2594\u2581\u2582\u2583\u2585\u2587\u2588\u2589\u2593\u258a\u258b\u258c\u258d\u258e\u258f\u2595\n"
					+ "→←↑↓\u2196\u2197\u2198\u2199\n" + "\u256d\u256e\u2570\u256f\n"
					+ "\ufe35\ufe36\ufe39\ufe3a\ufe3f\ufe40\ufe3d\ufe3e\ufe41\ufe42\ufe43\ufe44\ufe3b\ufe3c\ufe37\ufe38\n"
					+ "\u2550\u2551\u2552\u2553\u2554\u2555\u2556\u2557\u2559\u255a\u255b\u255c\u255d\u255e\u255f\u2560\u2561\u2562\u2563\u2564\u2565\u2566\u2567\u2568\u2569\u256a\u256b\u256c\u3012\n"
					+ "┌┍┎┏┐┑┒┓└┕┖┗┘┙┚┛├┝┞┟┠┡┢┣┤┥┦┧┨┩┪┫┬┭┮┯┰┱┲┳┴┵┶┷┸┹┺┻┼┽┾┿╀╁╂╃╄╅╆╇╈╉╊╋" + "\n"
					+ "\\s*|\t|\n|\r|\\\\|\"]");

	/**
	 * 如果传入的值是null，返回空字符串，如果不是null，返回本身。
	 *
	 * @param word 传入的源字符串。
	 * @return
	 */
	public static String getNotNullValue(String word) {
		return ((word == null) || word.equalsIgnoreCase("null")) ? "" : word;
	}

	/**
	 * 如果传入的值是null，返回空字符串，如果不是null，返回本身。
	 *
	 * @param word 传入的源字符串。
	 * @return
	 */
	public static String getNotNullValue(String word, String defaultWord) {
		return ((word == null) || word.equalsIgnoreCase("null")) ? defaultWord : word;
	}

	/**
	 * 根据分隔符从一段字符串拿到对应的列表。应用于以下场景。 2,3,4,5 ==> [2,3,4,5]
	 *
	 * @param originWord
	 * @param symbol
	 * @return
	 */
	public static List<String> getSplitListFromString(String originWord, String symbol) {
		List<String> result = new ArrayList<String>();
		if (isBlank(originWord)) {
			return result;
		}

		String[] splitData = originWord.split(symbol);
		if ((splitData == null) || (splitData.length == 0)) {
			return result;
		}

		for (String word : splitData) {
			if (isNotBlank(word)) {
				result.add(word);
			}
		}
		return result;
	}

	public static String replaceBlank(String str) {
		String dest = "";
		if (str != null) {
			Matcher m = PATTERN_SPECIAL_CHARACTERS.matcher(str);
			dest = m.replaceAll("");
		}
		return dest;
	}

	/**
	 * @author lijch
	 * @param originalStr
	 * @param symbol
	 * @return
	 */
	public static List<Long> getLongListFromString(String originalStr, String symbol) {
		List<Long> result = new ArrayList<Long>();
		if (isBlank(originalStr)) {
			return result;
		}

		String[] splitData = originalStr.split(symbol);

		for (String word : splitData) {
			if (isNotBlank(word)) {
				result.add(Long.parseLong(word));
			}
		}
		return result;
	}

	/**
	 * 移除左边的0, eg：00000jakjdkf89000988000 转换之后变为 jakjdkf89000988000
	 *
	 * @param str
	 * @return
	 */
	public static String removeLeftZero(String str) {
		int start = 0;
		if (isNotEmpty(str)) {
			char[] chars = str.toCharArray();
			for (int i = 0; i < chars.length; i++) {
				if (chars[i] != '0') {
					start = i;
					break;
				}
			}
			return str.substring(start);
		}
		return "";
	}

	// 用0补足6位
	public static String fillLeftZero(String str) {
		if (StringUtils.isBlank(str)) {
			return str;
		}
		char[] chars = str.toCharArray();
		if (Character.isLetter(chars[0])) {
			return str;
		}
		if (str.length() >= 6) {
			return str;
		}
		int fillnum = 6 - str.length();
		StringBuilder sb = new StringBuilder("");
		for (int i = 0; i < fillnum; i++) {
			sb.append("0");
		}
		sb.append(str);
		return sb.toString();
	}

	/**
	 * UTF8字符长度(汉字占3个字符长度)
	 *
	 * @param str
	 * @return
	 */
	public static int lengthBytes(String str) {
		try {
			return str == null ? 0 : str.getBytes("utf8").length;
		} catch (UnsupportedEncodingException e) {
			return 0;
		}
	}

	/**
	 * @author 杨冬 验证邮箱格式
	 * @return boolean
	 */
	public static boolean isEmail(String email) {
		Matcher matcher = patternMailboxFormat.matcher(email);
		if (isEmpty(email)) {
			// System.out.println("邮箱为空");
			return false;
		} else if (!matcher.matches()) {
			// System.out.println("邮箱格式验证不通过：" + matcher.matches());
			return false;
		} else {
			// System.out.println("邮箱格式验证通过：" + matcher.matches());
			return true;
		}
	}

	/**
	 * @author 杨冬 验证手机号码 中国电信发布中国3G号码段:中国联通185,186;中国移动188,187;中国电信189,180共6个号段。
	 *         3G业务专属的180-189号段已基本分配给各运营商使用,
	 *         其中180、189分配给中国电信,187、188归中国移动使用,185、186属于新联通。
	 *         中国移动拥有号码段：139、138、137、136、135、134、159、158、157（3G）、152、151、150、188（3G）、187（3G）;14个号段
	 *         中国联通拥有号码段：130、131、132、155、156（3G）、186（3G）、185（3G）;6个号段
	 *         中国电信拥有号码段：133、153、189（3G）、180（3G）;4个号码段 移动:
	 *         2G号段(GSM网络)有139,138,137,136,135,134(0-8),159,158,152,151,150
	 *         3G号段(TD-SCDMA网络)有157,188,187 147是移动TD上网卡专用号段. 联通:
	 *         2G号段(GSM网络)有130,131,132,155,156 3G号段(WCDMA网络)有186,185 电信:
	 *         2G号段(CDMA网络)有133,153 3G号段(CDMA网络)有189,180
	 * @return boolean
	 */
	public static boolean isMobileNO(String mobileNo) {
		Matcher matcher = patternMobileNoFormat.matcher(mobileNo);
		if (isEmpty(mobileNo)) {
			// System.out.println("手机号码为空");
			return false;
		} else if (!matcher.matches()) {
			// System.out.println("手机号码验证不通过" + matcher.matches());
			return false;
		} else {
			// System.out.println("手机号码验证通过" + matcher.matches());
			return true;
		}
	}

	/**
	 * 校验是否有效的电话号码
	 *
	 * @param phoneNo 电话号码
	 * @return boolean
	 */
	public static boolean isPhoneNO(String phoneNo) {
		Matcher matcher = patternPhoneNoFormat.matcher(phoneNo);
		if (isEmpty(phoneNo)) {
			// System.out.println("手机号码为空");
			return false;
		} else if (!matcher.matches()) {
			// System.out.println("手机号码验证不通过" + matcher.matches());
			return false;
		} else {
			// System.out.println("手机号码验证通过" + matcher.matches());
			return true;
		}
	}

	/**
	 * 验证手机号码是否11位数字
	 *
	 * @param mobileNo
	 * @return boolean
	 */
	public static boolean isMobileNoNew(String mobileNo) {
		if (StringUtils.isNotBlank(mobileNo)) {
			Matcher matcher = patternMobileNois11.matcher(mobileNo);
			if (matcher.matches()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 验证是否正整数
	 *
	 * @param mobileNo
	 * @return boolean
	 */
	public static boolean isPositiveInteger(String number) {
		if (StringUtils.isNotBlank(number)) {
			Matcher matcher = patternPositiveInteger.matcher(number);
			if (matcher.matches()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 将BigDecimal格式化成千分位格式字符串
	 *
	 * @return String
	 * @author 杨冬
	 */
	public static String formatBigDecimal(BigDecimal number) {
		if (number == null) {
			return null;
		}
		NumberFormat currency = NumberFormat.getCurrencyInstance(new Locale("zh", "CN"));// 建立货币格式
		String formatNumber = currency.format(number);
		formatNumber = formatNumber.replace("￥", "");
		return formatNumber;
	}

	/**
	 * 千分位格式化BigDecimal类型数据,保留6位小数
	 *
	 * @param number
	 * @return
	 */
	public static String formatBigDecimal2(BigDecimal number) {
		if (number == null) {
			return null;
		}
		// ,代表分隔符
		// 0.后面的##代表位数 如果换成0 效果就是位数不足0补齐
		DecimalFormat format = new DecimalFormat("#,##0.######");
		return format.format(number);
	}

	/**
	 * 格式化Double类型数据
	 *
	 * @param number
	 * @return String
	 */
	public static String formatDouble(Double number) {
		if (number == null) {
			return null;
		}
		DecimalFormat df = new DecimalFormat(",###,##0.0");
		return df.format(number);
	}

	/**
	 * 格式化股权结构，股数的Double类型数据
	 *
	 * @param number
	 * @return String
	 */
	public static String formatDoubleOfShareNumber(Double number) {
		if (number == null) {
			return null;
		}
		DecimalFormat df = new DecimalFormat(",###,##0");
		return df.format(number);
	}

	/**
	 * 格式化股权结构，股数的BigDecimal类型数据
	 *
	 * @param number
	 * @return String
	 */
	public static String formatDoubleOfNumberNoZero(BigDecimal number) {
		if (number == null) {
			return null;
		}
		DecimalFormat decimalFormat = new DecimalFormat(",###,##0.###########");
		return decimalFormat.format(number);
	}

	/**
	 * 将Long格式化成千分位格式字符串
	 *
	 * @return String
	 * @author 杨冬
	 */
	public static String formatLong(Long number) {
		if (number == null) {
			return null;
		}
		DecimalFormat format = new DecimalFormat("##0,000");// 格式化成千分位
		String numberStr = number.toString();
		String formatNumber = "";// 返回的格式化字符串
		long numberAbs = Math.abs(number);// 求绝对值
		// 如果大于1000转换成千分位格式
		if (numberAbs >= 1000) {
			formatNumber = format.format(number);
		} else {
			formatNumber = numberStr;
		}
		return formatNumber;
	}

	/**
	 * guoziqiu 判断小数位数是否符合，支持大数，整数，负数，小数，不支持科学记数法
	 *
	 * @param number 数字(String)
	 * @param bit    位数
	 * @return
	 */
	public static boolean isNumberDecimal(String number, int bit) {
		boolean flag = false;
		if ((bit < 0) || StringUtil.isBlank(number)) {
			return false;
		}
		BigDecimal bigDecimal = new BigDecimal(number);
		// 格式化位数+1的值
		bigDecimal = bigDecimal.setScale((bit + 1), BigDecimal.ROUND_UP);
		String decimal = String.valueOf(bigDecimal.toPlainString());
		// 判断位数
		if (decimal.indexOf(".") != -1) {// 判断为小数
			String pointString = decimal.split("\\.")[1];// 截取小数位后面的值
			// 去除字符末尾的零
			pointString = pointString.replaceAll("(0)*$", "");
			if (pointString.length() < (bit + 1)) {
				flag = true;
			}
		}
		return flag;
	}

	/**
	 * 计算字符串的字节长度
	 *
	 * @param s
	 * @return
	 */
	public static int getStrByteLength(String s) {
		int length = 0;
		if (isBlank(s)) {
			return 0;
		}
		for (int i = 0; i < s.length(); i++) {
			int ascii = Character.codePointAt(s, i);
			if ((ascii >= 0) && (ascii <= 255)) {
				length++;
			} else {
				length += 2;
			}
		}
		return length;
	}

	/**
	 * 通过字节长度截取字符串
	 *
	 * @param str
	 * @param length
	 * @return
	 */
	public static String getStringByBytes(String str, int length) {
		if (isBlank(str) || (length == 0)) {
			return "";
		}

		StringBuffer result = new StringBuffer();
		if (StringUtil.getStrByteLength(str) > length) {
			int count = 0;
			char[] chars = str.toCharArray();
			int charLength = chars.length;
			for (int i = 0; i < charLength; i++) {
				int ascii = Character.codePointAt(chars, i);
				if ((ascii >= 0) && (ascii <= 255)) {
					count++;
				} else {
					count += 2;
				}
				if (count < length) {
					result.append(chars[i]);
				}
			}
		} else {
			return str;
		}
		return result.toString();
	}

	/**
	 * 生成随机密码
	 *
	 * @param length 密码长度
	 * @return String
	 */
	public static String randomPassword(int length) {
		char[] str = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S',
				'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
				'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8',
				'9' };
		StringBuffer password = new StringBuffer();
		Random random = new Random();
		while (password.length() < length) {
			password.append(str[random.nextInt(str.length)]);
		}
		return password.toString();
	}

	/**
	 * 验证网址
	 *
	 * @return boolean
	 */
	public static boolean isWebsite(String website) {
		Matcher matcher = patternWebsite.matcher(website);
		if (isBlank(website)) {
			return false;
		} else if (!matcher.matches()) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * 验证字母、数字、中划线、下划线、点，
	 *
	 * @return boolean
	 */
	public static boolean isEnKey(String website) {
		Matcher matcher = patternWebsiteSymbol.matcher(website);
		if (isBlank(website)) {
			return false;
		} else if (!matcher.matches()) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * 去除所有的符号并替换成空格
	 *
	 * @param str
	 * @return
	 * @throws PatternSyntaxException
	 */
	public static String stringFilter(String str) throws PatternSyntaxException {

		Matcher matcher = PATTERN_ALL_CHARACTERS.matcher(str);
		return matcher.replaceAll(" ").trim();
	}

	/**
	 * 正则表达式匹配两个指定字符串中间的内容
	 *
	 * @param soap
	 * @return
	 */
	public static List<String> getSubUtil(String soap, String rgex) {
		List<String> list = new ArrayList<String>();
		if (StringUtil.isBlank(soap)) {
			return list;
		}
		Pattern pattern = Pattern.compile(rgex);// 匹配的模式
		Matcher m = pattern.matcher(soap);
		while (m.find()) {
			int i = 1;
			list.add(m.group(i));
			i++;
		}
		return list;
	}

	/**
	 * 返回单个字符串，若匹配到多个的话就返回第一个，方法与getSubUtil一样
	 *
	 * @param soap
	 * @param rgex
	 * @return
	 */
	public static String getSubUtilSimple(String soap, String rgex) {
		Pattern pattern = Pattern.compile(rgex);// 匹配的模式
		Matcher m = pattern.matcher(soap);
		while (m.find()) {
			return m.group(1);
		}
		return "";
	}

	public static boolean isChinese(char c) {
		Character.UnicodeBlock ub = Character.UnicodeBlock.of(c);
		if ((ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS)
				|| (ub == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS)
				|| (ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A)
				|| (ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_B)
				|| (ub == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION)
				|| (ub == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS)
				|| (ub == Character.UnicodeBlock.GENERAL_PUNCTUATION)) {
			return true;
		}
		return false;
	}

	public static boolean isEnglish(String charaString) {
		return charaString.matches("^[a-zA-Z]*");
	}

	public static boolean isContainChinese(String str) {// 检测是否包含中文
		String regEx = "[\\u4E00-\\u9FA5]+";
		Pattern p = Pattern.compile(regEx);
		Matcher m = p.matcher(str);
		if (m.find()) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 消息文案内容替换
	 *
	 * @param content 待替换字符串
	 * @param params  待替换文案Map
	 * @return String
	 */
	public static String replaceMsgWord(String content, Map<String, String> params) {
		if (StringUtil.isNotBlank(content) && (params != null) && !params.isEmpty()) {
			for (String key : params.keySet()) {
				// 判断是否还有占位符
				if (content.indexOf(SYMBOL_AT) < 0) {
					break;
				}
				// 替换字符串
				if (StringUtil.isNotBlank(key) && (content.indexOf(key) > -1)
						&& StringUtil.isNotBlank(params.get(key))) {
					content = content.replace(key, params.get(key));
				}
			}
		}
		return content;
	}

	/**
	 * 对象转换为字符串
	 *
	 * @see 重写valueOf()方法
	 * @param object
	 * @return String
	 */
	public static String valueOf(Object object) {
		if (object != null) {
			return object.toString();
		}
		return null;
	}

	/**
	 * 解决CSV文件的读写问题（特别是有双引号和逗号的情况）
	 */
	public static String convertCsvStr(String str) {
		if (str != null) {
			StringBuilder sb = new StringBuilder();
			sb.append("\"");
			str = str.replace("\"", "\"\"");
			sb.append(str);
			sb.append("\"");
			return sb.toString();
		}
		return null;
	}
}
