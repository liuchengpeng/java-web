/*
 * Copyright 2015 Alibaba.com All right reserved. This software is the confidential and proprietary information of
 * Alibaba.com ("Confidential Information"). You shall not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the license agreement you entered into with Alibaba.com.
 */
package com.example.demo.util.common;

import java.io.Serializable;

/**
 *
 * @author cheng
 *
 * @param <T>
 */
public class CustomPage<T> implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 8104777827863916550L;
	/** 每页显示记录数 **/
	private Integer pageSize;
	/** 查询的集合 **/
	private T datas;
	/** 总页数 **/
	private int totalPage;
	/** 当前页，第几页 **/
	private Integer currentPage;
	/** 总记录数 **/
	private Integer totalCount;
	/** 是否有下页 **/
	private boolean hasNextPage = false;
	/** 是否有上页 **/
	private boolean hasPreviousPage = false;

	/**
	 *
	 */
	public CustomPage() {
		super();
	}

	/**
	 * @param pageSize  每页条数
	 * @param pageIndex 当前页，第几页
	 * @param totalNum  总记录数
	 * @param data      查询的集合
	 */
	public CustomPage(Integer pageSize, Integer currentPage, Integer totalCount, T data) {
		super();
		if ((pageSize == null) || (pageSize == 0)) {
			pageSize = 5;
		}
		if ((currentPage == null) || (currentPage == 0)) {
			currentPage = 1;
		}
		if ((totalCount == null) || (totalCount == 0)) {
			totalCount = 0;
		}
		this.pageSize = pageSize;
		this.datas = data;
		// pageSize==0会报错
		this.totalPage = ((totalCount % pageSize) == 0) ? (totalCount / pageSize) : ((totalCount / pageSize) + 1);
		this.currentPage = (currentPage == 0) ? (1) : (currentPage);
		this.totalCount = totalCount;
		this.hasNextPage = ((this.totalPage > 1) && (this.totalPage > this.currentPage));
		this.hasPreviousPage = (this.currentPage > 1);

	}

	/**
	 * @return the pageSize
	 */
	public Integer getPageSize() {
		return pageSize;
	}

	/**
	 * @param pageSize the pageSize to set
	 */
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * @return the datas
	 */
	public T getDatas() {
		return datas;
	}

	/**
	 * @param datas the datas to set
	 */
	public void setDatas(T datas) {
		this.datas = datas;
	}

	/**
	 * @return the totalPage
	 */
	public int getTotalPage() {
		return totalPage;
	}

	/**
	 * @param totalPage the totalPage to set
	 */
	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}

	/**
	 * @return the currentPage
	 */
	public Integer getCurrentPage() {
		return currentPage;
	}

	/**
	 * @return the totalCount
	 */
	public Integer getTotalCount() {
		return totalCount;
	}

	/**
	 * @param totalCount the totalCount to set
	 */
	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	/**
	 * @return the hasNextPage
	 */
	public boolean isHasNextPage() {
		return hasNextPage;
	}

	/**
	 * @param hasNextPage the hasNextPage to set
	 */
	public void setHasNextPage(boolean hasNextPage) {
		this.hasNextPage = hasNextPage;
	}

	/**
	 * @return the hasPreviousPage
	 */
	public boolean isHasPreviousPage() {
		return hasPreviousPage;
	}

	/**
	 * @param hasPreviousPage the hasPreviousPage to set
	 */
	public void setHasPreviousPage(boolean hasPreviousPage) {
		this.hasPreviousPage = hasPreviousPage;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
