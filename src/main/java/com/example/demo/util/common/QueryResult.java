package com.example.demo.util.common;

import java.io.Serializable;
import java.util.List;

public class QueryResult<T> implements Serializable {

	private static final long serialVersionUID = 1L;
	private List<T> resultlist;
	private long totalrecord;

	public List<T> getResultlist() {
		return resultlist;
	}

	public void setResultlist(List<T> resultlist) {
		this.resultlist = resultlist;
	}

	public long getTotalrecord() {
		return totalrecord;
	}

	public void setTotalrecord(long totalrecord) {
		this.totalrecord = totalrecord;
	}
}
