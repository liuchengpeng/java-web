package com.example.demo.constant;

/**
 * @class_name: AiCoreConstant
 * @package: com.example.demo.constant
 * @describe: Ai Core 常量配置
 * @author: le.gao
 * @creat_date: 2019/7/2
 * @creat_time: 16:13
 **/
public class AiCoreConstant {
    /**
     * 创建机器人
     */
    public static final String ROBOT_CREATE="robot.create";
    /**
     * 更新机器人基本信息
     */
    public static final String ROBOT_UPINFO="robot.upinfo";
    /**
     * 机器人列表
     */
    public static final String ROBOT_LS="robot.ls";
    /**
     * 更新某机器人的secretkey
     */
    public static final String ROBOT_UPSEC="robot.upsec";
    /**
     * 更新某机器人的状态
     */
    public static final String ROBOT_UPSTATUS="robot.upstatus";
    /**
     * 更新某机器人的名字
     */
    public static final String ROBOT_UPNAME="robot.upname";
    /**
     * 更新某机器人的杂项信息
     */
    public static final String ROBOT_UPOTHER="robot.upother";
    /**
     * 获取某机器人各项信息
     */
    public static final String ROBOT_ONE="robot.one";
    /**
     * 更新机器人流程图入口节点
     */
    public static final String ROBOT_UPENTRYTREEID="robot.upentrytreeid";
    /**
     * 将某机器人所有信息覆盖至另一个
     */
    public static final String ROBOT_COPY="robot.copy";
    /**
     * 新增一个知识库
     */
    public static final String KNOW_ADDCATE = "know.addcate";
    /**
     * 修改一个知识库
     */
    public static final String KNOW_UPCATE = "know.upcate";
    /**
     * 知识库列表
     */
    public static final String KNOW_LSCATE = "know.lscate";
    /**
     * 删除一个知识库
     */
    public static final String KNOW_DELCATE = "know.delcate";
    /**
     * 添加一个知识词条
     */
    public static final String KNOW_ADDKNOW = "know.addknow";
    /**
     * 更新一个知识词条
     */
    public static final String KNOW_UPKNOW = "know.upknow";
    /**
     * 获取一个知识词条
     */
    public static final String KNOW_ONEKNOW = "know.oneknow";
    /**
     * 列出某知识库的知识词条
     */
    public static final String KNOW_LSKNOW = "know.lsknow";
    /**
     * 删除一个知识词条
     */
    public static final String KNOW_DELKNOW = "know.delknow";
    /**
     * 建立/修改robot与知识库的绑定关系
     */
    public static final String KNOW_BANDRELA = "know.bandrela";
    /**
     * 解除robot与知识库的绑定关系
     */
    public static final String KNOW_UNBANDRELA = "know.unbandrela";
    /**
     * 列出某已robot绑定的知识库
     */
    public static final String KNOW_LSBANDED = "know.lsbanded";


    /**
     * 创建自定义实体
     */
    public static final String ENTITY_CREATE = "entity.create";

    /**
     * 实体列表
     */
    public static final String ENTITY_LS = "entity.ls";

    /**
     * 修改实体名和描述
     */
    public static final String ENTITY_UP_NAME = "entity.upname";

    /**
     * 修改实体的实体值
     */
    public static final String ENTITY_UP_CONTENT = "entity.upcontent";

    /**
     * 删除某实体
     */
    public static final String ENTITY_DEL = "entity.del";

    /**
     * 获取某单一实体全部信息
     */
    public static final String ENTITY_ONE = "entity.one";

    /**
     * 创建任务节点
     */
    public static final String TREE_CREATE = "tree.create";

    /**
     * 列出某robot下的所有任务
     */
    public static final String TREE_LS = "tree.ls";

    /**
     * 列出某任务内容
     */
    public static final String TREE_ONE = "tree.one";

    /**
     * 更新某个任务
     */
    public static final String TREE_UPDATE = "tree.update";

    /**
     * 删除某个任务
     */
    public static final String TREE_DEL = "tree.del";

    /**
     * 更新/添加某节点的某跳转条件
     */
    public static final String TREE_UP_JUMP = "tree.upjump";

    /**
     * 删除某节点的某跳转条件
     */
    public static final String TREE_DEL_JUMP = "tree.deljump";

    /**
     * 对某节点的某跳转条件
     */
    public static final String TREE_SORT_JUMP = "tree.sortjump";

}
